# The name of the package:
atlas_subdir (read_truth3)

# Add the shared library:
atlas_add_library (read_truth3Lib
  read_truth3/*.h Root/*.cxx
  PUBLIC_HEADERS read_truth3
  LINK_LIBRARIES AnaAlgorithmLib xAODCore xAODJet xAODMissingET xAODEventInfo xAODBase xAODTruth MCTruthClassifierLib PathResolver PMGAnalysisInterfacesLib)

if (XAOD_STANDALONE)
 # Add the dictionary (for AnalysisBase only):
 atlas_add_dictionary (read_truth3Dict
  read_truth3/read_truth3Dict.h
  read_truth3/selection.xml
  LINK_LIBRARIES read_truth3Lib)
endif ()

if (NOT XAOD_STANDALONE)
  # Add a component library for AthAnalysis only:
  atlas_add_component (read_truth3
    src/components/*.cxx
    LINK_LIBRARIES read_truth3Lib)
endif ()

# Install files from the package:
atlas_install_joboptions( share/*_jobOptions.py )
atlas_install_scripts( share/*_eljob.py )
atlas_install_scripts( share/*_config.py )
atlas_install_data( data/* )
