import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

# same file different histograms to draw in same plots

### inputs ###
# +
outpdf = 'tt_Cutflow_BR.pdf'
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'PowhegPythia8',
                  'FSR muR=0.5',
                  'FSR muR=2.0',]
filenm   = '../anatree/gridout/user.xiaohu.lu.testv17a.410470.e6337_s3126_r9364_p4097_hist/merged.root'
listlist_hnm = [ ['WetauHad_1prong_cutflow_weight', 'WetauHad_1prong_fsr_muR05_cutflow_weight', 'WetauHad_1prong_fsr_muR20_cutflow_weight' ],
              ['WetauHad_3prong_cutflow_weight', 'WetauHad_3prong_fsr_muR05_cutflow_weight', 'WetauHad_3prong_fsr_muR20_cutflow_weight' ],
              ['WmutauHad_1prong_cutflow_weight', 'WmutauHad_1prong_fsr_muR05_cutflow_weight', 'WmutauHad_1prong_fsr_muR20_cutflow_weight' ],
              ['WmutauHad_3prong_cutflow_weight', 'WmutauHad_3prong_fsr_muR05_cutflow_weight', 'WmutauHad_3prong_fsr_muR20_cutflow_weight' ],
              ['Wemu_cutflow_weight', 'Wemu_fsr_muR05_cutflow_weight', 'Wemu_fsr_muR20_cutflow_weight'],
            ]
list_style    = [ [ 1, 2, R.kBlack, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kBlue, 0 ],
                  [ 1, 2, R.kRed, 0 ]]
list_draw_opt = [ 'hist',
                  'hist',
                  'hist', ]
legend_title  =  None
### inputs ###

print(listlist_hnm)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,list_hnm in enumerate(listlist_hnm):
  print("Processing histogram {0}".format(list_hnm))

  # get list of histograms
  list_h, __f = listh_dhsf( list_hnm, filenm )
  print(list_h)
  # normalisat to 1
  norm2bin1(list_h,'binomial')
  # set styles
  hstyle(list_h, list_style)

  islogy = True
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], bottompane=0.5, ratiorange=[0.9,1.1], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(listlist_hnm) - 1: c.Print( outpdf )
  if i == len(listlist_hnm) - 1: c.Print( outpdf+')' )
