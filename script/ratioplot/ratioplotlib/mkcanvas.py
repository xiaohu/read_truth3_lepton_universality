import ROOT as R
from util import *

# inputs: list_h_nm [list_h, list_nm], list_h[0] is the denominator
# they are list of histograms and list of their legend entry names
# outputs: canvas
def mkcanvas( list_h_nm, xtitle=None, ytitle=None, bottompane=0.3, ratiorange=[0.5,1.5], legend_title=None, legendx=None, islogy=False, islogx=False,  atlas_label= 'Internal', draw_opt=[] ):

  R.gROOT.SetBatch(1)

  list_h = list_h_nm[0]

  # ATLAS style
  # call it as top level of the scripts, otherwise the style is cleaned up in python

  # setup canvas and pads
  c = R.TCanvas('c','c',800,800)
  padup = R.TPad("padup", "padup", 0, bottompane, 1, 1.0)
  padup.SetBottomMargin(0.02)
  padup.Draw()
  c.cd()
  paddn = R.TPad("paddn", "paddn", 0, 0, 1, bottompane)
  paddn.SetTopMargin(0.02)
  paddn.SetBottomMargin(0.3)
  paddn.Draw()

  # draw histograms in upper pad
  padup.cd()
  ymax, _maxh = findmax( list_h )
  ymin, _minh = findmin( list_h )
  ymin_abs, _minh_abs = findmin_abs( list_h )
  for i,h in enumerate(list_h):
    if i == 0:
      h.SetMaximum( ymax * 1.2 ) # max in y 20%
      h.SetMinimum( 0 )
      if ytitle is None:
        h.SetYTitle('A.U.')
      else:
        h.SetYTitle(ytitle)
      h.GetXaxis().SetLabelSize(0)
      if islogy:
        h.SetMaximum( ymax*1000 ) # max in y 1000x
        #h.SetMinimum( ymin_abs/10 ) # findmin_abs returns 0 if min=0 which is not good for logy
        h.SetMinimum( ymax*0.0001 ) # 4 orders of mag smaller
      if len(draw_opt) == 0:
        h.Draw()
      else:
        h.Draw( draw_opt[i] )
    else:
      if len(draw_opt) == 0:
        h.Draw('same')
      else:
        h.Draw( 'same'+draw_opt[i] )

  if islogy:
    padup.SetLogy()
  if islogx:
    padup.SetLogx()

  # mk legend in upper pad
  _leg = mklegend( list_h_nm, legend_title, legendx, atlas_label )

  # draw ratios in down pad
  paddn.cd()
  # calculte ratio and error
  list_ratio = []
  for i,h in enumerate(list_h):
    newnm = h.GetName()+'_ratio'
    newh = h.Clone(newnm)
    list_ratio.append(newh)

    # denominator 1 +/- its own error%
    if i == 0:
      for ibin in range(newh.GetNbinsX()):
        ibin = ibin + 1
        newh.SetBinError( ibin, h.GetBinError(ibin) / h.GetBinContent(ibin) if h.GetBinContent(ibin)!=0 else 0 )
        newh.SetBinContent( ibin, 1 )
      newh.SetMaximum( ratiorange[1] ) # ratio y range
      newh.SetMinimum( ratiorange[0] ) # ratio y range

      newh.SetYTitle('Ratios')
      if xtitle is not None:
        newh.SetXTitle( xtitle )
      if newh.GetXaxis().GetTitle() == '': # if no x title, use hist title
        newh.GetXaxis().SetTitle( newh.GetTitle() )

      newh.GetXaxis().SetTitleSize( newh.GetXaxis().GetTitleSize()*2 )
      newh.GetYaxis().SetTitleSize( newh.GetYaxis().GetTitleSize()*2 )
      newh.GetXaxis().SetTitleOffset( newh.GetXaxis().GetTitleOffset()*0.75 )
      newh.GetYaxis().SetTitleOffset( newh.GetYaxis().GetTitleOffset()*0.5 )
      newh.SetLabelSize( R.gStyle.GetLabelSize()*2 )
      #newh.GetYaxis().SetNdivisions(5)
      newh.GetYaxis().SetNdivisions(505)
      newh.GetYaxis().SetLabelSize( newh.GetYaxis().GetLabelSize()*2.5 )
      newh.Draw('E3 hist')
      newg = R.TGraphErrors(newh)
      newg.SetFillColor(R.kBlack)
      newg.SetFillStyle(3554)
      newg.Draw('3')
      list_ratio.append(newg)
      #if len( draw_opt ) == 0:
      #  newh.Draw( 'E2hist' )
      #else:
      #  newh.Draw( 'E1hist'+draw_opt[i] )

    # numerators ratio +/-  its own error%
    else:
      newh.Divide( list_h[0] )
      for ibin in range(newh.GetNbinsX()):
        ibin = ibin + 1
        newh.SetBinError( ibin, h.GetBinError(ibin) / h.GetBinContent(ibin) if h.GetBinContent(ibin)!=0 else 0 )
      newh.Draw('same E1 hist')
      #if len( draw_opt ) == 0:
      #  newh.Draw( 'same E1 hist' )
      #else:
      #  newh.Draw( 'same E1 hist'+draw_opt[i] )

  if islogx:
    paddn.SetLogx()

  c.cd()
  c.Update()
  return c, [list_ratio, _leg]

