import ROOT as R
import sys
import ctypes
from math import sqrt

# normalise histogram(s) to unity
# input: h or list_h
def norm2unity( list_h ):
  if type(list_h) == list:
    for h in list_h:
      integral = h.Integral()
      if integral == 0:
        integral = 1
      h.Scale( 1 / integral, 'width' )
  else:
    h = list_h
    integral = h.Integral();
    if integral == 0:
      integral = 1
    h.Scale( 1 / integral, 'width' )

# normalise histogram(s) to bin1 content
# input: h or list_h
# err_opt = 'binomial': then consider bin1 as denominator and calculate eff err
# https://root.cern.ch/doc/master/TH1_8cxx_source.html#l02871
# fSumw2.fArray[i] = TMath::Abs( ( (1. - 2.* b1 / b2) * e1sq  + b1sq * e2sq / b2sq ) / b2sq );
def norm2bin1( list_h, err_opt=None ):
  if type(list_h) != list:
    list_h = [list_h]

  for h in list_h:
    thebin = h.GetBinContent(1)
    theerr = h.GetBinError(1)

    if thebin == 0:
      thebin = 1

    if err_opt == None:
      h.Scale( 1 / thebin, 'width' )
    elif err_opt == 'binomial':
      for i in range(1,h.GetNbinsX()+1):
        if i == 1:
          h.SetBinContent(i,1.0)
          h.SetBinError(i,0.0) # bin1 all evts, eff=100% err=0%
        else:
          _bin_c = h.GetBinContent(i)
          _bin_e = h.GetBinError(i)

          b1 = _bin_c
          b2 = thebin
          b1sq = b1 * b1
          b2sq = b2 * b2
          e1sq = _bin_e * _bin_e
          e2sq = theerr * theerr
          __err = sqrt(abs( ( (1. - 2.* b1 / b2) * e1sq  + b1sq * e2sq / b2sq ) / b2sq ))

          h.SetBinContent(i,_bin_c / thebin)
          h.SetBinError(i,__err)
    else:
      h.Scale( 1 / thebin, 'width' )

# set histogram style
def hstyle( list_h, list_style ):
  for i,h in enumerate(list_h):
    sty = list_style[i]
    h.SetLineStyle( sty[0] )
    h.SetLineWidth( sty[1] )
    h.SetLineColor( sty[2] )
    h.SetMarkerSize( sty[3] )

# rebin histogram
# binning is None, then only change range
def hrebin( list_h, binning, xrange=[] ):

  # only for the case of new un-even bins
  _list_h_newbins = []

  for h in list_h:

    if binning is None:
      pass # no action on rebinning
    elif type(binning) is not list:
      # simply merge bins
      h.Rebin( binning )
      if len(xrange) !=0:
        h.SetAxisRange( xrange[0], xrange[1], 'X')
    else:
      # rebin with bin edges
      _c_binning = (ctypes.c_double * len(binning))(*binning)
      _h_newbins = h.Rebin( len(binning)-1, h.GetName(), _c_binning )
      _h_newbins.Scale(1, 'width')
      if len(xrange) !=0:
        _h_newbins.SetAxisRange( xrange[0], xrange[1], 'X')
      _list_h_newbins.append( _h_newbins )

    if len(xrange) !=0:
      h.SetAxisRange( xrange[0], xrange[1], 'X')

  return _list_h_newbins

# range histogram
def hrange( list_h, xmin, xmax ):
  for h in list_h:
    h.GetXaxis().SetRange( h.FindBin(xmin), h.FindBin(xmax) )

# find y max from list of histograms
# return: max and where it comes from
def findmax( list_h ):
  maxh = list_h[0]
  ymax = maxh.GetBinContent( maxh.GetMaximumBin() )
  for h in list_h:
    _ymax = h.GetBinContent( h.GetMaximumBin() )
    if ymax < _ymax :
      ymax = _ymax 
      maxh = h
  return ymax, maxh

def findmin( list_h ):
  minh = list_h[0]
  ymin = minh.GetBinContent( minh.GetMinimumBin() )
  for h in list_h:
    _ymin = h.GetBinContent( h.GetMinimumBin() )
    if ymin > _ymin:
      ymin = _ymin
      minh = h
  return ymin, minh

# used for setting logy min where it cannot be 0 or negative
# TODO it returns 0 when min=0, which cannot work for logy
def findmin_abs( list_h ):
  minh = list_h[0]
  ymin = abs(minh.GetBinContent( minh.GetMinimumBin() ))
  for h in list_h:
    _ymin = abs(h.GetBinContent( h.GetMinimumBin() ))
    if ymin > _ymin:
      ymin = _ymin
      minh = h
  return ymin, minh

# input: list of hisgrams, list of their entry names
def mklegend( list_h_nm, legend_title=None, legendx=None, atlas_label='Internal' ):
  R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
  R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")

  list_h, list_nm = list_h_nm
  ymax, maxh = findmax( list_h )
  to_right = False if maxh.FindFirstBinAbove( ymax*0.9 )>maxh.GetNbinsX()/2 else True
  print('mklegend {0} {1} {2}'.format(maxh.FindFirstBinAbove( ymax*0.9 ), maxh.GetNbinsX()/2, to_right) )

  x1 = 0.7 if to_right else 0.2
  if legendx is not None:
    x1 = legendx if to_right else 0.9 - legendx
  y1_leg_up = 0.80
  #
  y1_leg_dn = y1_leg_up - 0.07*len(list_h) - 0.07
  leg = R.TLegend( x1, y1_leg_dn, x1+0.2, y1_leg_up )
  leg.SetBorderSize(0)
  leg.SetTextSize(0.04)
  for i,h in enumerate(list_h):
    leg.AddEntry( h, list_nm[i], 'l' )
  if legend_title is not None:
    leg.SetHeader( legend_title )
  leg.Draw()
  #
  R.myText(x1,y1_leg_up+0.02,1,'#sqrt{s}= 13 TeV')
  R.ATLASLabel(x1,y1_leg_up++0.02+0.05, atlas_label )
  return leg

# obtain histogram names from list of file names
def listhnm( list_fnm ):
  f0 = R.TFile( list_fnm[0] )
  list_hnm = []
  inext = R.TIter( f0.GetListOfKeys() )
  while True:
    ikey = inext()
    if ikey == None : break
    cl = R.gROOT.GetClass( ikey.GetClassName() )
    if not cl.InheritsFrom("TH1"):
      continue
    if cl.InheritsFrom("TH2"): # do not look at 2D histograms
      continue
    h = ikey.ReadObj()
    print('Found histogram with name {0}'.format(h.GetName()))
    list_hnm.append( h.GetName() )
  return list_hnm

# fetch histgrams with same name from list of files
def listh( hnm, list_filenm ):
  list_h = []
  list_f = []
  for fnm in list_filenm:
    f = R.TFile( fnm )
    list_h.append( f.Get(hnm) )
    list_f.append( f )
  return list_h, list_f # return f list as well to keep them alive

# fatch hisotgrams with different names from a same file
# dfsh: different histograms same file
def listh_dhsf( list_hnm, filenm ):
  f = R.TFile( filenm )
  list_h = []
  for hnm in list_hnm:
    list_h.append( f.Get(hnm) )
  return list_h, f

# linear combination for lambda variation
# for histogram operation
# scheme "0,1,2" "0,1,20"
# kl kt: kappa lambda and top
# basis: [A,B,C] basis histograms to combine according to scheme
# ordered by kl, e.g. basis_kl=[0, 1, 2] for '0,1,2'
def lc_lambda( scheme, kl, kt, basis ):
  h = basis[0].Clone('lckl{0}kt{1}'.format(kl,kt))
  for i in range(1,h.GetNbinsX()+1):
    c,e = _lc_lambda( scheme, kl, kt, [bas.GetBinContent(i) for bas in basis], [ bas.GetBinError(i) for bas in basis] )
    h.SetBinContent( i, c )
    h.SetBinError( i, e )
  return h

# for scalar operation of lc_lambda
# basis value; basis error
# return combined output and error
def _lc_lambda( scheme, kl, kt, basis, basis_e ):
  A,B,C = basis
  A_e,B_e,C_e = basis_e
  res = 0
  res_e = 0

  A_coef = 0
  B_coef = 0
  C_coef = 0
  if scheme == '0,1,2':
    A_coef = kt*kt - 3*kt*kl/2. + kl*kl/2.
    B_coef = 2*kt*kl - kl*kl
    C_coef = -kt*kl/2. + kl*kl/2.
  if scheme == '0,1,20':
    A_coef = kt*kt - 399*kt*kl/380. + kl*kl/20.
    B_coef = 20*kt*kl/19. - kl*kl/19.
    C_coef = -kt*kl/380. + kl*kl/380.
  else:
    print('{0} is NOT FOUND'.format(scheme))
    sys.exit(1)

  res = kt*kt*( A_coef*A + B_coef*B + C_coef*C )
  res_e = A_coef*A_coef*A_e*A_e + B_coef*B_coef*B_e*B_e + C_coef*C_coef*C_e*C_e
  res_e = kt*kt*res_e**0.5
  return res,res_e

