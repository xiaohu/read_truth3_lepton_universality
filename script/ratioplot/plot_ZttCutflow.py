import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

### inputs ###
## +
#outpdf = 'Ztt_Cutflow_cut_z_mass_0.pdf'
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/cut_z_mass_0/PowhegPythia8_Ztautau_cutflow.root',
#                  '../anatree/cut_z_mass_0/Sherpa221_Ztautau_cutflow.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]
#legend_title  =  'No truth cut'
## +
#outpdf = 'Ztt_Cutflow_cut_z_mass_60.pdf'
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/cut_z_mass_60/PowhegPythia8_Ztautau_cutflow.root',
#                  '../anatree/cut_z_mass_60/Sherpa221_Ztautau_cutflow.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]
#legend_title  =  'Truth m(Z)>60GeV'
## +
#outpdf = 'Ztt_Cutflow_cut_z_mass_60_rwtSherpa2Powheg.pdf'
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'PowhegPythia8',
#                  'Sherpa221' ]
#list_filenm   = [ '../anatree/cut_z_mass_60/PowhegPythia8_Ztautau_cutflow.root',
#                  '../anatree/cut_z_mass_60_rwtSherpa2Powheg/Sherpa221_Ztautau_cutflow.root', ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]
#legend_title  =  'Truth m(Z)>60GeV, Sherpa rwt to Powheg'
### +
#outpdf = 'Ztt_RZ_Cutflow_cut_z_mass_60.pdf'
outpdf = 'Ztt_RZ_Cutflow_cut_z_mass_60_rwtSherpa2Powheg.pdf'
#outpdf = 'Ztt_RZ_Cutflow_cut_z_mass_60_mc16a.pdf'
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'PowhegPythia8',
                  #'Sherpa221' ]
                  'Sherpa221 rwt to PhPy8 by ZpT' ]
list_filenm   = [ '../anatree/Ztt_RZ_PowhegPythia8_Ztt_mZ60_hist.root',
                  #'../anatree/Ztt_RZ_Sherpa221_Ztt_mZ60_hist.root', ]
                  '../anatree/Ztt_RZ_Sherpa221_Ztt_mZ60_ZPTrwt_hist.root', ]
#list_filenm   = [ '../anatree/Ztt_RZ_PowhegPythia8_Ztt_mZ60_hist_mc16a.root',
#                  '../anatree/Ztt_RZ_Sherpa221_Ztt_mZ60_hist_mc16a.root', ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
legend_title  =  'Truth m(Z)>60GeV'
### inputs ###

# get all histogram names
#list_histogram_names = listhnm( list_filenm )
list_histogram_names = [
#  'Ztautau_etau_1prong_cutflow_weight',
#  'Ztautau_etau_3prong_cutflow_weight',
#  'Ztautau_mutau_1prong_cutflow_weight',
#  'Ztautau_mutau_3prong_cutflow_weight',
#  'Ztautau_emu_cutflow_weight',
##  'Ztautau_etau_cutflow_weight',
##  'Ztautau_mutau_cutflow_weight',

  # for RZ only
  'h_ltau',
  'h_emu',
  'h_RZ',
]
 
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))


  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  # comment out only for RZ
  #norm2bin1(list_h,'binomial')
  # set styles
  hstyle(list_h, list_style)

  islogy = True
  #islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  #c, _aux_c = mkcanvas( [list_h,list_title], bottompane = 0.5, ratiorange=[0.85,1.15], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # only for RZ
  c, _aux_c = mkcanvas( [list_h,list_title], xtitle='', ytitle='R(Z)', bottompane = 0.5, ratiorange=[0.85,1.15], legend_title=legend_title, legendx=0.55, atlas_label=atlas_label, islogy=islogy, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )
