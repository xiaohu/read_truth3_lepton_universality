import ROOT as R
from ratioplotlib.mkcanvas import *
from ratioplotlib.util import *

#../LHE/HH_PWG_FT/FTapprox-lambda01-hist.root
#../LHE/HH_PWG_FT/FT-lambda00-hist.root
#../LHE/HH_PWG_FT/FT-lambda01-hist.root
#../LHE/HH_PWG_FT/FT-lambda02.5-hist.root
#../LHE/HH_PWG_FT/FT-lambda-05-hist.root
#../LHE/HH_PWG_FT/FT-lambda05-hist.root
#../LHE/HH_PWG_FT/FT-lambda10-hist.root

### inputs ###
# +
#outpdf = 'HH_FT-lambda01_FTapprox-lambda01.pdf'
#legend_title  =  'NLO Powheg-Box-V2' #None#
#atlas_label = 'Internal' # 'Generator-level'
#list_title    = [ 'FT #lambda=1',
#                  'FTApprox #lambda=1' ]
#list_filenm   = [ '../LHE/HH_PWG_FT/FT-lambda01-hist.root',
#                  '../LHE/HH_PWG_FT/FTapprox-lambda01-hist.root' ]
#list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
#                  [ 1, 2, R.kRed, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]
# +
#outpdf = 'HH_FTapprox-Powheg-MG5aMCatNLO.pdf'
#legend_title  =  'FTApprox #lambda=1' #None#
#list_title    = [ 'Powheg-Box-V2',
#                  'MG5_aMC@NLO 2.2.3' ]
#list_filenm   = [ '../LHE/HH_PWG_FT/FTapprox-lambda01-hist.root',
#                  '../LHE/HH_MG5aMCatNLO_FTapprox/FTapprox-lambda01-hist.root' ]
#list_style    = [ [ 1, 2, R.kBlack, 0 ], # style, width, color, marker size
#                  [ 2, 2, R.kBlue, 0 ] ]
#list_draw_opt = [ 'hist',
#                  'hist' ]

# +
lambdastr='-05' # '10' '02.5' # look at names in ../LHE/HH_PWG_FT/
lambdavalue='-5'
outpdf = 'HH_lambda{0}-PowhegFT-MG5LO.pdf'.format(lambdastr)
legend_title  =  '#lambda = {0}'.format(lambdavalue) #None#
atlas_label = 'Internal' # 'Generator-level'
list_title    = [ 'Powheg-Box-V2 FT',
                  'MG5 LO 2.3.3' ]
list_filenm   = [ '../LHE/HH_PWG_FT/FT-lambda{0}-hist.root'.format(lambdastr),
                  '../LHE/HH_MG5_LO_forlambda/FT-lambda{0}-hist.root'.format(lambdastr) ]
list_style    = [ [ 1, 2, R.kBlue, 0 ], # style, width, color, marker size
                  [ 1, 2, R.kRed, 0 ] ]
list_draw_opt = [ 'hist',
                  'hist' ]
### inputs ###

# get all histogram names
list_histogram_names = listhnm( list_filenm )
print(list_histogram_names)

# ATLAS style
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasStyle.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasLabels.C")
R.gROOT.LoadMacro("./ratioplotlib/atlasstyle/AtlasUtils.C")
R.SetAtlasStyle()

for i,hnm in enumerate(list_histogram_names):
  print("Processing histogram {0}".format(hnm))

  # get list of histograms
  list_h, __list_f = listh( hnm, list_filenm )
  print(list_h)
  # normalisat to 1
  norm2unity(list_h)
  # set styles
  hstyle(list_h, list_style)
  # rebin if needed
  if 'H1_pT' in hnm or 'H1_E' in hnm : hrebin(list_h, 20)
  if 'H2_pT' in hnm or 'H2_E' in hnm : hrebin(list_h, 20)
  if '_eta' in hnm or '_phi' in hnm : hrebin( list_h, 4)
  if 'HH_pT' in hnm : hrebin(list_h, 10)
  if 'HH_E' in hnm : hrebin(list_h, 40)
  if 'HH_m' in hnm :
    _HH_m_binning = range(0,600, 20) + range(600,1040,40)
    list_h = hrebin(list_h, _HH_m_binning, [250,799])
  if 'HH_dEta' in hnm  or 'HH_dPhi' in hnm or 'HH_dR' in hnm : hrebin(list_h, 2)
  if 'gq_pT' in hnm : hrebin(list_h, 10)
  if 'gq_E' in hnm : hrebin(list_h, 20)
  if 'weight' in hnm :
    hrange(list_h, -0.5, 0.5 )
    for _i,_h in enumerate(list_h):
      print( 'Weight pos:neg [{0}] {1} {2} '.format( list_title[_i], _h.Integral(1,_h.FindBin(0)-1), _h.Integral(_h.FindBin(0),_h.GetNbinsX()) ) )
  islogy = True if 'HH_pT' in hnm else False # compare Powheg MG5_aMC@NLO only

  # make canvas with ratio
  c, _aux_c = mkcanvas( [list_h,list_title], legend_title=legend_title, legendx=0.55, islogy=islogy, atlas_label=atlas_label, draw_opt=list_draw_opt)
  # write to pdf
  if i == 0: c.Print( outpdf+'(' )
  if i > 0 and i < len(list_histogram_names) - 1: c.Print( outpdf )
  if i == len(list_histogram_names) - 1: c.Print( outpdf+')' )
