// use HGamSamples to get xsec kfactor etc. xsec-lu/xs_mc16_lu.txt
// use get_sumw.C to get sumw of all MC events
// manually copy and multiply here
//
map<int, double> map_dsid_xsecw;
double lumi = 139*1000; // ifb, this does not matter for truth studies

void init_tab_xsecweight( const char* mc ){

// crossSection*kFactor*genFiltEff / sumW * lumi

if( TString(mc)=="mc16a" ){
  lumi = 36.1*1000;
  map_dsid_xsecw[364128] = 1.9820E+00 * 0.9751 * 8.2142E-01 / 5376035     * lumi;
  map_dsid_xsecw[364129] = 1.9819E+00 * 0.9751 * 1.1343E-01 / 2869442     * lumi;
  map_dsid_xsecw[364130] = 1.9821E+00 * 0.9751 * 6.5432E-02 / 4114442.25  * lumi;
  map_dsid_xsecw[364131] = 1.1066E-01 * 0.9751 * 6.9203E-01 / 2190964     * lumi;
  map_dsid_xsecw[364132] = 1.1046E-01 * 0.9751 * 1.8905E-01 / 731614.1875 * lumi;
  map_dsid_xsecw[364133] = 1.1067E-01 * 0.9751 * 1.1851E-01 / 2090757.25  * lumi;
  map_dsid_xsecw[364134] = 4.0756E-02 * 0.9751 * 6.1697E-01 / 2959723.25  * lumi;
  map_dsid_xsecw[364135] = 4.0716E-02 * 0.9751 * 2.3280E-01 / 2001155.375 * lumi;
  map_dsid_xsecw[364136] = 4.0746E-02 * 0.9751 * 1.5026E-01 / 3472626     * lumi;
  map_dsid_xsecw[364137] = 8.6726E-03 * 0.9751 * 5.6407E-01 / 6854242     * lumi;
  map_dsid_xsecw[364138] = 8.6757E-03 * 0.9751 * 2.6622E-01 / 918259.625  * lumi;
  map_dsid_xsecw[364139] = 8.6755E-03 * 0.9751 * 1.7537E-01 / 1860876     * lumi;
  map_dsid_xsecw[364140] = 1.8078E-03 * 0.9751 * 1.0000E+00 / 2978478.5   * lumi;
  map_dsid_xsecw[364141] = 1.4826E-04 * 0.9751 * 1.0000E+00 / 1018271.25  * lumi;
}
if( TString(mc)=="mc16d" ){
  lumi = 44.4*1000;
  map_dsid_xsecw[364128] = 1.9820E+00 * 0.9751 * 8.2142E-01 / 6726281.5   * lumi;
  map_dsid_xsecw[364129] = 1.9819E+00 * 0.9751 * 1.1343E-01 / 3532960     * lumi;
  map_dsid_xsecw[364130] = 1.9821E+00 * 0.9751 * 6.5432E-02 / 5137387.5   * lumi;
  map_dsid_xsecw[364131] = 1.1066E-01 * 0.9751 * 6.9203E-01 / 2755702.5   * lumi;
  map_dsid_xsecw[364132] = 1.1046E-01 * 0.9751 * 1.8905E-01 / 903049.6875 * lumi;
  map_dsid_xsecw[364133] = 1.1067E-01 * 0.9751 * 1.1851E-01 / 2628966.25  * lumi;
  map_dsid_xsecw[364134] = 4.0756E-02 * 0.9751 * 6.1697E-01 / 3734850.5   * lumi;
  map_dsid_xsecw[364135] = 4.0716E-02 * 0.9751 * 2.3280E-01 / 2500530     * lumi;
  map_dsid_xsecw[364136] = 4.0746E-02 * 0.9751 * 1.5026E-01 / 4324785     * lumi;
  map_dsid_xsecw[364137] = 8.6726E-03 * 0.9751 * 5.6407E-01 / 8620319     * lumi;
  map_dsid_xsecw[364138] = 8.6757E-03 * 0.9751 * 2.6622E-01 / 1133852.75  * lumi;
  map_dsid_xsecw[364139] = 8.6755E-03 * 0.9751 * 1.7537E-01 / 2343161.5   * lumi;
  map_dsid_xsecw[364140] = 1.8078E-03 * 0.9751 * 1.0000E+00 / 3723236     * lumi;
  map_dsid_xsecw[364141] = 1.4826E-04 * 0.9751 * 1.0000E+00 / 1272900.25  * lumi;
}
if( TString(mc)=="mc16e" ){
  lumi = 58.5*1000;
  map_dsid_xsecw[364128] = 1.9820E+00 * 0.9751 * 8.2142E-01 / 8932068     * lumi;
  map_dsid_xsecw[364129] = 1.9819E+00 * 0.9751 * 1.1343E-01 / 4771039.5   * lumi;
  map_dsid_xsecw[364130] = 1.9821E+00 * 0.9751 * 6.5432E-02 / 6843571     * lumi;
  map_dsid_xsecw[364131] = 1.1066E-01 * 0.9751 * 6.9203E-01 / 3656688.5   * lumi;
  map_dsid_xsecw[364132] = 1.1046E-01 * 0.9751 * 1.8905E-01 / 1209846.375 * lumi;
  map_dsid_xsecw[364133] = 1.1067E-01 * 0.9751 * 1.1851E-01 / 3476879.75  * lumi;
  map_dsid_xsecw[364134] = 4.0756E-02 * 0.9751 * 6.1697E-01 / 4966304     * lumi;
  map_dsid_xsecw[364135] = 4.0716E-02 * 0.9751 * 2.3280E-01 / 3329676.25  * lumi;
  map_dsid_xsecw[364136] = 4.0746E-02 * 0.9751 * 1.5026E-01 / 5756186.5   * lumi;
  map_dsid_xsecw[364137] = 8.6726E-03 * 0.9751 * 5.6407E-01 / 11374047    * lumi;
  map_dsid_xsecw[364138] = 8.6757E-03 * 0.9751 * 2.6622E-01 / 1532853.875 * lumi;
  map_dsid_xsecw[364139] = 8.6755E-03 * 0.9751 * 1.7537E-01 / 3119964.25  * lumi;
  map_dsid_xsecw[364140] = 1.8078E-03 * 0.9751 * 1.0000E+00 / 4922786.5   * lumi;
  map_dsid_xsecw[364141] = 1.4826E-04 * 0.9751 * 1.0000E+00 / 1697854.875 * lumi;
}

}
