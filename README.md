# A framework for reading truth information used in lepton universality analyses

The framework can read truth informatoin from TRUTH3 derivation or the truth containers directly in STDM4.
The latter is mostly used as those are the same datasets in the reco analysis.

The supported processes and event generators:

- Ztautau PowhegPythia8, Sherpa221
- tt PowhegPythia8


# Setup and compile

## For the first time

Go to a clean directory and set up in the following way.

```
mkdir build run source
cd source
git clone ssh://git@gitlab.cern.ch:7999/xiaohu/read_truth3_lepton_universality.git
```

Create CMakeLists.txt under source/ with the following contents

```
#
# Project configuration for UserAnalysis.
#

# Set the minimum required CMake version:
cmake_minimum_required( VERSION 3.4 FATAL_ERROR )

# Try to figure out what project is our parent. Just using a hard-coded list
# of possible project names. Basically the names of all the other
# sub-directories inside the Projects/ directory in the repository.
set( _parentProjectNames Athena AthenaP1 AnalysisBase AthAnalysis
   AthSimulation AthDerivation AnalysisTop )
set( _defaultParentProject AnalysisBase )
foreach( _pp ${_parentProjectNames} )
   if( NOT "$ENV{${_pp}_DIR}" STREQUAL "" )
      set( _defaultParentProject ${_pp} )
      break()
   endif()
endforeach()

# Set the parent project name based on the previous findings:
set( ATLAS_PROJECT ${_defaultParentProject}
   CACHE STRING "The name of the parent project to build against" )

# Clean up:
unset( _parentProjectNames )
unset( _defaultParentProject )

# Find the AnalysisBase project. This is what, amongst other things, pulls
# in the definition of all of the "atlas_" prefixed functions/macros.
find_package( ${ATLAS_PROJECT} REQUIRED )

# Set up CTest. This makes sure that per-package build log files can be
# created if the user so chooses.
atlas_ctest_setup()

# Set up the GitAnalysisTutorial project. With this CMake will look for "packages"
# in the current repository and all of its submodules, respecting the
# "package_filters.txt" file, and set up the build of those packages.
atlas_project( UserAnalysis 1.0.0
   USE ${ATLAS_PROJECT} ${${ATLAS_PROJECT}_VERSION} )

# Set up the runtime environment setup script. This makes sure that the
# project's "setup.sh" script can set up a fully functional runtime environment,
# including all the externals that the project uses.
lcg_generate_env( SH_FILE ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh )
install( FILES ${CMAKE_BINARY_DIR}/${ATLAS_PLATFORM}/env_setup.sh
   DESTINATION . )

# Set up CPack. This call makes sure that an RPM or TGZ file can be created
# from the built project. Used by Panda to send the project to the grid worker
# nodes.
atlas_cpack_setup()
```

Then setup up the environment

```
# basic
cd build/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
asetup 21.2.100,AnalysisBase
```

Stay in build/ and compile the source codes
```
make -j4
source x86_64-*/setup.sh
```

# For everytime after

Once you have successfully compiled the codes in the first time, then everytime later becomes easier.
Stay in the root directory and do the following.

```
# basic
_rootdir=$(pwd)
cd build/
export ATLAS_LOCAL_ROOT_BASE=/cvmfs/atlas.cern.ch/repo/ATLASLocalRootBase
source ${ATLAS_LOCAL_ROOT_BASE}/user/atlasLocalSetup.sh
# https://atlassoftwaredocs.web.cern.ch/ABtutorial/release_setup/
asetup 21.2.100,AnalysisBase

# once compilation exists
source x86_64-*/setup.sh

cd ${_rootdir}
```

# Usage

The steering file is written in python in `source/read_truth3/share/`.

## Local runs

To run Ztautau events, you need this steering file `source/read_truth3/share/Zll_eljob.py`.
To do it locally, you need to uncomment these lines
```
# local runs
inputFilePath = '/eos/user/x/xiaohu/LU/dataset/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975/'
if options.localinput != "local input":
  inputFilePath = '/eos/user/x/xiaohu/LU/dataset/'+options.localinput
ROOT.SH.ScanDir().filePattern( 'DAOD_STDM4*.pool.root.1' ).scan( sh, inputFilePath )
...
# local runs
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
```
while comment out these
```
# grid runs
ROOT.SH.scanRucio (sh, options.gridinput)
...
mc=''
if 'r9364' in options.gridinput:
  mc='a'
if 'r10201' in options.gridinput:
  mc='d'
if 'r10724' in options.gridinput:
  mc='e'
# grid runs
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv18{0}.%in:name[2]%.%in:name[6]%".format(mc))
driver.submitOnly( job, options.submission_dir )
```
And don't forget to download a piece of dataset to your disk as the input.

To run tt events, you need this steering file `source/read_truth3/share/Wlv_eljob.py`.
Similarly, to run it locally uncomment these
```
# local runs
inputFilePath = '/eos/user/x/xiaohu/LU/dataset/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_s3126_r9364_p4097/'
ROOT.SH.ScanDir().filePattern( 'DAOD_STDM4*.pool.root.1' ).scan( sh, inputFilePath )
...
# local runs
driver = ROOT.EL.DirectDriver()
driver.submit( job, options.submission_dir )
```
while comment out these
```
# grid runs
ROOT.SH.scanRucio (sh, "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r10724_p4097")
...
# grid runs
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv20e.%in:name[2]%.%in:name[6]%")
driver.submitOnly( job, options.submission_dir )
```

Then you can run a local job with
```
Zll_eljob.py --submission-dir=output_Zll
```
for Ztautau events with a fixed input in the steering file,
```
Zll_eljob.py -e powheg --submission-dir=output_Zll -l mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975
```
for Ztautau events with the inputs specified in the command line,
```
Wlv_eljob.py --submission-dir=output_Wlv
```
for tt events with a fixed input in the steering file.


## Grid runs

As described above, uncomment the opposite of local runs, then you can run on the grid.
The steering files will not be repeated here.
We have a look at the command lines for grid runs.
```
Zll_eljob.py -e powheg --submission-dir=output_Zll -g mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975
Zll_eljob.py -e powheg --submission-dir=output_Zll -g mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r10201_p3975
Zll_eljob.py -e powheg --submission-dir=output_Zll -g mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r10724_p3975
```
for running all MC16a/d/e Ztautau PowhegPythia8 samples on the grid,
```
for mc16 in r9364 r10201 r10724
do

Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364137.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CVetoBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364128.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CVetoBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097

Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364129.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_CFilterBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364130.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV0_70_BFilter.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364131.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CVetoBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364132.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_CFilterBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364133.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV70_140_BFilter.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364134.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CVetoBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364135.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_CFilterBVeto.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364136.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV140_280_BFilter.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364138.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_CFilterBVeto.deriv.DAOD_STDM4.e5313_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364139.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV280_500_BFilter.deriv.DAOD_STDM4.e5313_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364140.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV500_1000.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097
Zll_eljob.py -e sherpa --submission-dir=output_Zll -g mc16_13TeV.364141.Sherpa_221_NNPDF30NNLO_Ztautau_MAXHTPTV1000_E_CMS.deriv.DAOD_STDM4.e5307_s3126_${mc16}_p4097

done
```
for running all the Ztautau Sherpa221 samples on the grid,
```
Wlv_eljob.py --submission-dir=output_Wlv
```
for running the tt samples on the grid, with the fixed input in the steering file.

Don't forget to setup the output file name pattern, for example Ztautau
```
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv18{0}.%in:name[2]%.%in:name[6]%".format(mc))
```
needs setting differently (such as testv18 testv19 ...) for a new round of production after some updates in the codes implemented.
For tt, it is this row
```
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv20e.%in:name[2]%.%in:name[6]%")
```


# Structure of the framework

The framework is designed in the following structure.

```
├── CMakeLists.txt
├── data
│   ├── em_MC_event_number_Sherpa_Ztt.root
│   ├── em_MC_event_number_Ztt.root
│   ├── et_MC_event_number_Sherpa_Ztt.root
│   ├── et_MC_event_number_Ztt.root
│   ├── mt_MC_event_number_Sherpa_Ztt.root
│   ├── mt_MC_event_number_Ztt.root
│   ├── tt_em_MC_event_number.root
│   ├── tt_et_MC_event_number.root
│   └── tt_mt_MC_event_number.root
├── README.md
├── read_truth3
│   ├── Cutflow.h
│   ├── read_truth3Dict.h
│   ├── selection.xml
│   ├── Tree_Wlv.h
│   ├── Tree_Zll.h
│   ├── Wlv.h
│   └── Zll.h
├── Root
│   ├── Cutflow.cxx
│   ├── Tree_Wlv.cxx
│   ├── Tree_Zll.cxx
│   ├── Wlv.cxx
│   └── Zll.cxx
├── share
│   ├── Wlv_eljob.py
│   └── Zll_eljob.py
└── src
```

The latest EventLoop libraries are used as base classes.
Two algorithms are developped separately for Ztautau and tt processes.
Their impelementation are in
```
 Wlv.cxx
 Zll.cxx
```
whose function is to capture the truth production and decay that interest us,
and to make event selections as well as the tree filling.

This piece of codes defines the squential selections in tt
```
  if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWej ||  isWmuj ){

    // event selections
        pass_all = 1;

        //
        if( isWetauHad_1prong || isWetauHad_3prong || isWemu || isWej ){
                e_pT = electron->pt()*GeV;
                e_eta = electron->eta();
                e_phi = electron->phi();
                e_E = electron->e()*GeV;
                pass_e_pT = ( e_pT > 27 );
                pass_all *= pass_e_pT;
        if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("Electron pT",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("Electron pT",1,weight);
                  if(isWemu) cf_Wemu("Electron pT",1,weight);
                  if(isWej) cf_Wej("Electron pT",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Electron pT",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Electron pT",1,weight_fsr_muR05);
                  if(isWemu) cf_Wemu_fsr_muR05("Electron pT",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("Electron pT",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Electron pT",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Electron pT",1,weight_fsr_muR20);
                  if(isWemu) cf_Wemu_fsr_muR20("Electron pT",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("Electron pT",1,weight_fsr_muR20);
                }
        }

        //
        if( isWetauHad_1prong || isWetauHad_3prong || isWemu || isWej ){
                double _e_eta = abs( e_eta ); // abs!!!
                pass_e_eta = ( (_e_eta<1.37) || ( (_e_eta>1.52) && (_e_eta<2.47) ) );
                pass_all *= pass_e_eta;
                if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("Electron eta",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("Electron eta",1,weight);
                  if(isWemu) cf_Wemu("Electron eta",1,weight);
                  if(isWej) cf_Wej("Electron eta",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Electron eta",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Electron eta",1,weight_fsr_muR05);
                  if(isWemu) cf_Wemu_fsr_muR05("Electron eta",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("Electron eta",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Electron eta",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Electron eta",1,weight_fsr_muR20);
                  if(isWemu) cf_Wemu_fsr_muR20("Electron eta",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("Electron eta",1,weight_fsr_muR20);
                }
        }

        //
        if( isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWmuj ){
                m_pT = muon->pt()*GeV;
                m_eta = muon->eta();
                m_phi = muon->phi();
                m_E = muon->e()*GeV;
                pass_mu_pT = ( m_pT > 27 );
                pass_all *= pass_mu_pT;
        if(pass_all){
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Muon pT",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Muon pT",1,weight);
                  if(isWemu) cf_Wemu("Muon pT",1,weight);
                  if(isWmuj) cf_Wmuj("Muon pT",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Muon pT",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Muon pT",1,weight_fsr_muR05);
                  if(isWemu) cf_Wemu_fsr_muR05("Muon pT",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("Muon pT",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Muon pT",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Muon pT",1,weight_fsr_muR20);
                  if(isWemu) cf_Wemu_fsr_muR20("Muon pT",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("Muon pT",1,weight_fsr_muR20);
                }
        }

        //
        if( isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWmuj ){
                double _mu_eta = abs( m_eta ); // abs!!!
                pass_mu_eta = ( _mu_eta<2.5 );
                pass_all *= pass_mu_eta;
        if(pass_all){
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Muon eta",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Muon eta",1,weight);
                  if(isWemu) cf_Wemu("Muon eta",1,weight);
                  if(isWmuj) cf_Wmuj("Muon eta",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Muon eta",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Muon eta",1,weight_fsr_muR05);
                  if(isWemu) cf_Wemu_fsr_muR05("Muon eta",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("Muon eta",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Muon eta",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Muon eta",1,weight_fsr_muR20);
                  if(isWemu) cf_Wemu_fsr_muR20("Muon eta",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("Muon eta",1,weight_fsr_muR20);
                }
        }

        //
        if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej || isWm uj ){
                t_pT = tau_had->Pt()*GeV;
                t_eta = tau_had->Eta();
                t_phi = tau_had->Phi();
                t_E = tau_had->E()*GeV;
                pass_tau_pT = ( t_pT > 25 );
                pass_all *=  pass_tau_pT;
                if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("Tau pT",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("Tau pT",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Tau pT",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Tau pT",1,weight);
                  if(isWej) cf_Wej("Tau pT",1,weight);
                  if(isWmuj) cf_Wmuj("Tau pT",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("Tau pT",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("Tau pT",1,weight_fsr_muR20);
                }
        }

        //
        if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej || isWm uj ){
                double _tau_eta = abs( t_eta ); // abs!!!
                pass_tau_eta = ( (_tau_eta<1.37) || ((_tau_eta>1.52) && (_tau_eta<2.5)) );
                pass_all *= pass_tau_eta;
                if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("Tau eta",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("Tau eta",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Tau eta",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Tau eta",1,weight);
                  if(isWej) cf_Wej("Tau eta",1,weight);
                  if(isWmuj) cf_Wmuj("Tau eta",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("Tau eta",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("Tau eta",1,weight_fsr_muR20);
                }
        }

    // Jets
        auto jetorigin = get_jetorigin( jets ); // vector of jet origins
        n_ljet = get_njetx(jetorigin,0); // pt eta is considered
        n_bjet = get_njetx(jetorigin,5); // pt eta is considered
    n_jet = n_ljet+n_bjet; // not consider tau jets !!!

        // Bjets
        // get_nbjet consdiers pT eta already
        pass_bjet_n = ( n_bjet == 2 );
        pass_all *= pass_bjet_n;
        if(pass_all){

          auto bidx = get_idx_jetx(jetorigin,5);

          auto b0_p4 = jets->at( bidx[0] )->p4();
          b0_pT = b0_p4.Pt()*GeV;
          b0_eta = b0_p4.Eta();
          b0_phi = b0_p4.Phi();
          b0_E = b0_p4.E()*GeV;
          auto b1_p4 = jets->at( bidx[1] )->p4();
          b1_pT = b1_p4.Pt()*GeV;
          b1_eta = b1_p4.Eta();
          b1_phi = b1_p4.Phi();
          b1_E = b1_p4.E()*GeV;

          if(isWetauHad_1prong) cf_WetauHad_1prong("BJet n=2",1,weight);
          if(isWetauHad_3prong) cf_WetauHad_3prong("BJet n=2",1,weight);
          if(isWmutauHad_1prong) cf_WmutauHad_1prong("BJet n=2",1,weight);
          if(isWmutauHad_3prong) cf_WmutauHad_3prong("BJet n=2",1,weight);
          if(isWemu) cf_Wemu("BJet n=2",1,weight);
          if(isWej) cf_Wej("BJet n=2",1,weight);
          if(isWmuj) cf_Wmuj("BJet n=2",1,weight);
          if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWemu) cf_Wemu_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWej) cf_Wej_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWmuj) cf_Wmuj_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
          if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWemu) cf_Wemu_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWej) cf_Wej_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
          if(isWmuj) cf_Wmuj_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
        }

        // only with additional jets
        const xAOD::Jet* jet = nullptr;
        if( isWej || isWmuj ) jet = get_3rdjet_inWlj( jets, tau_had); // the jet faking tau case in Wej Wmuj
        else jet = get_3rdjet( jets ); // get third jet that is light after pT eta // this is the normal signal  case
        // nullptr means there is no 3rd jets after pT eta, on top of the 2 bjets
        if( !jet ){ // nullptr !!!
            // these events are also signals
                // have to put them into cutflow
                // thus pretend they pass the 3rd jet cuts
            pass_3rdjet = 1;
                pass_all *= pass_3rdjet;
                // fill in cutflow
        }else{ // have 3rd jet that is light
            pass_3rdjet = 1;
                pass_all *= pass_3rdjet;
                j3_pT = jet->pt()*GeV;
                j3_eta = jet->eta();
                j3_phi = jet->phi();
                j3_E = jet->e()*GeV;
                //
                TLorentzVector v_lep;
                if( isWetauHad_1prong || isWetauHad_3prong ) v_lep = electron->p4();
                if( isWmutauHad_1prong || isWmutauHad_3prong ) v_lep = muon->p4();
                auto v_j3 = jet->p4();
                dR_l_j3 = v_lep.DeltaR(v_j3);
        if(pass_all){
              if(isWetauHad_1prong) cf_WetauHad_1prong("Jet 3rd",1,weight);
              if(isWetauHad_3prong) cf_WetauHad_3prong("Jet 3rd",1,weight);
              if(isWmutauHad_1prong) cf_WmutauHad_1prong("Jet 3rd",1,weight);
              if(isWmutauHad_3prong) cf_WmutauHad_3prong("Jet 3rd",1,weight);
              if(isWej) cf_Wej("Jet 3rd",1,weight);
              if(isWmuj) cf_Wmuj("Jet 3rd",1,weight);
              //if(isWemu) cf_Wemu("Jet 3rd",1,weight); // no 3rd cut on emu
              if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              if(isWej) cf_Wej_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              if(isWmuj) cf_Wmuj_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
              //if(isWemu) cf_Wemu_fsr_muR05("Jet 3rd",1,weight_fsr_muR05); // no 3rd cut on emu
              if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              if(isWej) cf_Wej_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              if(isWmuj) cf_Wmuj_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
              //if(isWemu) cf_Wemu_fsr_muR20("Jet 3rd",1,weight_fsr_muR20); // no 3rd cut on emu
            }

                //
                if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej  || isWmuj ){
                        //TLorentzVector _tau_p4 = tau->p4();
                        TLorentzVector _jet_p4 = jet->p4();
                        m_taujet = (*tau_had+_jet_p4).M()*GeV;
                        pass_m_taujet = !( (m_taujet > 50) && (m_taujet<90) );
                        pass_all *= pass_m_taujet;
                if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("M(tau,jet)",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("M(tau,jet)",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("M(tau,jet)",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("M(tau,jet)",1,weight);
                  if(isWej) cf_Wej("M(tau,jet)",1,weight);
                  if(isWmuj) cf_Wmuj("M(tau,jet)",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
                }
                }

        }

        // 4th 5th jet, i.e. 2nd (index 1) 3rd (index 2) ljet
        if( isWej || isWmuj ) jet = get_3pXrdjet_inWlj( jets, tau_had, 1 ); // the jet faking tau case in Wej W muj
        else jet = get_3pXrdjet( jets, 1 ); // get third jet that is light after pT eta // this is the normal s ignal case
        if( jet ){
          j4_pT = jet->pt()*GeV;
          j4_eta = jet->eta();
          j4_phi = jet->phi();
          j4_E = jet->e()*GeV;
        }
        if( isWej || isWmuj ) jet = get_3pXrdjet_inWlj( jets, tau_had, 2 ); // the jet faking tau case in Wej W muj
        else jet = get_3pXrdjet( jets, 2 ); // get third jet that is light after pT eta // this is the normal s ignal case
        if( jet ){
          j5_pT = jet->pt()*GeV;
          j5_eta = jet->eta();
          j5_phi = jet->phi();
          j5_E = jet->e()*GeV;
        }


        // reco match
        if( isWetauHad_1prong || isWetauHad_3prong || isWej ){
                pass_reco_etau = ( selected_reco_etau(eventnumber) );
                pass_all *= pass_reco_etau;
                if(pass_all){
                  if(isWetauHad_1prong) cf_WetauHad_1prong("Reco etau",1,weight);
                  if(isWetauHad_3prong) cf_WetauHad_3prong("Reco etau",1,weight);
                  if(isWej) cf_Wej("Reco etau",1,weight);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Reco etau",1,weight_fsr_muR05);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Reco etau",1,weight_fsr_muR05);
                  if(isWej) cf_Wej_fsr_muR05("Reco etau",1,weight_fsr_muR05);
                  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Reco etau",1,weight_fsr_muR20);
                  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Reco etau",1,weight_fsr_muR20);
                  if(isWej) cf_Wej_fsr_muR20("Reco etau",1,weight_fsr_muR20);
                }
        }
        if( isWmutauHad_1prong || isWmutauHad_3prong || isWmuj ){
                pass_reco_mutau = ( selected_reco_mutau(eventnumber) );
                pass_all *= pass_reco_mutau;
                if(pass_all){
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Reco mutau",1,weight);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Reco mutau",1,weight);
                  if(isWmuj) cf_Wmuj("Reco mutau",1,weight);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
                  if(isWmuj) cf_Wmuj_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
                  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
                  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
                  if(isWmuj) cf_Wmuj_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
                }
        }
        if( isWemu ){
                pass_reco_emu = ( selected_reco_emu(eventnumber) );
                pass_all *= pass_reco_emu;
                if(pass_all){
                  cf_Wemu("Reco emu",1,weight);
                  cf_Wemu_fsr_muR05("Reco emu",1,weight_fsr_muR05);
                  cf_Wemu_fsr_muR20("Reco emu",1,weight_fsr_muR20);
                }
        }

  }
```

This piece of codes defines the squential selections in Ztautau
```
  // channels
  //
  // // // // // //

  pass_all = 1;
  if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("This channel", 1, weight);
  if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("This channel", 1, weight);
  if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("This channel", 1, weight);
  if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("This channel", 1, weight);
  if(isZtautau_emu) cf_Ztautau_emu("This channel", 1, weight);

  // pass e
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_emu ){
    // pT
    pass_e_pT = ( electron->pt()*GeV > 27 );
        pass_all *= pass_e_pT;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("e pT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("e pT", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("e pT", 1, weight);
        }
        // eta
        double _eta = abs(electron->eta());
        pass_e_eta = ( _eta<1.37 ) || ( (_eta>1.52) && (_eta<2.47) );
        pass_all *= pass_e_eta;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("e eta", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("e eta", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("e eta", 1, weight);
        }
  }

  // pass mu
  if( isZtautau_mutau_1prong || isZtautau_mutau_3prong || isZtautau_emu ){
    // pT
    pass_mu_pT = ( muon->pt()*GeV > 27 );
        pass_all *= pass_mu_pT;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("mu pT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("mu pT", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("mu pT", 1, weight);
        }
        // eta
        double _eta = abs(muon->eta());
        pass_mu_eta = ( _eta<2.5 );
        pass_all *= pass_mu_eta;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("mu eta", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("mu eta", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("mu eta", 1, weight);
        }
  }

  // pass tau
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    // pT
    pass_tau_pT = ( tau_had->Pt()*GeV > 25 );
        pass_all *= pass_tau_pT;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("tau pT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("tau pT", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("tau pT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("tau pT", 1, weight);
        }
        // eta
        double _eta = abs(tau_had->Eta());
        pass_tau_eta = ( _eta<1.37 ) || ( (_eta>1.52) && (_eta<2.5) );
        pass_all *= pass_tau_eta;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("tau eta", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("tau eta", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("tau eta", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("tau eta", 1, weight);
        }
  }

  // bjet, no overlap removal is needed
  // as e mu from W/Z/H/tau are not included
  // only count for pT>25 eta<2.5 !!! already in get_nbjet()
  pass_nbjet = ( get_nbjet( jets ) == 0 );
  pass_all *= pass_nbjet;
  if( pass_all ){
    if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("nb=0 pT eta", 1, weight);
    if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("nb=0 pT eta", 1, weight);
    if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("nb=0 pT eta", 1, weight);
    if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("nb=0 pT eta", 1, weight);
    if(isZtautau_emu) cf_Ztautau_emu("nb=0 pT eta", 1, weight);
  }

  //  pass_m3star
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    std::vector<double> _v_m3star = get_m3star( *tau_had, electron->p4(), electron->charge(), met );
        m3T = _v_m3star[0]*GeV;
        sinthetastar = _v_m3star[1];
        m3star = _v_m3star[2]*GeV;
        pass_m3star = ((m3star > 60) && (m3star < 110));
        pass_all *= pass_m3star;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("m3star", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("m3star", 1, weight);
        }
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    std::vector<double> _v_m3star = get_m3star( *tau_had, muon->p4(), muon->charge(), met );
        m3T = _v_m3star[0]*GeV;
        sinthetastar = _v_m3star[1];
        m3star = _v_m3star[2]*GeV;
        pass_m3star = ((m3star > 60) && (m3star < 110));
        pass_all *= pass_m3star;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("m3star", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("m3star", 1, weight);
        }
  }else if( isZtautau_emu ){
    std::vector<double> _v_m3star = get_m3star( electron->p4(), muon->p4(), muon->charge(), met );
        m3T = _v_m3star[0]*GeV;
        sinthetastar = _v_m3star[1];
        m3star = _v_m3star[2]*GeV;
        pass_m3star = ((m3star > 60) && (m3star < 110));
        pass_all *= pass_m3star;
        if( pass_all ){
      cf_Ztautau_emu("m3star", 1, weight);
        }
  }


  // pass_sumcosdphi
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    sumcosphi = get_sumcosphi( *tau_had, electron->p4(), met );
        pass_sumcosdphi = ( sumcosphi > -0.1 );
        pass_all *= pass_sumcosdphi;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("sumcosphi", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("sumcosphi", 1, weight);
        }
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    sumcosphi = get_sumcosphi( *tau_had, muon->p4(), met );
        pass_sumcosdphi = ( sumcosphi > -0.1 );
        pass_all *= pass_sumcosdphi;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("sumcosphi", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("sumcosphi", 1, weight);
        }
  }else if( isZtautau_emu ){
    sumcosphi = get_sumcosphi( electron->p4(), muon->p4(), met );
        pass_sumcosdphi = ( sumcosphi > -0.1 );
        pass_all *= pass_sumcosdphi;
        if( pass_all ){
      cf_Ztautau_emu("sumcosphi", 1, weight);
        }
  }


  // pass_at
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    aT = get_aT( *tau_had, electron->p4() )*GeV;
        pass_at = ( aT < 60 );
        pass_all *= pass_at;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("aT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("aT", 1, weight);
        }
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    aT = get_aT( *tau_had, muon->p4() )*GeV;
        pass_at = ( aT < 60 );
        pass_all *= pass_at;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("aT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("aT", 1, weight);
        }
  }else if( isZtautau_emu ){
    aT = get_aT( electron->p4(), muon->p4() )*GeV;
        pass_at = ( aT < 60 );
        pass_all *= pass_at;
        if( pass_all ){
      cf_Ztautau_emu("aT", 1, weight);
        }
  }


  // pass_tau1prong_eta for mutau only
  if( isZtautau_mutau_1prong ){
    pass_tau1prong_eta = (abs(tau_had->Eta()) > 0.1);
        pass_all *= pass_tau1prong_eta;
    if( pass_all ) cf_Ztautau_mutau_1prong("eta tau_had 1prong", 1, weight);
  }

  // pass_m_ll for etau only
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_mutau_1prong || isZtautau_mutau_3prong || isZtautau_emu ){
    TLorentzVector _ll;
        if( isZtautau_etau_1prong || isZtautau_etau_3prong ) _ll = ( *tau_had + electron->p4() );
        if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ) _ll = ( *tau_had + muon->p4() );
        if(isZtautau_emu)  _ll = ( electron->p4() + muon->p4() );
    double _m_ll = _ll.M()*GeV;
        pass_m_ll = ( _m_ll < 85 );
        pass_all *= pass_m_ll;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("m(l,l)", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("m(l,l)", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("m(l,l)", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("m(l,l)", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("m(l,l)", 1, weight);
        }
  }

  // pass reco
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    pass_reco = selected_reco_etau( eventnumber );
        pass_all *= pass_reco;
        if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("reco", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("reco", 1, weight);
        }
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    pass_reco = selected_reco_mutau( eventnumber );
        pass_all *= pass_reco;
        if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("reco", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("reco", 1, weight);
        }
  }else if( isZtautau_emu ){
    pass_reco = selected_reco_emu( eventnumber );
        pass_all *= pass_reco;
        if( pass_all ){
      cf_Ztautau_emu("reco", 1, weight);
        }
  }

  // check signal crossing
  pass_reco_etau = selected_reco_etau( eventnumber );
  pass_reco_mutau = selected_reco_mutau( eventnumber );
  pass_reco_emu = selected_reco_emu( eventnumber );
```

The output of the framework is ntuples, i.e. plain trees.
These trees record decay flags, event selection flags and many interesting kinematic variables.
So one can make distributions at any selection level and remake cutflow tables with the selection flags.
The skeleton of trees are defined separately in
```
 Tree_Wlv.cxx
 Tree_Zll.cxx
```

# Plot making

In the dir `script/`, C files are the scripts to produce histograms out of the trees produced by the framework
as described aove. Then the plots can be made out of the histograms by the pythonic scripts in `script/ratioplot/`.
The scripts are very self-explaining.
