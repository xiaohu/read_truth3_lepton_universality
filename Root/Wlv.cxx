#include <AsgTools/MessageCheck.h>
#include <read_truth3/Wlv.h>

SG::AuxElement::ConstAccessor<unsigned int> Wlv :: ptype("classifierParticleType");
SG::AuxElement::ConstAccessor<unsigned int> Wlv :: porigin("classifierParticleOrigin");
SG::AuxElement::ConstAccessor<int> Wlv :: flavor("HadronConeExclTruthLabelID");

Wlv :: Wlv (const std::string& name,
            ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
	  cf_WetauHad_1prong("WetauHad_1prong"),
	  cf_WetauHad_3prong("WetauHad_3prong"),
	  //cf_WetauLep("WetauLep"),
	  cf_WmutauHad_1prong("WmutauHad_1prong"),
	  cf_WmutauHad_3prong("WmutauHad_3prong"),
	  //cf_WmutauLep("WmutauLep"),
	  cf_Wemu("Wemu"),
	  cf_Wej("Wej"),
	  cf_Wmuj("Wmuj"),

	  cf_WetauHad_1prong_fsr_muR05("WetauHad_1prong_fsr_muR05"),
	  cf_WetauHad_3prong_fsr_muR05("WetauHad_3prong_fsr_muR05"),
	  cf_WmutauHad_1prong_fsr_muR05("WmutauHad_1prong_fsr_muR05"),
	  cf_WmutauHad_3prong_fsr_muR05("WmutauHad_3prong_fsr_muR05"),
	  cf_Wemu_fsr_muR05("Wemu_fsr_muR05"),
	  cf_Wej_fsr_muR05("Wej_fsr_muR05"),
	  cf_Wmuj_fsr_muR05("Wmuj_fsr_muR05"),
	  cf_WetauHad_1prong_fsr_muR20("WetauHad_1prong_fsr_muR20"),
	  cf_WetauHad_3prong_fsr_muR20("WetauHad_3prong_fsr_muR20"),
	  cf_WmutauHad_1prong_fsr_muR20("WmutauHad_1prong_fsr_muR20"),
	  cf_WmutauHad_3prong_fsr_muR20("WmutauHad_3prong_fsr_muR20"),
	  cf_Wemu_fsr_muR20("Wemu_fsr_muR20"),
	  cf_Wej_fsr_muR20("Wej_fsr_muR20"),
	  cf_Wmuj_fsr_muR20("Wmuj_fsr_muR20")
	  //tool_weight("PMGTools::PMGTruthWeightTool/tool_weight", this)
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  // not in use, store all events and mark type for each in TTree
  // channel: Wetau Wmutau (tt: W->ev / W->muv W->taunv); Wemu (tt: W->ev W->muv)
  //declareProperty( "channel", channel = "Wetau", "channels: Wetau Wmutau Wemu" );
  //isWetau = (channel == "Wetau");
  //isWmutau = (channel == "Wmutau");
  //isWemu = (channel == "Wemu");

  ctrWetauHad_1prong = 0;
  ctrWetauHad_3prong = 0;
  //ctrWetauLep = 0;
  ctrWmutauHad_1prong = 0;
  ctrWmutauHad_3prong = 0;
  //ctrWmutauLep = 0;
  ctrWemu = 0;
  //ctrWee = 0;
  //ctrWmumu = 0;
  //ctrWtautau = 0;
  ctrWej = 0;
  ctrWmuj = 0;
  //ctrWtauj = 0;
  //ctrUnknown = 0;
  ctrAll = 0;

}


StatusCode Wlv :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("Wlv", "Truth tree for tt")));
  initialize_tree( tree("Wlv") );

  // trees of eventnumbers for events passing reco selection
  __list_recosel_et = getlist_recoselection("read_truth3/tt_et_MC_event_number.root");
  __list_recosel_mt = getlist_recoselection("read_truth3/tt_mt_MC_event_number.root");
  __list_recosel_em = getlist_recoselection("read_truth3/tt_em_MC_event_number.root");

  // FSR weight
  //ATH_CHECK(tool_weight.retrieve());
  //for (auto nm_weight : tool_weight->getWeightNames()) {
  //  ANA_MSG_INFO( "Truth variation weights: " << nm_weight );
  //}

  return StatusCode::SUCCESS;
}



StatusCode Wlv :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // cleanup and preparation
  initialize_leaves();

  // daod containers

  const DataVector<xAOD::TruthParticle>* taus = nullptr;
  ANA_CHECK (evtStore()->retrieve ( taus, "STDMTruthTaus"));

  const DataVector<xAOD::TruthParticle>* electrons = nullptr;
  ANA_CHECK (evtStore()->retrieve ( electrons, "STDMTruthElectrons"));

  const DataVector<xAOD::TruthParticle>* muons = nullptr;
  ANA_CHECK (evtStore()->retrieve ( muons, "STDMTruthMuons"));

  const xAOD::MissingETContainer* mets = nullptr;
  ANA_CHECK (evtStore()->retrieve ( mets, "MET_Truth"));

  const DataVector<xAOD::TruthParticle>* particles = nullptr;
  ANA_CHECK (evtStore()->retrieve ( particles, "TruthParticles"));

  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve ( jets, "AntiKt4TruthWZJets"));

  const xAOD::EventInfo * eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve (eventInfo, "EventInfo"));

  // particle containers that we need
  const xAOD::TruthParticle* W = nullptr;
  const xAOD::TruthParticle* tau = nullptr;
  const xAOD::TruthParticle* electron = nullptr;
  const xAOD::TruthParticle* muon = nullptr;
  const xAOD::TruthParticle* quark = nullptr; // W->qq'
  // in case of Wll
  const xAOD::TruthParticle* tau1 = nullptr;
  const xAOD::TruthParticle* electron1 = nullptr;
  const xAOD::TruthParticle* muon1 = nullptr;
  const xAOD::TruthParticle* quark1 = nullptr; // W->qq'

/*
  // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
  // origin:top = 10
  for( auto ilep : *taus ){
    if( ptype(*ilep)==MCTruthPartClassifier::ParticleType::IsoTau
	  && porigin(*ilep)==MCTruthPartClassifier::ParticleOrigin::top
	  && fromW(ilep) ) tau = ilep;
	//ANA_MSG_INFO("tau origin = " << porigin(*ilep) );
  }
  for( auto ilep : *electrons ){
    if( ptype(*ilep)==MCTruthPartClassifier::ParticleType::IsoElectron
	  && porigin(*ilep)==MCTruthPartClassifier::ParticleOrigin::top
	  && fromW(ilep) ) electron = ilep;
	//ANA_MSG_INFO("electorn origin = " << porigin(*ilep) );
  }
  for( auto ilep : *muons ){
	if( ptype(*ilep)==MCTruthPartClassifier::ParticleType::IsoMuon
	  && porigin(*ilep)==MCTruthPartClassifier::ParticleOrigin::top
	  && fromW(ilep) ) muon = ilep;
	//ANA_MSG_INFO("muon origin = " << porigin(*ilep) );
  }
*/

  for( auto part : *particles ){
    // tau
    if( abs(part->pdgId()) == 15 
	  && fromW(part) ){
	  part = correctedParticle(part); // radiation correction
	  if( !tau ) tau = part;
	  else if( !tau1 ) tau1 = part;
	  else ; // just ignore in case of 3 tau found
	}
    // muon
    if( abs(part->pdgId()) == 13
	  && fromW(part) ){
	  part = correctedParticle(part); // radiation correction
	  if( !muon ) muon = part;
	  else if( !muon1 ) muon1 = part;
	  else ; // just ignore in case of 3 muon found
	}
    // electron
    if( abs(part->pdgId()) == 11 
	  && fromW(part) ){
	  part = correctedParticle(part); // radiation correction
	  if( !electron ) electron = part;
	  else if( !electron1 ) electron1 = part;
	  else ; // just ignore in case of 3 electron found
	}
    // quark from W->qq'
    if( abs(part->pdgId()) <= 4
	  && fromW(part) ){
	  part = correctedParticle(part); // radiation correction
	  if( !quark ) quark = part;
	  else if( !quark1 ) quark1 = part;
	  else ; // just ignore in case of 3 electron found
	}

	// stop when at leaset two leptons are found
	//if( (!tau + !muon + !electron) <= 1){
	//  //ANA_MSG_INFO("Already found two leptons");
	//  break;
    //}
	// cut the loop if already two leptons are found
	// this is running on tt non-allhad
	if( (electron&&tau) || (muon&&tau) || (electron&&muon)
	  || (electron&&electron1) || (muon&&muon1) || (tau&&tau1)
	  || (electron&&quark) || (muon&&quark) ){
	  break;
	}
  }

  // identify channel based on truth particles
  // no action on ltau+1l events
  ++ctrAll;
  // count tau decay cases, no flag no if no return, just a count
  if( tau && (tauDecay(tau) == 0) ) { decay_tau_x+=1;
    if(tauDecayLep(tau)==11) decay_tau_e+=1; if(tauDecayLep(tau)==13) decay_tau_mu+=1; }
  if( tau && (tauDecay(tau) == 1) ) { decay_tau_x+=1; decay_tau_had_1prong+=1; }
  if( tau && (tauDecay(tau) == 3) ) { decay_tau_x+=1; decay_tau_had_3prong+=1; }
  if( tau && (tauDecay(tau) == 5) ) { decay_tau_x+=1; decay_tau_had_5prong+=1; }

  // end of tau decay counts
  if( tau && (tauDecay(tau) == 1) && electron ){ isWetauHad_1prong = 1;  ++ctrWetauHad_1prong; }
  else if( tau && (tauDecay(tau) == 3) && electron ){ isWetauHad_3prong = 1;  ++ctrWetauHad_3prong; }
  //else if( tau && !tauHad(tau) && electron ){ isWetauLep = 1; ++ctrWetauLep;}
  else if( tau && (tauDecay(tau) == 1) && muon ){ isWmutauHad_1prong = 1; ++ctrWmutauHad_1prong;}
  else if( tau && (tauDecay(tau) == 3) && muon ){ isWmutauHad_3prong = 1; ++ctrWmutauHad_3prong;}
  //else if( tau && !tauHad(tau) && muon ){ isWmutauLep = 1; ++ctrWmutauLep;}
  else if( electron && muon ){ isWemu = 1; ++ctrWemu;}
  //else if( electron && electron1 ){ isWee = 1; ++ctrWee;}
  //else if( muon && muon1 ){ isWmumu = 1; ++ctrWmumu;}
  //else if( tau && tau1 ){ isWtautau = 1; ++ctrWtautau;}
  else if( electron && quark ){ isWej = 1; ++ctrWej;}
  else if( muon && quark ){ isWmuj = 1; ++ctrWmuj;}
  //else if( tau && !electron && !muon ){ isWtauj = 1; ++ctrWtauj;}
  //else{ isUnknown = 1; ++ctrUnknown;}

  // event info
  weight = eventInfo->mcEventWeight(0);
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/PmgTopProcesses#Systematic_uncertainties
  // For a TOPQ derivation the weights are found to be the following:
  // weight 198 corresponding to "isr:muRfac=10_fsr:muRfac=20"
  // weight 199 corresponding to "isr:muRfac=10_fsr:muRfac=05"
  weight_fsr_muR05 = eventInfo->mcEventWeight(199);
  weight_fsr_muR20 = eventInfo->mcEventWeight(198);
  //
  weight_fsr_muR0625 = eventInfo->mcEventWeight(212);
  weight_fsr_muR0750 = eventInfo->mcEventWeight(213);
  weight_fsr_muR0875 = eventInfo->mcEventWeight(214);
  weight_fsr_muR1250 = eventInfo->mcEventWeight(211);
  weight_fsr_muR1500 = eventInfo->mcEventWeight(210);
  weight_fsr_muR1750 = eventInfo->mcEventWeight(209);
  //
  weight_Var3cUp = eventInfo->mcEventWeight(193);
  weight_Var3cDown = eventInfo->mcEventWeight(194);
  //
  eventnumber = eventInfo->eventNumber();
  //ANA_MSG_INFO("weight " << weight);
  //ANA_MSG_INFO("weight_fsr_muR05 " << weight_fsr_muR05);
  //ANA_MSG_INFO("weight_fsr_muR20 " << weight_fsr_muR20);

   // ALL-channel cutflows: first bin fill all events
  cf_WetauHad_1prong("MC total", 1, weight);
  cf_WetauHad_3prong("MC total", 1, weight);
  cf_WmutauHad_1prong("MC total", 1, weight);
  cf_WmutauHad_3prong("MC total", 1, weight);
  cf_Wemu("MC total",1, weight);
  cf_Wej("MC total",1, weight);
  cf_Wmuj("MC total",1, weight);
  //
  cf_WetauHad_1prong_fsr_muR05 ("MC total", 1, weight_fsr_muR05);
  cf_WetauHad_3prong_fsr_muR05 ("MC total", 1, weight_fsr_muR05);
  cf_WmutauHad_1prong_fsr_muR05("MC total", 1, weight_fsr_muR05);
  cf_WmutauHad_3prong_fsr_muR05("MC total", 1, weight_fsr_muR05);
  cf_Wemu_fsr_muR05            ("MC total", 1, weight_fsr_muR05);
  cf_Wej_fsr_muR05             ("MC total", 1, weight_fsr_muR05);
  cf_Wmuj_fsr_muR05            ("MC total", 1, weight_fsr_muR05);
  cf_WetauHad_1prong_fsr_muR20 ("MC total", 1, weight_fsr_muR20);
  cf_WetauHad_3prong_fsr_muR20 ("MC total", 1, weight_fsr_muR20);
  cf_WmutauHad_1prong_fsr_muR20("MC total", 1, weight_fsr_muR20);
  cf_WmutauHad_3prong_fsr_muR20("MC total", 1, weight_fsr_muR20);
  cf_Wemu_fsr_muR20            ("MC total", 1, weight_fsr_muR20);
  cf_Wej_fsr_muR20            ("MC total", 1, weight_fsr_muR20);
  cf_Wmuj_fsr_muR20            ("MC total", 1, weight_fsr_muR20);

  // channels
  if(isWetauHad_1prong) cf_WetauHad_1prong("Channel",1,weight);
  if(isWetauHad_3prong) cf_WetauHad_3prong("Channel",1,weight);
  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Channel",1,weight);
  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Channel",1,weight);
  if(isWemu) cf_Wemu("Channel",1,weight);
  if(isWej) cf_Wej("Channel",1,weight);
  if(isWmuj) cf_Wmuj("Channel",1,weight);
  //
  if(isWetauHad_1prong)cf_WetauHad_1prong_fsr_muR05 ("Channel", 1, weight_fsr_muR05);
  if(isWetauHad_3prong)cf_WetauHad_3prong_fsr_muR05 ("Channel", 1, weight_fsr_muR05);
  if(isWmutauHad_1prong)cf_WmutauHad_1prong_fsr_muR05("Channel", 1, weight_fsr_muR05);
  if(isWmutauHad_3prong)cf_WmutauHad_3prong_fsr_muR05("Channel", 1, weight_fsr_muR05);
  if(isWemu)cf_Wemu_fsr_muR05            ("Channel", 1, weight_fsr_muR05);
  if(isWej)cf_Wej_fsr_muR05            ("Channel", 1, weight_fsr_muR05);
  if(isWmuj)cf_Wmuj_fsr_muR05            ("Channel", 1, weight_fsr_muR05);
  if(isWetauHad_1prong)cf_WetauHad_1prong_fsr_muR20 ("Channel", 1, weight_fsr_muR20);
  if(isWetauHad_3prong)cf_WetauHad_3prong_fsr_muR20 ("Channel", 1, weight_fsr_muR20);
  if(isWmutauHad_1prong)cf_WmutauHad_1prong_fsr_muR20("Channel", 1, weight_fsr_muR20);
  if(isWmutauHad_3prong)cf_WmutauHad_3prong_fsr_muR20("Channel", 1, weight_fsr_muR20);
  if(isWemu)cf_Wemu_fsr_muR20            ("Channel", 1, weight_fsr_muR20);
  if(isWej)cf_Wej_fsr_muR20            ("Channel", 1, weight_fsr_muR20);
  if(isWmuj)cf_Wmuj_fsr_muR20            ("Channel", 1, weight_fsr_muR20);

  // tau had counting only tau had decay products
  TLorentzVector* tau_had = nullptr;
  if( tau )
	tau_had = get_tau_had(tau);
  // consider W->jets faking tau_had, take leading pT jet
  if( isWej || isWmuj )
    tau_had = get_tau_had_fakedbyWqq( jets );

  // met
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TruthDAOD#Truth_MET
  // in many cases it may be more accurate to sum NonInt and IntOut
  const xAOD::MissingET * met_NonInt = (*mets)["NonInt"];
  const xAOD::MissingET * met_IntOut = (*mets)["IntOut"];
  xAOD::MissingET * met = new xAOD::MissingET(*met_NonInt);
  (*met) += (*met_IntOut);
  met_pT = met->met()*GeV;
  met_phi = met->phi();

  // channels
  // NOTE: channels are orthogonal by isWxxx, so do not need to refresh all leaves
  //
  // // // // // //
  // W->etau tau_had
  // NOTE: fill kinematics !!!
  if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWej || isWmuj ){

    // event selections
	pass_all = 1;

	//
	if( isWetauHad_1prong || isWetauHad_3prong || isWemu || isWej ){
		e_pT = electron->pt()*GeV;
		e_eta = electron->eta();
		e_phi = electron->phi();
		e_E = electron->e()*GeV;
		pass_e_pT = ( e_pT > 27 );
		pass_all *= pass_e_pT;
        if(pass_all){
		  if(isWetauHad_1prong) cf_WetauHad_1prong("Electron pT",1,weight);
		  if(isWetauHad_3prong) cf_WetauHad_3prong("Electron pT",1,weight);
		  if(isWemu) cf_Wemu("Electron pT",1,weight);
		  if(isWej) cf_Wej("Electron pT",1,weight);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Electron pT",1,weight_fsr_muR05);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Electron pT",1,weight_fsr_muR05);
		  if(isWemu) cf_Wemu_fsr_muR05("Electron pT",1,weight_fsr_muR05);
		  if(isWej) cf_Wej_fsr_muR05("Electron pT",1,weight_fsr_muR05);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Electron pT",1,weight_fsr_muR20);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Electron pT",1,weight_fsr_muR20);
		  if(isWemu) cf_Wemu_fsr_muR20("Electron pT",1,weight_fsr_muR20);
		  if(isWej) cf_Wej_fsr_muR20("Electron pT",1,weight_fsr_muR20);
		}
	}

	//
	if( isWetauHad_1prong || isWetauHad_3prong || isWemu || isWej ){
		double _e_eta = abs( e_eta ); // abs!!!
		pass_e_eta = ( (_e_eta<1.37) || ( (_e_eta>1.52) && (_e_eta<2.47) ) );
		pass_all *= pass_e_eta;
		if(pass_all){
		  if(isWetauHad_1prong) cf_WetauHad_1prong("Electron eta",1,weight);
		  if(isWetauHad_3prong) cf_WetauHad_3prong("Electron eta",1,weight);
		  if(isWemu) cf_Wemu("Electron eta",1,weight);
		  if(isWej) cf_Wej("Electron eta",1,weight);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Electron eta",1,weight_fsr_muR05);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Electron eta",1,weight_fsr_muR05);
		  if(isWemu) cf_Wemu_fsr_muR05("Electron eta",1,weight_fsr_muR05);
		  if(isWej) cf_Wej_fsr_muR05("Electron eta",1,weight_fsr_muR05);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Electron eta",1,weight_fsr_muR20);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Electron eta",1,weight_fsr_muR20);
		  if(isWemu) cf_Wemu_fsr_muR20("Electron eta",1,weight_fsr_muR20);
		  if(isWej) cf_Wej_fsr_muR20("Electron eta",1,weight_fsr_muR20);
		}
	}

	//
	if( isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWmuj ){
		m_pT = muon->pt()*GeV;
		m_eta = muon->eta();
		m_phi = muon->phi();
		m_E = muon->e()*GeV;
		pass_mu_pT = ( m_pT > 27 );
		pass_all *= pass_mu_pT;
        if(pass_all){
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Muon pT",1,weight);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Muon pT",1,weight);
		  if(isWemu) cf_Wemu("Muon pT",1,weight);
		  if(isWmuj) cf_Wmuj("Muon pT",1,weight);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Muon pT",1,weight_fsr_muR05);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Muon pT",1,weight_fsr_muR05);
		  if(isWemu) cf_Wemu_fsr_muR05("Muon pT",1,weight_fsr_muR05);
		  if(isWmuj) cf_Wmuj_fsr_muR05("Muon pT",1,weight_fsr_muR05);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Muon pT",1,weight_fsr_muR20);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Muon pT",1,weight_fsr_muR20);
		  if(isWemu) cf_Wemu_fsr_muR20("Muon pT",1,weight_fsr_muR20);
		  if(isWmuj) cf_Wmuj_fsr_muR20("Muon pT",1,weight_fsr_muR20);
		}
	}

	//
	if( isWmutauHad_1prong || isWmutauHad_3prong || isWemu || isWmuj ){
		double _mu_eta = abs( m_eta ); // abs!!!
		pass_mu_eta = ( _mu_eta<2.5 );
		pass_all *= pass_mu_eta;
        if(pass_all){
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Muon eta",1,weight);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Muon eta",1,weight);
		  if(isWemu) cf_Wemu("Muon eta",1,weight);
		  if(isWmuj) cf_Wmuj("Muon eta",1,weight);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Muon eta",1,weight_fsr_muR05);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Muon eta",1,weight_fsr_muR05);
		  if(isWemu) cf_Wemu_fsr_muR05("Muon eta",1,weight_fsr_muR05);
		  if(isWmuj) cf_Wmuj_fsr_muR05("Muon eta",1,weight_fsr_muR05);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Muon eta",1,weight_fsr_muR20);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Muon eta",1,weight_fsr_muR20);
		  if(isWemu) cf_Wemu_fsr_muR20("Muon eta",1,weight_fsr_muR20);
		  if(isWmuj) cf_Wmuj_fsr_muR20("Muon eta",1,weight_fsr_muR20);
		}
	}

	//
	if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej || isWmuj ){
		t_pT = tau_had->Pt()*GeV;
		t_eta = tau_had->Eta();
		t_phi = tau_had->Phi();
		t_E = tau_had->E()*GeV;
		pass_tau_pT = ( t_pT > 25 );
		pass_all *=  pass_tau_pT;
		if(pass_all){
		  if(isWetauHad_1prong) cf_WetauHad_1prong("Tau pT",1,weight);
		  if(isWetauHad_3prong) cf_WetauHad_3prong("Tau pT",1,weight);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Tau pT",1,weight);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Tau pT",1,weight);
		  if(isWej) cf_Wej("Tau pT",1,weight);
		  if(isWmuj) cf_Wmuj("Tau pT",1,weight);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWej) cf_Wej_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWmuj) cf_Wmuj_fsr_muR05("Tau pT",1,weight_fsr_muR05);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		  if(isWej) cf_Wej_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		  if(isWmuj) cf_Wmuj_fsr_muR20("Tau pT",1,weight_fsr_muR20);
		}
	}

	//
	if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej || isWmuj ){
		double _tau_eta = abs( t_eta ); // abs!!!
		pass_tau_eta = ( (_tau_eta<1.37) || ((_tau_eta>1.52) && (_tau_eta<2.5)) );
		pass_all *= pass_tau_eta;
		if(pass_all){
		  if(isWetauHad_1prong) cf_WetauHad_1prong("Tau eta",1,weight);
		  if(isWetauHad_3prong) cf_WetauHad_3prong("Tau eta",1,weight);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Tau eta",1,weight);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Tau eta",1,weight);
		  if(isWej) cf_Wej("Tau eta",1,weight);
		  if(isWmuj) cf_Wmuj("Tau eta",1,weight);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWej) cf_Wej_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWmuj) cf_Wmuj_fsr_muR05("Tau eta",1,weight_fsr_muR05);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		  if(isWej) cf_Wej_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		  if(isWmuj) cf_Wmuj_fsr_muR20("Tau eta",1,weight_fsr_muR20);
		}
	}

    // Jets
	auto jetorigin = get_jetorigin( jets ); // vector of jet origins
	n_ljet = get_njetx(jetorigin,0); // pt eta is considered
	n_bjet = get_njetx(jetorigin,5); // pt eta is considered
    n_jet = n_ljet+n_bjet; // not consider tau jets !!!

	// Bjets
	// get_nbjet consdiers pT eta already
	pass_bjet_n = ( n_bjet == 2 );
	pass_all *= pass_bjet_n;
	if(pass_all){

	  auto bidx = get_idx_jetx(jetorigin,5);

	  auto b0_p4 = jets->at( bidx[0] )->p4();
	  b0_pT = b0_p4.Pt()*GeV;
	  b0_eta = b0_p4.Eta();
	  b0_phi = b0_p4.Phi();
	  b0_E = b0_p4.E()*GeV;
	  auto b1_p4 = jets->at( bidx[1] )->p4();
	  b1_pT = b1_p4.Pt()*GeV;
	  b1_eta = b1_p4.Eta();
	  b1_phi = b1_p4.Phi();
	  b1_E = b1_p4.E()*GeV;

	  if(isWetauHad_1prong) cf_WetauHad_1prong("BJet n=2",1,weight);
	  if(isWetauHad_3prong) cf_WetauHad_3prong("BJet n=2",1,weight);
	  if(isWmutauHad_1prong) cf_WmutauHad_1prong("BJet n=2",1,weight);
	  if(isWmutauHad_3prong) cf_WmutauHad_3prong("BJet n=2",1,weight);
	  if(isWemu) cf_Wemu("BJet n=2",1,weight);
	  if(isWej) cf_Wej("BJet n=2",1,weight);
	  if(isWmuj) cf_Wmuj("BJet n=2",1,weight);
	  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWemu) cf_Wemu_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWej) cf_Wej_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWmuj) cf_Wmuj_fsr_muR05("BJet n=2",1,weight_fsr_muR05);
	  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWemu) cf_Wemu_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWej) cf_Wej_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	  if(isWmuj) cf_Wmuj_fsr_muR20("BJet n=2",1,weight_fsr_muR20);
	}

	// only with additional jets
	const xAOD::Jet* jet = nullptr;
	if( isWej || isWmuj ) jet = get_3rdjet_inWlj( jets, tau_had); // the jet faking tau case in Wej Wmuj
	else jet = get_3rdjet( jets ); // get third jet that is light after pT eta // this is the normal signal case
	// nullptr means there is no 3rd jets after pT eta, on top of the 2 bjets
	if( !jet ){ // nullptr !!!
	    // these events are also signals
		// have to put them into cutflow
		// thus pretend they pass the 3rd jet cuts
	    pass_3rdjet = 1;
		pass_all *= pass_3rdjet;
		// fill in cutflow
	}else{ // have 3rd jet that is light
	    pass_3rdjet = 1;
		pass_all *= pass_3rdjet;
		j3_pT = jet->pt()*GeV;
		j3_eta = jet->eta();
		j3_phi = jet->phi();
		j3_E = jet->e()*GeV;
		//
		TLorentzVector v_lep;
		if( isWetauHad_1prong || isWetauHad_3prong ) v_lep = electron->p4();
		if( isWmutauHad_1prong || isWmutauHad_3prong ) v_lep = muon->p4();
		auto v_j3 = jet->p4();
		dR_l_j3 = v_lep.DeltaR(v_j3);
    	if(pass_all){
	      if(isWetauHad_1prong) cf_WetauHad_1prong("Jet 3rd",1,weight);
	      if(isWetauHad_3prong) cf_WetauHad_3prong("Jet 3rd",1,weight);
	      if(isWmutauHad_1prong) cf_WmutauHad_1prong("Jet 3rd",1,weight);
	      if(isWmutauHad_3prong) cf_WmutauHad_3prong("Jet 3rd",1,weight);
	      if(isWej) cf_Wej("Jet 3rd",1,weight);
	      if(isWmuj) cf_Wmuj("Jet 3rd",1,weight);
	      //if(isWemu) cf_Wemu("Jet 3rd",1,weight); // no 3rd cut on emu
	      if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      if(isWej) cf_Wej_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      if(isWmuj) cf_Wmuj_fsr_muR05("Jet 3rd",1,weight_fsr_muR05);
	      //if(isWemu) cf_Wemu_fsr_muR05("Jet 3rd",1,weight_fsr_muR05); // no 3rd cut on emu
	      if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      if(isWej) cf_Wej_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      if(isWmuj) cf_Wmuj_fsr_muR20("Jet 3rd",1,weight_fsr_muR20);
	      //if(isWemu) cf_Wemu_fsr_muR20("Jet 3rd",1,weight_fsr_muR20); // no 3rd cut on emu
	    }

		//
		if( isWetauHad_1prong || isWetauHad_3prong || isWmutauHad_1prong || isWmutauHad_3prong || isWej || isWmuj ){
			//TLorentzVector _tau_p4 = tau->p4();
			TLorentzVector _jet_p4 = jet->p4();
			m_taujet = (*tau_had+_jet_p4).M()*GeV;
			pass_m_taujet = !( (m_taujet > 50) && (m_taujet<90) );
			pass_all *= pass_m_taujet;
    		if(pass_all){
	          if(isWetauHad_1prong) cf_WetauHad_1prong("M(tau,jet)",1,weight);
	          if(isWetauHad_3prong) cf_WetauHad_3prong("M(tau,jet)",1,weight);
	          if(isWmutauHad_1prong) cf_WmutauHad_1prong("M(tau,jet)",1,weight);
	          if(isWmutauHad_3prong) cf_WmutauHad_3prong("M(tau,jet)",1,weight);
	          if(isWej) cf_Wej("M(tau,jet)",1,weight);
	          if(isWmuj) cf_Wmuj("M(tau,jet)",1,weight);
	          if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWej) cf_Wej_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWmuj) cf_Wmuj_fsr_muR05("M(tau,jet)",1,weight_fsr_muR05);
	          if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	          if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	          if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	          if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	          if(isWej) cf_Wej_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	          if(isWmuj) cf_Wmuj_fsr_muR20("M(tau,jet)",1,weight_fsr_muR20);
	        }
		}

	}

	// 4th 5th jet, i.e. 2nd (index 1) 3rd (index 2) ljet
	if( isWej || isWmuj ) jet = get_3pXrdjet_inWlj( jets, tau_had, 1 ); // the jet faking tau case in Wej Wmuj
	else jet = get_3pXrdjet( jets, 1 ); // get third jet that is light after pT eta // this is the normal signal case
	if( jet ){
	  j4_pT = jet->pt()*GeV;
	  j4_eta = jet->eta();
	  j4_phi = jet->phi();
	  j4_E = jet->e()*GeV;
	}
	if( isWej || isWmuj ) jet = get_3pXrdjet_inWlj( jets, tau_had, 2 ); // the jet faking tau case in Wej Wmuj
	else jet = get_3pXrdjet( jets, 2 ); // get third jet that is light after pT eta // this is the normal signal case
	if( jet ){
	  j5_pT = jet->pt()*GeV;
	  j5_eta = jet->eta();
	  j5_phi = jet->phi();
	  j5_E = jet->e()*GeV;
	}


	// reco match
	if( isWetauHad_1prong || isWetauHad_3prong || isWej ){
		pass_reco_etau = ( selected_reco_etau(eventnumber) );
		pass_all *= pass_reco_etau;
		if(pass_all){
		  if(isWetauHad_1prong) cf_WetauHad_1prong("Reco etau",1,weight);
		  if(isWetauHad_3prong) cf_WetauHad_3prong("Reco etau",1,weight);
		  if(isWej) cf_Wej("Reco etau",1,weight);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR05("Reco etau",1,weight_fsr_muR05);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR05("Reco etau",1,weight_fsr_muR05);
		  if(isWej) cf_Wej_fsr_muR05("Reco etau",1,weight_fsr_muR05);
		  if(isWetauHad_1prong) cf_WetauHad_1prong_fsr_muR20("Reco etau",1,weight_fsr_muR20);
		  if(isWetauHad_3prong) cf_WetauHad_3prong_fsr_muR20("Reco etau",1,weight_fsr_muR20);
		  if(isWej) cf_Wej_fsr_muR20("Reco etau",1,weight_fsr_muR20);
		}
	}
	if( isWmutauHad_1prong || isWmutauHad_3prong || isWmuj ){
		pass_reco_mutau = ( selected_reco_mutau(eventnumber) );
		pass_all *= pass_reco_mutau;
		if(pass_all){
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong("Reco mutau",1,weight);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong("Reco mutau",1,weight);
		  if(isWmuj) cf_Wmuj("Reco mutau",1,weight);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
		  if(isWmuj) cf_Wmuj_fsr_muR05("Reco mutau",1,weight_fsr_muR05);
		  if(isWmutauHad_1prong) cf_WmutauHad_1prong_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
		  if(isWmutauHad_3prong) cf_WmutauHad_3prong_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
		  if(isWmuj) cf_Wmuj_fsr_muR20("Reco mutau",1,weight_fsr_muR20);
		}
	}
	if( isWemu ){
		pass_reco_emu = ( selected_reco_emu(eventnumber) );
		pass_all *= pass_reco_emu;
		if(pass_all){
		  cf_Wemu("Reco emu",1,weight);
		  cf_Wemu_fsr_muR05("Reco emu",1,weight_fsr_muR05);
		  cf_Wemu_fsr_muR20("Reco emu",1,weight_fsr_muR20);
		}
	}

  }


  //// // // // // //
  //// W->mutau tau_had
  //// NOTE: fill kinematics !!!
  //if( isWmutauHad ){
  //  cf_WmutauHad("Channel WmutauHad",1,weight);

  //  // event selections
  //  pass_all = 1;

  //  //
  //  double _mu_pt = muon->pt()*GeV;
  //  pass_mu_pT = ( _mu_pt > 27 );
  //  pass_all *= pass_mu_pT;
  //  if(pass_all) cf_WmutauHad("Muon pT",1,weight);

  //  //
  //  double _mu_eta = abs(muon->eta());
  //  pass_mu_eta = ( _mu_eta<2.5 );
  //  pass_all *= pass_mu_eta;
  //  if(pass_all) cf_WmutauHad("Muon eta",1,weight);

  //  //
  //  double _tau_pt = tau->pt()*GeV;
  //  pass_tau_pT = ( _tau_pt > 25 );
  //  pass_all *=  pass_tau_pT;
  //  if(pass_all) cf_WmutauHad("Tau pT",1,weight);

  //  //
  //  double _tau_eta = abs(tau->eta());
  //  pass_tau_eta = ( (_tau_eta<1.37) || ((_tau_eta>1.52) && (_tau_eta<2.5)) );
  //  pass_all *= pass_tau_eta;
  //  if(pass_all) cf_WmutauHad("Tau eta",1,weight);

  //  // only with additional jets
  //  if( jets->size() < 1 ){
  //  }else{
  //  	const xAOD::Jet* jet = jets->at(0);
  //  	double _jet_pt = jet->pt()*GeV;
  //  	pass_jet_pT = ( _jet_pt > 25 );
  //  	pass_all *= pass_jet_pT;
  //  	if(pass_all) cf_WmutauHad("Jet n, pT",1,weight);

  //  	//
  //  	double _jet_eta = abs(jet->eta());
  //  	pass_jet_eta = ( _jet_eta < 4.5 );
  //  	pass_all *= pass_jet_eta;
  //  	if(pass_all) cf_WmutauHad("Jet eta",1,weight);

  //  	//
  //  	TLorentzVector _tau_p4 = tau->p4();
  //  	TLorentzVector _jet_p4 = jet->p4();
  //  	double _m_taujet = (_tau_p4+_jet_p4).M()*GeV;
  //  	pass_m_taujet = !( (_m_taujet > 50) && (_m_taujet<90) );
  //  	pass_all *= pass_m_taujet;
  //  	if(pass_all) cf_WmutauHad("M(tau,jet)",1,weight);

  //  	//
  //  	pass_reco_mutau = ( selected_reco_mutau(eventnumber) );
  //  	pass_all *= pass_reco_mutau;
  //  	if(pass_all) cf_WmutauHad("Reco mutau",1,weight);
  //  }

  //}
  //// // // // // //
  //// W->emu
  //// NOTE: fill kinematics !!!
  //if( isWemu ){
  //  cf_Wemu("Channel Wemu",1,weight);

  //  // event selections
  //  pass_all = 1;

  //  //
  //  double _e_pt = electron->pt()*GeV;
  //  pass_e_pT = ( _e_pt > 27 );
  //  pass_all *= pass_e_pT;
  //  if(pass_all) cf_Wemu("Electron pT",1,weight);

  //  //
  //  double _e_eta = abs(electron->eta());
  //  pass_e_eta = ( (_e_eta<1.37) || ( (_e_eta>1.52) && (_e_eta<2.47) ) );
  //  pass_all *= pass_e_eta;
  //  if(pass_all) cf_Wemu("Electron eta",1,weight);

  //  //
  //  double _mu_pt = muon->pt()*GeV;
  //  pass_mu_pT = ( _mu_pt > 27 );
  //  pass_all *= pass_mu_pT;
  //  if(pass_all) cf_Wemu("Muon pT",1,weight);

  //  //
  //  double _mu_eta = abs(muon->eta());
  //  pass_mu_eta = ( _mu_eta<2.5 );
  //  pass_all *= pass_mu_eta;
  //  if(pass_all) cf_Wemu("Muon eta",1,weight);

  //  //
  //  pass_reco_emu = ( selected_reco_emu(eventnumber) );
  //  pass_all *= pass_reco_emu;
  //  if(pass_all) cf_Wemu("Reco emu",1,weight);
  //}


  tree( "Wlv" )->Fill();

  delete tau_had;
  delete met;

  return StatusCode::SUCCESS;
}

StatusCode Wlv :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // fill cutflow to histogram
  ANA_CHECK (book ( cf_WetauHad_1prong.cutflow_hist() )); 
  ANA_CHECK (book ( cf_WetauHad_1prong.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WetauHad_3prong.cutflow_hist() )); 
  ANA_CHECK (book ( cf_WetauHad_3prong.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_1prong.cutflow_hist() )); 
  ANA_CHECK (book ( cf_WmutauHad_1prong.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_3prong.cutflow_hist() )); 
  ANA_CHECK (book ( cf_WmutauHad_3prong.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wemu.cutflow_hist() )); 
  ANA_CHECK (book ( cf_Wemu.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wej.cutflow_hist() )); 
  ANA_CHECK (book ( cf_Wej.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wmuj.cutflow_hist() )); 
  ANA_CHECK (book ( cf_Wmuj.cutflow_hist_weight() ));

  ANA_CHECK (book ( cf_WetauHad_1prong_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WetauHad_3prong_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_1prong_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_3prong_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wemu_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wej_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wmuj_fsr_muR05.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WetauHad_1prong_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WetauHad_3prong_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_1prong_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_WmutauHad_3prong_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wemu_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wej_fsr_muR20.cutflow_hist_weight() ));
  ANA_CHECK (book ( cf_Wmuj_fsr_muR20.cutflow_hist_weight() ));

  // these print outs are well consistentn with my prediction using pdgID numbers
  // then I split prongs so now comment them out and will not further breakdown
  //
  // myWlv                    INFO    Take BRs Wlv=0.108 Wqq'=0.676
  // myWlv                    INFO    Wetau or Wmutau or Wemu / total = (0.108 * 0.108 * 2) / (1-0.676*0.676) = 0.043
  // myWlv                    INFO    Fractions Wetau = Wmutau = Wemu
  // myWlv                    INFO    BR tau_had = 0.65 tau_lep = 0.35
  // myWlv                    INFO    WetauHad / total = 0.043 * 0.65 = 0.028
  // myWlv                    INFO    WetauLep / total = 0.043 * 0.65 = 0.015
  // myWlv                    INFO    # of WetauHad events: 2546 fraction (exp. 0.028) = 0.0282889
  // myWlv                    INFO    # of WetauLep events: 1414 fraction (exp. 0.015) = 0.0157111
  // myWlv                    INFO    # of WmutauHad events: 2537 fraction (exp. 0.028) = 0.0281889
  // myWlv                    INFO    # of WmutauLep events: 1344 fraction (exp. 0.015) = 0.0149333
  // myWlv                    INFO    # of Wemu events: 3772 fraction (exp. 0.043) = 0.0419111
  // myWlv                    INFO    # of Wee events: 1950 fraction (exp. 0.021) = 0.0216667
  // myWlv                    INFO    # of Wmumu events: 1945 fraction (exp. 0.021) = 0.0216111
  // myWlv                    INFO    # of Wtautau events: 1964 fraction (exp. 0.021) = 0.0218222
  // myWlv                    INFO    # of Wej events: 24148 fraction (exp. 0.27) = 0.268311
  // myWlv                    INFO    # of Wmuj events: 24032 fraction (exp. 0.27) = 0.267022
  // myWlv                    INFO    # of Wtauj events: 24348 fraction (exp. 0.27) = 0.270533
  // myWlv                    INFO    # of Unknown events: 2546 fraction (exp. 0) = 0.0282889
  // myWlv                    INFO    # of all events: 90000
  //ANA_MSG_INFO("Take BRs Wlv=0.108 Wqq'=0.676");
  //ANA_MSG_INFO("Wetau or Wmutau or Wemu / total = (0.108 * 0.108 * 2) / (1-0.676*0.676) = 0.043");
  //ANA_MSG_INFO("Fractions Wetau = Wmutau = Wemu");
  //ANA_MSG_INFO("BR tau_had = 0.65 tau_lep = 0.35");
  //ANA_MSG_INFO("WetauHad / total = 0.043 * 0.65 = 0.028");
  //ANA_MSG_INFO("WetauLep / total = 0.043 * 0.65 = 0.015");

  //ANA_MSG_INFO("# of WetauHad events: " << ctrWetauHad << " fraction (exp. 0.028) = " << 1.0*ctrWetauHad/ctrAll );
  //ANA_MSG_INFO("# of WetauLep events: " << ctrWetauLep << " fraction (exp. 0.015) = " << 1.0*ctrWetauLep/ctrAll );
  //ANA_MSG_INFO("# of WmutauHad events: " << ctrWmutauHad << " fraction (exp. 0.028) = " << 1.0*ctrWmutauHad/ctrAll );
  //ANA_MSG_INFO("# of WmutauLep events: " << ctrWmutauLep << " fraction (exp. 0.015) = " << 1.0*ctrWmutauLep/ctrAll );
  //ANA_MSG_INFO("# of Wemu events: " << ctrWemu << " fraction (exp. 0.043) = " << 1.0*ctrWemu/ctrAll );
  //ANA_MSG_INFO("# of Wee events: " << ctrWee << " fraction (exp. 0.021) = " << 1.0*ctrWee/ctrAll );
  //ANA_MSG_INFO("# of Wmumu events: " << ctrWmumu << " fraction (exp. 0.021) = " << 1.0*ctrWmumu/ctrAll );
  //ANA_MSG_INFO("# of Wtautau events: " << ctrWtautau << " fraction (exp. 0.021) = " << 1.0*ctrWtautau/ctrAll );
  //ANA_MSG_INFO("# of Wej events: " << ctrWej << " fraction (exp. 0.27) = " << 1.0*ctrWej/ctrAll );
  //ANA_MSG_INFO("# of Wmuj events: " << ctrWmuj << " fraction (exp. 0.27) = " << 1.0*ctrWmuj/ctrAll );
  //ANA_MSG_INFO("# of Wtauj events: " << ctrWtauj << " fraction (exp. 0.27) = " << 1.0*ctrWtauj/ctrAll );
  //ANA_MSG_INFO("# of Unknown events: " << ctrUnknown << " fraction (exp. 0) = " << 1.0*ctrUnknown/ctrAll );
  //ANA_MSG_INFO("# of all events: " << ctrAll );

  delete __list_recosel_et;
  delete __list_recosel_mt;
  delete __list_recosel_em;

  return StatusCode::SUCCESS;
}

bool Wlv :: fromW( const xAOD::TruthParticle* p ){
  for( unsigned int ipa=0; ipa<p->nParents(); ++ipa ){
    if( p->parent(ipa) && abs(p->parent(ipa)->pdgId())==24 ){ // incompelte truth record exist, check pointer first
      //ANA_MSG_INFO("PART " << p->pdgId() << " has W parent");
	  return true;
	}
  }
  return false;
}

bool Wlv :: tauHad( const xAOD::TruthParticle* tau ){
  //ANA_MSG_INFO("tau " << tau);
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
    //ANA_MSG_INFO("tau decay child pdgId = " << tau->child(itau)->pdgId() );
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==111) || (_id_==211) ) return true; // once found pi0/+, regard as had tau
	if( (_id_==11) || (_id_==13) ) return false; // once found e/mu, regard as lep tau
  }
  return true; // other cases, regard as had tau
}

// go down to the end of the "self-decay" (radiating gluons/photons etc.)
const xAOD::TruthParticle* Wlv :: correctedParticle( const xAOD::TruthParticle * part ){
  const xAOD::TruthParticle* corrpart = part;
  for( unsigned ikid=0; ikid<part->nChildren(); ++ikid ){
    const xAOD::TruthParticle* kid = part->child( ikid );
	if( kid->pdgId() == part->pdgId() ){
	  //Info( "correctedParticle()", "[pdgId %d] pT %f, eta %f, barcode %d, parent barcode %d, status %d",
	  //      kid->pdgId(), kid->pt(), kid->eta(), kid->barcode(), kid->parent(0)->barcode(), kid->status() );
	  corrpart = correctedParticle( kid );
	  break;
	}
  }
  return corrpart;
}

bool Wlv :: selected_reco_etau( ULong64_t evtnum ){
  //if( __tree_selected_et->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_et->begin(), __list_recosel_et->end(), evtnum);
}
bool Wlv :: selected_reco_mutau( ULong64_t evtnum ){
  //if( __tree_selected_mt->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_mt->begin(), __list_recosel_mt->end(), evtnum);
}
bool Wlv :: selected_reco_emu( ULong64_t evtnum ){
  //if( __tree_selected_em->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_em->begin(), __list_recosel_em->end(), evtnum);
}

// not responsible for mem consuption of the large vector!!!
std::vector<ULong64_t>* Wlv :: getlist_recoselection( const char* fname ){

  auto _gD = gDirectory;
  auto _file = new TFile(PathResolverFindCalibFile(fname).c_str());
  auto _tree = (TTree*) _file->Get("output");
  ULong64_t _evtnb = 0;
  _tree->SetBranchAddress("MCEventNumber", &_evtnb);
  auto _n = _tree->GetEntries();

  // disappointed by ROOT ... this is long-type var and it does not work when 6M events are in the tree ...
  /*
  _tree->Draw("MCEventNumber","1","goff");
  int _nrows = _tree->GetSelectedRows();
  double* _darray = _tree->GetV1();
  std::vector<int>* _vec = new std::vector<int>( _nrows, 0 );
  for( int i=0; i<_nrows; ++i )
    _vec->push_back( int(_darray[i]) );
  */

  std::vector<ULong64_t>* _vec = new std::vector<ULong64_t>( _n, 0 );
  for( ULong64_t i=0; i<_n; ++i ){
    _tree->GetEntry(i);
    _vec->push_back( _evtnb );
  }
  std::sort( _vec->begin(), _vec->end());

  delete _tree;
  _file->Close();
  delete _file;
  _gD->cd();

  return _vec;

}


// return 0: tau lep
// 1: 1-prong
// 3: 2-prong
int Wlv :: tauDecay( const xAOD::TruthParticle* tau ){
  //ANA_MSG_INFO("tau " << tau);
  int _sum_charge_trk = 0;
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
    // status
    int _status_ = tau->child(itau)->status();
	if( _status_ == 3 ) continue;
	// status == 3 will avoid double couting tau kids in PowhegPythia8
	//   ID [status] 15 2
	//   ID   kid [status] -16 1
	//   ID   kid [status] 111 2
	//   ID   kid [status] 211 1
	//   ID   kid [status] 211 1
	//   ID   kid [status] -211 1
	//   ID   kid [status] 22 1
	//   ID   kid [status] -16 3
	//   ID   kid [status] 111 3
	//   ID   kid [status] 211 3
	//   ID   kid [status] 211 3
	//   ID   kid [status] -211 3
	//
    //ANA_MSG_INFO("tau decay child pdgId = " << tau->child(itau)->pdgId() );
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==11) || (_id_==13) ){ // once found e/mu, regard as lep tau
	  //ANA_MSG_INFO("tau decay type : lep");
	  //return 0;
	}
	else{ // had tau decay
	  //ANA_MSG_INFO("tau decay type : had");
	  _sum_charge_trk += int(abs(tau->child(itau)->charge())); // consider |charge| as number of charged track
	}
  }
  //ANA_MSG_INFO("tau decay to charged tracks # = " << _sum_charge_trk);
  return _sum_charge_trk; // number of charged tracks
}

// known tau lep decay, find out 11 or 13
int Wlv :: tauDecayLep( const xAOD::TruthParticle* tau ){
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==11) ){
	  return 11;
	}
	if( (_id_==13) ){
	  return 13;
	}
  }
  return 0;
}
// pass me a tau that decays had
// return the sum p4 of non-neutrino decay products
TLorentzVector* Wlv :: get_tau_had( const xAOD::TruthParticle* tau ){
  TLorentzVector* _v = nullptr;
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
    int _status_ = tau->child(itau)->status();
	if( _status_ == 3 ) continue; // especially for pythia !!! it records all status=3 particles
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==12) || (_id_==14) || (_id_==16) ){ // neutrinos
	  continue;
	}
	if( !_v ){
	  _v = new TLorentzVector(tau->child(itau)->p4()); // considering radiation all within calo cones, thus do not remove radiations
	}else{
	  (*_v) += tau->child(itau)->p4(); // considering radiation all within calo cones, thus do not remove radiations
	}
  }

  return _v;
}

// find jets decaying from W and take highest pT one as the taked tau_had
TLorentzVector* Wlv :: get_tau_had_fakedbyWqq( const xAOD::JetContainer* jets ){
  const xAOD::Jet* jet = nullptr;
  TLorentzVector* _v = nullptr;
  for( auto j : *jets){
    // shame! somebody designed this aux but it does not exist in the DAOD !!!
	// so let's simply the algo
    //if( porigin(*j) == 12 ) // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
	//{
	//  if( _v ){
	//    if( _v->Pt() < j->pt() ){
	//	  *_v = j->p4();
	//	}
	//  }else{
	//    *_v = j->p4();
	//  }
	//}
	// over simplified; just avoid bjets ...
	if( flavor(*j) != 5 ){
	  if( !jet ){ // first light jet found
	    jet = j;
	  }else{ // more light jet fount
	    if( jet->pt() < j->pt() ){ // take higher pt jet
	      jet = j;
	    }
	  }
	}
  }
  if( !jet ) // this is already only for Wljet. if still cannot find ljet ... use bjet !
    _v = new TLorentzVector(jets->at(0)->p4());
  else
    _v = new TLorentzVector(jet->p4());
  return _v;
}

// https://gitlab.cern.ch/atlas/athena/-/blob/21.0/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/ParticleJetTools/JetFlavourInfo.h#L10
// 5:             b jet
// 4:             c jet
// 15:           tau jet
// 0: light-flavour jet
std::vector<int> Wlv :: get_jetorigin( const xAOD::JetContainer* jets ){
  std::vector<int> bidx( jets->size() );
  int _idx = 0;
  for( auto j : *jets ){
    bidx[_idx] = -1; // -1 for not interested jets (e.g. out of acceptance)

    bool _bkine = false; // bjet kine
	bool _lkine = false; // light jet kine
	if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<2.5 ) ) _bkine = true; // only count for pT>25 eta<2.5 !!!
	if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<4.5 ) ) _lkine = true;

    if( _bkine && (flavor(*j) == 5) ) bidx[_idx] = 5; // bjets
	if( _lkine && (flavor(*j) == 0 || flavor(*j) == 4) ) bidx[_idx] = 0; // light and c jets
	if( _lkine && (flavor(*j) == 15) ) bidx[_idx] = 15; // tau jets
	
	++_idx;
  }

  return bidx;
}

// nbjet = get_njetx( jetid, 5 )
// nljet = get_njetx( jetid, 0 )
int Wlv :: get_njetx( const std::vector<int> & jetid, const int whichid ){
  int _n = 0;
  for( auto j : jetid ){
    if( j == whichid ) ++_n;
  }
  return _n;
}
// return idx of the whichid jets
std::vector<int> Wlv :: get_idx_jetx( const std::vector<int> & jetid, const int whichid ){
  std::vector<int> _idx;
  int _i=0;
  for( auto j : jetid ){
    if( j == whichid ) _idx.push_back(_i);
	++_i;
  }
  return _idx;
}

// putting aside 2 bjets find the 3rd jets
// if not enough 2 bjets, return nullptr
// if find 2 bjets, but find no other light jets, return nullptr
// if find 2 bjets, and find 1+ other light jets, return the highest pT light jet
// consider pT eta cuts
const xAOD::Jet* Wlv :: get_3rdjet( const xAOD::JetContainer* jets ){

  const xAOD::Jet* ljet = nullptr;
  int nb = 0; // nbjets

  for( auto j : *jets ){
    // skip tau jet
	if( (flavor(*j) == 15) ) continue;

    // b or l jet
    bool _kine = false;
	if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<2.5 ) ) _kine = true; // only count for pT>25 eta<2.5 !!!
    if( _kine && (flavor(*j) == 5) ) ++nb;
	if( flavor(*j) != 5 ){ // this is a light
	  if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<4.5 ) ){ // light jets 4.5 eta
	    if( !ljet ){ // first light jet found
		  ljet = j;
		}else{ // more light jet fount
		  if( ljet->pt() < j->pt() ){ // take higher pt jet
		    ljet = j;
		  }
		}
	  }
	}
  }

  if( nb < 2 ){
    return nullptr;
  }else{ // nb >= 2, well mainly interested in == 2
    return ljet;
  }

}
// X=0: get_3pXrdjet == get_3rdjet
// X=1: get the 4th jet
// to simplify, assume jet container is already pT ordered
const xAOD::Jet* Wlv :: get_3pXrdjet( const xAOD::JetContainer* jets, int X=0 ){

  const xAOD::Jet* ljet = nullptr;
  int nb = 0; // nbjets
  int _x = 0;

  for( auto j : *jets ){
    // skip tau jet
	if( (flavor(*j) == 15) ) continue;

    // b or l jet
    bool _kine = false;
	if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<2.5 ) ) _kine = true; // only count for pT>25 eta<2.5 !!!
    if( _kine && (flavor(*j) == 5) ) ++nb;
	if( flavor(*j) != 5 ){ // this is a light
	  if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<4.5 ) ){ // light jets 4.5 eta
	    //if( !ljet ){ // first light jet found
		  ljet = j;
		// consider jet container is already pT ordered
		//}else{ // more light jet fount
		//  if( ljet->pt() < j->pt() ){ // take higher pt jet
		//    ljet = j;
		//  }
		//}
		if( _x == X ) break;
		++_x;
	  }
	}
  }

  if( nb < 2 ){
    return nullptr;
  }else{ // nb >= 2, well mainly interested in == 2
    return ljet;
  }

}

// for WlWj faking WlWtau
// Wjj': j fakes a tau: jtau_fake; find pT ljet that is NOT overlapped with jtau_fake !!! not necessarily j'
const xAOD::Jet* Wlv :: get_3rdjet_inWlj( const xAOD::JetContainer* jets, TLorentzVector* jtau_fake ){
  const xAOD::Jet* jet = nullptr;

  for( auto j : *jets){
    // shame ! not in DAOD
    //if( porigin(*j) == 12 ) // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
	//{
	  if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<4.5 ) ){ // light jets 4.5 eta
	    if( j->p4().DeltaR(*jtau_fake) > 0.4 ){ // deltaR to identify this is not the same jet that we used for taking tau_had
	      if( !jet ){ // first light jet found
		    jet = j;
		  }else{ // more light jet fount
		    if( jet->pt() < j->pt() ){ // take higher pt jet
		      jet = j;
		    }
		  }
		}
	  }
	//}
  }
  return jet;
}
// similar to get_3pXrdjet
// assume pT is ordered
const xAOD::Jet* Wlv :: get_3pXrdjet_inWlj( const xAOD::JetContainer* jets, TLorentzVector* jtau_fake, int X=0 ){
  const xAOD::Jet* jet = nullptr;
  int _x = 0;

  for( auto j : *jets){
    // shame ! not in DAOD
    //if( porigin(*j) == 12 ) // https://gitlab.cern.ch/atlas/athena/blob/21.2/PhysicsAnalysis/MCTruthClassifier/MCTruthClassifier/MCTruthClassifierDefs.h
	//{
	  if( (j->pt()*GeV > CUT_jetpT) && ( abs(j->eta())<4.5 ) ){ // light jets 4.5 eta
	    if( j->p4().DeltaR(*jtau_fake) > 0.4 ){ // deltaR to identify this is not the same jet that we used for taking tau_had
	      //if( !jet ){ // first light jet found
		    jet = j;
		  //}else{ // more light jet fount
		  //  if( jet->pt() < j->pt() ){ // take higher pt jet
		  //    jet = j;
		  //  }
		  //}
		  if( _x == X ) break;
		  ++_x;
		}
	  }
	//}
  }
  return jet;
}
