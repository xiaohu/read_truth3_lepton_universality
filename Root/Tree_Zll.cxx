#include <AsgTools/MessageCheck.h>
#include <read_truth3/Tree_Zll.h>

void Tree_Zll :: initialize_tree( TTree* tr ){

  // connect to the external tree
  thetree = tr;

  // connect to branches

  // event basic info
  thetree->Branch("eventnumber", &eventnumber);

  // weights
  thetree->Branch("weight", &weight);

  // reord channel by truth finding
  thetree->Branch("isZtautau_etau_1prong", &isZtautau_etau_1prong);
  thetree->Branch("isZtautau_etau_3prong", &isZtautau_etau_3prong);
  thetree->Branch("isZtautau_mutau_1prong",&isZtautau_mutau_1prong);
  thetree->Branch("isZtautau_mutau_3prong",&isZtautau_mutau_3prong);
  thetree->Branch("isZtautau_emu",          &isZtautau_emu);
  thetree->Branch("isUncategorised",             &isUncategorised);
  thetree->Branch("isHowManyTau",             &isHowManyTau);
  //
  thetree->Branch("isZtautau_tautau_11prong",             &isZtautau_tautau_11prong);
  thetree->Branch("isZtautau_tautau_13prong",             &isZtautau_tautau_13prong);
  thetree->Branch("isZtautau_tautau_33prong",             &isZtautau_tautau_33prong);
  thetree->Branch("isZtautau_ee",             &isZtautau_ee);
  thetree->Branch("isZtautau_mumu",             &isZtautau_mumu);
  //
  thetree->Branch("isZtautau_etau_5prong",    &isZtautau_etau_5prong);
  thetree->Branch("isZtautau_mutau_5prong",   &isZtautau_mutau_5prong);
  thetree->Branch("isZtautau_tautau_15prong", &isZtautau_tautau_15prong);
  thetree->Branch("isZtautau_tautau_35prong", &isZtautau_tautau_35prong);
  thetree->Branch("isZtautau_tautau_55prong", &isZtautau_tautau_55prong);

  // event selections
  thetree->Branch("pass_e_pT"     ,&pass_e_pT);
  thetree->Branch("pass_e_eta"    ,&pass_e_eta);
  thetree->Branch("pass_mu_pT"    ,&pass_mu_pT);
  thetree->Branch("pass_mu_eta"   ,&pass_mu_eta);
  thetree->Branch("pass_tau_pT"   ,&pass_tau_pT);
  thetree->Branch("pass_tau_eta"  ,&pass_tau_eta);
  thetree->Branch("pass_jet_pT"   ,&pass_jet_pT);
  thetree->Branch("pass_jet_eta"  ,&pass_jet_eta);
  thetree->Branch("pass_nbjet",         &pass_nbjet);
  thetree->Branch("pass_m3star",        &pass_m3star);
  thetree->Branch("pass_sumcosdphi",    &pass_sumcosdphi);
  thetree->Branch("pass_at",            &pass_at);
  thetree->Branch("pass_tau1prong_eta", &pass_tau1prong_eta);
  thetree->Branch("pass_m_ll",        &pass_m_ll);
  thetree->Branch("pass_reco",           &pass_reco);
  thetree->Branch("pass_all",           &pass_all);

  thetree->Branch("pass_reco_etau",           &pass_reco_etau);
  thetree->Branch("pass_reco_mutau",           &pass_reco_mutau);
  thetree->Branch("pass_reco_emu",           &pass_reco_emu);

  //
  thetree->Branch("decay_tau_x",           &decay_tau_x);
  thetree->Branch("decay_tau_e",           &decay_tau_e);
  thetree->Branch("decay_tau_mu",          &decay_tau_mu);
  thetree->Branch("decay_tau_had_1prong",  &decay_tau_had_1prong);
  thetree->Branch("decay_tau_had_3prong",  &decay_tau_had_3prong);
  thetree->Branch("decay_tau_had_5prong",  &decay_tau_had_5prong);

  // 
  thetree->Branch("e_pT",           &e_pT);
  thetree->Branch("e_eta",          &e_eta);
  thetree->Branch("e_phi",          &e_phi);
  thetree->Branch("e_E",            &e_E);
                                                      
  thetree->Branch("m_pT",           &m_pT);
  thetree->Branch("m_eta",          &m_eta);
  thetree->Branch("m_phi",          &m_phi);
  thetree->Branch("m_E",            &m_E);
                                                      
  thetree->Branch("t_pT",           &t_pT);
  thetree->Branch("t_eta",          &t_eta);
  thetree->Branch("t_phi",          &t_phi);
  thetree->Branch("t_E",            &t_E);
                                                      
  thetree->Branch("truth_t0_pT",           &truth_t0_pT);
  thetree->Branch("truth_t0_eta",          &truth_t0_eta);
  thetree->Branch("truth_t0_phi",          &truth_t0_phi);
  thetree->Branch("truth_t0_E",            &truth_t0_E);
  thetree->Branch("truth_t1_pT",           &truth_t1_pT);
  thetree->Branch("truth_t1_eta",          &truth_t1_eta);
  thetree->Branch("truth_t1_phi",          &truth_t1_phi);
  thetree->Branch("truth_t1_E",            &truth_t1_E);
                                                      
  thetree->Branch("ll_pT",           &ll_pT);
  thetree->Branch("ll_eta",          &ll_eta);
  thetree->Branch("ll_phi",          &ll_phi);
  thetree->Branch("ll_E",            &ll_E);
  thetree->Branch("ll_m",            &ll_m);

  thetree->Branch("z_pT",           &z_pT);
  thetree->Branch("z_eta",          &z_eta);
  thetree->Branch("z_phi",          &z_phi);
  thetree->Branch("z_E",            &z_E);
  thetree->Branch("z_m",            &z_m);

  thetree->Branch("tautau_truth_pT",           &tautau_truth_pT);
  thetree->Branch("tautau_truth_eta",          &tautau_truth_eta);
  thetree->Branch("tautau_truth_phi",          &tautau_truth_phi);
  thetree->Branch("tautau_truth_E",            &tautau_truth_E);
  thetree->Branch("tautau_truth_m",            &tautau_truth_m);
                                                      
  thetree->Branch("m3star",        &m3star);
  thetree->Branch("m3T",            &m3T);
  thetree->Branch("sinthetastar",   &sinthetastar);
  thetree->Branch("sumcosphi",      &sumcosphi);
  thetree->Branch("aT",             &aT);

  thetree->Branch("tau1_prong",             &tau1_prong);
  thetree->Branch("tau2_prong",             &tau2_prong);
  //
  thetree->Branch("tau3_prong",             &tau3_prong);
  thetree->Branch("tau4_prong",             &tau4_prong);
  thetree->Branch("tau5_prong",             &tau5_prong);
}

void Tree_Zll :: initialize_leaves(){

  // this should be called before filling any value to variables
  // such as the begining of execute() of the event selection

  // event basic info
  eventnumber = 0;

  // weights
  weight = 1;

  // reord channel by truth finding
  isZtautau_etau_1prong = 0;
  isZtautau_etau_3prong = 0;
  isZtautau_mutau_1prong = 0;
  isZtautau_mutau_3prong = 0;
  isZtautau_emu = 0;
  isUncategorised = 0;
  isHowManyTau = 0;
  // additional to check
  isZtautau_tautau_11prong = 0;
  isZtautau_tautau_13prong = 0;
  isZtautau_tautau_33prong = 0;
  isZtautau_ee = 0;
  isZtautau_mumu = 0;
  //
  isZtautau_etau_5prong = 0;
  isZtautau_mutau_5prong = 0;
  isZtautau_tautau_15prong = 0;
  isZtautau_tautau_35prong = 0;
  isZtautau_tautau_55prong = 0;

  // event selections
  pass_e_pT = 0;
  pass_e_eta = 0;
  pass_mu_pT = 0;
  pass_mu_eta = 0;
  pass_tau_pT = 0;
  pass_tau_eta = 0;
  pass_jet_pT = 0;
  pass_jet_eta = 0;
  pass_nbjet = 0;
  pass_m3star = 0;
  pass_sumcosdphi = 0;
  pass_at = 0;
  pass_tau1prong_eta = 0;
  pass_m_ll = 0;
  pass_reco = 0;
  pass_all = 0;

  pass_reco_etau = 0;
  pass_reco_mutau = 0;
  pass_reco_emu = 0;

  //
  decay_tau_x = 0;
  decay_tau_e = 0;
  decay_tau_mu = 0;
  decay_tau_had_1prong = 0;
  decay_tau_had_3prong = 0;
  decay_tau_had_5prong = 0;

  //
  e_pT = 0;
  e_eta = 0;
  e_phi = 0;
  e_E = 0;

  m_pT = 0;
  m_eta = 0;
  m_phi = 0;
  m_E = 0;

  t_pT = 0;
  t_eta = 0;
  t_phi = 0;
  t_E = 0;

  truth_t0_pT = 0;
  truth_t0_eta = 0;
  truth_t0_phi = 0;
  truth_t0_E = 0;
  truth_t1_pT = 0;
  truth_t1_eta = 0;
  truth_t1_phi = 0;
  truth_t1_E = 0;

  ll_pT = 0;
  ll_eta = 0;
  ll_phi = 0;
  ll_E = 0;
  ll_m = 0;

  z_pT = 0;
  z_eta = 0;
  z_phi = 0;
  z_E = 0;
  z_m = 0;

  tautau_truth_pT = 0;
  tautau_truth_eta = 0;
  tautau_truth_phi = 0;
  tautau_truth_E = 0;
  tautau_truth_m = 0;

  m3star = 0;
  m3T = 0;
  sinthetastar = 0;
  sumcosphi = 0;
  aT = 0;

  tau1_prong = -1;
  tau2_prong = -1;
  // Uncategorised cases
  tau3_prong = -1;
  tau4_prong = -1;
  tau5_prong = -1;
}

