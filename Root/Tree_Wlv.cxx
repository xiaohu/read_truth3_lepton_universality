#include <AsgTools/MessageCheck.h>
#include <read_truth3/Tree_Wlv.h>

void Tree_Wlv :: initialize_tree( TTree* tr ){

  // connect to the external tree
  thetree = tr;

  // connect to branches

  // event basic info
  thetree->Branch("eventnumber", &eventnumber);

  // weights
  thetree->Branch("weight", &weight);
  thetree->Branch("weight_fsr_muR05", &weight_fsr_muR05);
  thetree->Branch("weight_fsr_muR20", &weight_fsr_muR20);
  thetree->Branch("weight_fsr_muR0625", &weight_fsr_muR0625);
  thetree->Branch("weight_fsr_muR0750", &weight_fsr_muR0750);
  thetree->Branch("weight_fsr_muR0875", &weight_fsr_muR0875);
  thetree->Branch("weight_fsr_muR1250", &weight_fsr_muR1250);
  thetree->Branch("weight_fsr_muR1500", &weight_fsr_muR1500);
  thetree->Branch("weight_fsr_muR1750", &weight_fsr_muR1750);

  // reord channel by truth finding
  thetree->Branch("isWetauHad_1prong", &isWetauHad_1prong);
  thetree->Branch("isWetauHad_3prong", &isWetauHad_3prong);
  //thetree->Branch("isWetauLep", &isWetauLep);
  thetree->Branch("isWmutauHad_1prong", &isWmutauHad_1prong);
  thetree->Branch("isWmutauHad_3prong", &isWmutauHad_3prong);
  //thetree->Branch("isWmutauLep", &isWmutauLep);
  thetree->Branch("isWemu", &isWemu);
  //thetree->Branch("isWee", &isWee);
  //thetree->Branch("isWmumu", &isWmumu);
  //thetree->Branch("isWtautau", &isWtautau);
  thetree->Branch("isWej", &isWej);
  thetree->Branch("isWmuj", &isWmuj);
  //thetree->Branch("isWtauj", &isWtauj);
  //thetree->Branch("isUnknown", &isUnknown);

  // event selections
  thetree->Branch("pass_e_pT"     ,&pass_e_pT);
  thetree->Branch("pass_e_eta"    ,&pass_e_eta);
  thetree->Branch("pass_mu_pT"    ,&pass_mu_pT);
  thetree->Branch("pass_mu_eta"   ,&pass_mu_eta);
  thetree->Branch("pass_tau_pT"   ,&pass_tau_pT);
  thetree->Branch("pass_tau_eta"  ,&pass_tau_eta);
  thetree->Branch("pass_bjet_n"   ,&pass_bjet_n);
  thetree->Branch("pass_3rdjet"   ,&pass_3rdjet);
  thetree->Branch("pass_m_taujet" ,&pass_m_taujet);
  thetree->Branch("pass_reco_etau" ,&pass_reco_etau);
  thetree->Branch("pass_reco_mutau" ,&pass_reco_mutau);
  thetree->Branch("pass_reco_emu" ,&pass_reco_emu);
  thetree->Branch("pass_all"      ,&pass_all);

  //
  thetree->Branch("decay_tau_x",           &decay_tau_x);
  thetree->Branch("decay_tau_e",           &decay_tau_e);
  thetree->Branch("decay_tau_mu",          &decay_tau_mu);
  thetree->Branch("decay_tau_had_1prong",  &decay_tau_had_1prong);
  thetree->Branch("decay_tau_had_3prong",  &decay_tau_had_3prong);
  thetree->Branch("decay_tau_had_5prong",  &decay_tau_had_5prong);

  // kinematics
  thetree->Branch("e_pT",        &e_pT);
  thetree->Branch("e_eta",       &e_eta);
  thetree->Branch("e_phi",       &e_phi);
  thetree->Branch("e_E",         &e_E);

  thetree->Branch("m_pT",        &m_pT);
  thetree->Branch("m_eta",       &m_eta);
  thetree->Branch("m_phi",       &m_phi);
  thetree->Branch("m_E",         &m_E);

  thetree->Branch("t_pT",        &t_pT);
  thetree->Branch("t_eta",       &t_eta);
  thetree->Branch("t_phi",       &t_phi);
  thetree->Branch("t_E",         &t_E);

  thetree->Branch("met_pT",      &met_pT);
  thetree->Branch("met_eta",     &met_eta);
  thetree->Branch("met_phi",     &met_phi);
  thetree->Branch("met_E",       &met_E);

  thetree->Branch("b0_pT",       &b0_pT);
  thetree->Branch("b0_eta",      &b0_eta);
  thetree->Branch("b0_phi",      &b0_phi);
  thetree->Branch("b0_E",        &b0_E);

  thetree->Branch("b1_pT",       &b1_pT);
  thetree->Branch("b1_eta",      &b1_eta);
  thetree->Branch("b1_phi",      &b1_phi);
  thetree->Branch("b1_E",        &b1_E);

  thetree->Branch("j3_pT",       &j3_pT);
  thetree->Branch("j3_eta",      &j3_eta);
  thetree->Branch("j3_phi",      &j3_phi);
  thetree->Branch("j3_E",        &j3_E);
  thetree->Branch("j4_pT",       &j4_pT);
  thetree->Branch("j4_eta",      &j4_eta);
  thetree->Branch("j4_phi",      &j4_phi);
  thetree->Branch("j4_E",        &j4_E);
  thetree->Branch("j5_pT",       &j5_pT);
  thetree->Branch("j5_eta",      &j5_eta);
  thetree->Branch("j5_phi",      &j5_phi);
  thetree->Branch("j5_E",        &j5_E);
  thetree->Branch("dR_l_j3",        &dR_l_j3);

  thetree->Branch("n_bjet",          &n_bjet);
  thetree->Branch("n_ljet",          &n_ljet);
  thetree->Branch("n_jet",          &n_jet);

  thetree->Branch("w0_pT",       &w0_pT);
  thetree->Branch("w0_eta",      &w0_eta);
  thetree->Branch("w0_phi",      &w0_phi);
  thetree->Branch("w0_E",        &w0_E);

  thetree->Branch("w1_pT",       &w1_pT);
  thetree->Branch("w1_eta",      &w1_eta);
  thetree->Branch("w1_phi",      &w1_phi);
  thetree->Branch("w1_E",        &w1_E);

  thetree->Branch("m_taujet",        &m_taujet);
}

void Tree_Wlv :: initialize_leaves(){

  // this should be called before filling any value to variables
  // such as the begining of execute() of the event selection

  // event basic info
  eventnumber = 0;

  // weights
  weight = 1;
  weight_fsr_muR05 = 1;
  weight_fsr_muR20 = 1;
  weight_fsr_muR0625 = 1;
  weight_fsr_muR0750 = 1;
  weight_fsr_muR0875 = 1;
  weight_fsr_muR1250 = 1;
  weight_fsr_muR1500 = 1;
  weight_fsr_muR1750 = 1;

  // reord channel by truth finding
  isWetauHad_1prong = 0;
  isWetauHad_3prong = 0;
  //isWetauLep = 0;
  isWmutauHad_1prong = 0;
  isWmutauHad_3prong = 0;
  //isWmutauLep = 0;
  isWemu = 0;
  //isWee = 0;
  //isWmumu = 0;
  //isWtautau = 0;
  isWej = 0;
  isWmuj = 0;
  //isWtauj = 0;
  //isUnknown = 0;

  // event selections
  pass_e_pT = 0;
  pass_e_eta = 0;
  pass_mu_pT = 0;
  pass_mu_eta = 0;
  pass_tau_pT = 0;
  pass_tau_eta = 0;
  pass_bjet_n = 0;
  pass_3rdjet = 0;
  pass_m_taujet = 0;
  pass_reco_etau = 0;
  pass_reco_mutau = 0;
  pass_reco_emu = 0;
  pass_all = 0;

  //
  decay_tau_x = 0;
  decay_tau_e = 0;
  decay_tau_mu = 0;
  decay_tau_had_1prong = 0;
  decay_tau_had_3prong = 0;
  decay_tau_had_5prong = 0;


  e_pT = 0;
  e_eta = 0;
  e_phi = 0;
  e_E = 0;

  m_pT = 0;
  m_eta = 0;
  m_phi = 0;
  m_E = 0;

  t_pT = 0;
  t_eta = 0;
  t_phi = 0;
  t_E = 0;

  met_pT = 0;
  met_eta = 0;
  met_phi = 0;
  met_E = 0;

  b0_pT = 0;
  b0_eta = 0;
  b0_phi = 0;
  b0_E = 0;

  b1_pT = 0;
  b1_eta = 0;
  b1_phi = 0;
  b1_E = 0;

  j3_pT = 0;
  j3_eta = 0;
  j3_phi = 0;
  j3_E = 0;
  j4_pT = 0;
  j4_eta = 0;
  j4_phi = 0;
  j4_E = 0;
  j5_pT = 0;
  j5_eta = 0;
  j5_phi = 0;
  j5_E = 0;
  dR_l_j3 = 0;

  n_bjet = 0;
  n_ljet = 0;
  n_jet = 0; // this is number of jets (light and b etc. all inclusive)
          // n_jet == 2 mean only 2b; n_jet == 3, means 1 more additional jet
  w0_pT = 0;
  w0_eta = 0;
  w0_phi = 0;
  w0_E = 0;

  w1_pT = 0;
  w1_eta = 0;
  w1_phi = 0;
  w1_E = 0;

  m_taujet = 0;
}

