#include <AsgTools/MessageCheck.h>
#include <read_truth3/Zll.h>

SG::AuxElement::ConstAccessor<unsigned int> Zll :: ptype("classifierParticleType");
SG::AuxElement::ConstAccessor<unsigned int> Zll :: porigin("classifierParticleOrigin");
SG::AuxElement::ConstAccessor<int> Zll :: flavor("HadronConeExclTruthLabelID");

Zll :: Zll (const std::string& name,
            ISvcLocator *pSvcLocator)
    : EL::AnaAlgorithm (name, pSvcLocator),
	  cf_Ztautau_etau_1prong("Ztautau_etau_1prong"),
	  cf_Ztautau_etau_3prong("Ztautau_etau_3prong"),
	  cf_Ztautau_mutau_1prong("Ztautau_mutau_1prong"),
	  cf_Ztautau_mutau_3prong("Ztautau_mutau_3prong"),
	  cf_Ztautau_emu("Ztautau_emu")//,
	  //cf_Ztautau_tautau_11prong("cf_Ztautau_tautau_11prong"),
	  //cf_Ztautau_tautau_13prong("cf_Ztautau_tautau_13prong"),
	  //cf_Ztautau_tautau_33prong("cf_Ztautau_tautau_33prong"),
	  //cf_Ztautau_ee("cf_Ztautau_ee"),
	  //cf_Ztautau_mumu("cf_Ztautau_mumu"),
      //cf_Ztautau_etau_5prong("cf_Ztautau_etau_5prong"),
      //cf_Ztautau_mutau_5prong("cf_Ztautau_mutau_5prong"),
      //cf_Ztautau_tautau_15prong("cf_Ztautau_tautau_15prong"),
      //cf_Ztautau_tautau_35prong("cf_Ztautau_tautau_35prong"),
      //cf_Ztautau_tautau_55prong("cf_Ztautau_tautau_55prong")
{
  // Here you put any code for the base initialization of variables,
  // e.g. initialize all pointers to 0.  This is also where you
  // declare all properties for your algorithm.  Note that things like
  // resetting statistics variables or booking histograms should
  // rather go into the initialize() function.

  declareProperty("generator", generator, "generator");

  ctrAll = 0;
  ctrZtautau_etau_1prong = 0;
  ctrZtautau_etau_3prong = 0;
  ctrZtautau_mutau_1prong = 0;
  ctrZtautau_mutau_3prong = 0;
  ctrZtautau_emu = 0;
  ctrUncategorised = 0;
  //
  ctrZtautau_tautau_11prong = 0;
  ctrZtautau_tautau_13prong = 0;
  ctrZtautau_tautau_33prong = 0;
  ctrZtautau_ee = 0;
  ctrZtautau_mumu = 0;

}


StatusCode Zll :: initialize ()
{
  // Here you do everything that needs to be done at the very
  // beginning on each worker node, e.g. create histograms and output
  // trees.  This method gets called before any input files are
  // connected.

  ANA_CHECK (book (TTree ("Zll", "Truth tree for Zll")));
  initialize_tree( tree("Zll") );

  // trees of eventnumbers for events passing reco selection
  if( generator == "powheg" ){
    __list_recosel_et = getlist_recoselection("read_truth3/et_MC_event_number_Ztt.root");
    __list_recosel_mt = getlist_recoselection("read_truth3/mt_MC_event_number_Ztt.root");
    __list_recosel_em = getlist_recoselection("read_truth3/em_MC_event_number_Ztt.root");
  }else if( generator == "sherpa" ){
    __list_recosel_et = getlist_recoselection("read_truth3/et_MC_event_number_Sherpa_Ztt.root");
    __list_recosel_mt = getlist_recoselection("read_truth3/mt_MC_event_number_Sherpa_Ztt.root");
    __list_recosel_em = getlist_recoselection("read_truth3/em_MC_event_number_Sherpa_Ztt.root");
  }else{
    ANA_MSG_INFO("algo.generator is wrong : " << generator );
    return StatusCode::FAILURE;
  }

  // sherpa dsid
  sherpa_dsid.push_back(364128);
  sherpa_dsid.push_back(364129);
  sherpa_dsid.push_back(364130);
  sherpa_dsid.push_back(364131);
  sherpa_dsid.push_back(364132);
  sherpa_dsid.push_back(364133);
  sherpa_dsid.push_back(364134);
  sherpa_dsid.push_back(364135);
  sherpa_dsid.push_back(364136);
  sherpa_dsid.push_back(364137);
  sherpa_dsid.push_back(364138);
  sherpa_dsid.push_back(364139);
  sherpa_dsid.push_back(364140);
  sherpa_dsid.push_back(364141);

  return StatusCode::SUCCESS;
}



StatusCode Zll :: execute ()
{
  // Here you do everything that needs to be done on every single
  // events, e.g. read input variables, apply cuts, and fill
  // histograms and trees.  This is where most of your actual analysis
  // code will go.

  // cleanup and preparation
  initialize_leaves();

  // daod containers

  const DataVector<xAOD::TruthParticle>* taus = nullptr;
  ANA_CHECK (evtStore()->retrieve ( taus, "STDMTruthTaus"));

  const DataVector<xAOD::TruthParticle>* electrons = nullptr;
  ANA_CHECK (evtStore()->retrieve ( electrons, "STDMTruthElectrons"));

  const DataVector<xAOD::TruthParticle>* muons = nullptr;
  ANA_CHECK (evtStore()->retrieve ( muons, "STDMTruthMuons"));

  const xAOD::MissingETContainer* mets = nullptr;
  ANA_CHECK (evtStore()->retrieve ( mets, "MET_Truth"));

  const DataVector<xAOD::TruthParticle>* particles = nullptr;
  ANA_CHECK (evtStore()->retrieve ( particles, "TruthParticles"));

  const xAOD::JetContainer* jets = nullptr;
  ANA_CHECK (evtStore()->retrieve ( jets, "AntiKt4TruthWZJets"));

  const xAOD::EventInfo * eventInfo = nullptr;
  ANA_CHECK(evtStore()->retrieve (eventInfo, "EventInfo"));

  // particle containers that we need
  // 1. etau mutau: tau_lep (electron or muon) tau_had
  // 2. emu: electron muon
  std::vector<const xAOD::TruthParticle*> tau2; // hold tau_lep tau_had
  TLorentzVector* tau_had = nullptr;
  const xAOD::TruthParticle* electron = nullptr;
  const xAOD::TruthParticle* muon = nullptr;
  const xAOD::TruthParticle* zboson = nullptr;
  // additional objects not used in analysis
  TLorentzVector* tau_had2 = nullptr; // for tt->hadhad
  const xAOD::TruthParticle* electron2 = nullptr; // for tt->ee
  const xAOD::TruthParticle* muon2 = nullptr; // for tt->mumu

  int tau_had_prong = 0; // tau_had: 1 prong: 1; 3 prong: 3 !!! for tau_had
  int tau_had2_prong = 0; // for tau_had2

  int dsid = eventInfo->mcChannelNumber();
  int isSherpa = _isSherpa( dsid );
  //ANA_MSG_INFO("DSID : " << dsid);

  // event info
  weight = eventInfo->mcEventWeight(0);
  eventnumber = eventInfo->eventNumber();

  //
  // ABOUT Sherpa
  // in the derivation the truth records seem a bit crazy
  // 1. decay chain seems not correct
  //  INFO    ID 15
  //  INFO    ID   kid 15
  //  INFO    ID   kid -15
  //  INFO    ID   kid 22
  //  INFO    ID -15
  //  INFO    ID   kid 15
  //  INFO    ID   kid -15
  //  INFO    ID   kid 22
  // where particles and their kids are printed out for pdgID
  // 15 can decay to 15 -15 22 ?! Charge is not conserved !!!
  // 2. decay products are registered but their pointer sometimes is empty ...
  // 3. Z pdgID=23 does not exist
  // 4. some tau just does not decay ?!
  //

  // find 2 tau first
  //ANA_MSG_INFO("ENTRY . .. ... .... .....");
  for( auto part : *particles ){
    //
    //ANA_MSG_INFO("ID " << part->pdgId() );
	//for( unsigned iii=0; iii<part->nChildren(); ++iii )
	//  if(part->child(iii)) ANA_MSG_INFO("ID   kid " << part->child(iii)->pdgId() );
	//  else ANA_MSG_INFO("ID   kid invalid pointer " << part->child(iii) );
	//
    // tau
    if( abs(part->pdgId()) == 15 
	  && ( isSherpa || fromZ(part) ) ){
	  bool _duplica = false;
	  for( auto _tau : tau2 ){
	    if( correctedParticle(part) == _tau ) _duplica = true;
	  }
	  if( !_duplica ){
	    tau2.push_back(correctedParticle(part)); // radiation correction
	  }
	}

	if( abs(part->pdgId()) == 23 ){
	  zboson = correctedParticle(part);
	}

    // Sherpa does not have a full truth record ...
	// so have to run over all truth particles !!!
	// and collected all tau !!!
/*
	// stop once two taus are found, only for speeding up
	if( tau2.size() == 2 && ( isSherpa || zboson ) ){
	  break;
	}
*/
  }

  // identify tau decays
  for( auto _tau : tau2 ){
    int _ntrk_had = tauDecay(_tau);

	// tau lep
    if( _ntrk_had == 0 ){
	  for( unsigned int _itau=0; _itau<_tau->nChildren(); ++_itau ){
	    auto _thiskid = _tau->child(_itau);
		if( abs(_thiskid->pdgId()) == 11 ){ // tau->electron
		  _thiskid = correctedParticle( _thiskid );
		  if(!electron) electron = _thiskid;
		  else{ // for tt->ee not used in analysis
		    if(electron!=_thiskid) electron2 = _thiskid;
		  }
		}else if( abs(_thiskid->pdgId()) == 13 ){ // tau->muon
		  _thiskid = correctedParticle( _thiskid );
		  if(!muon) muon = _thiskid;
		  else{ // for tt->mumu not used in analysis
		    if(muon!=_thiskid) muon2 = _thiskid;
		  }
		}
	  }
	}

	// tau had
	else if( _ntrk_had == 1 ){
	  if(!tau_had){
	    tau_had = get_tau_had(_tau);
		tau_had_prong = _ntrk_had;
	  }else{
	    tau_had2 = get_tau_had(_tau);
		tau_had2_prong = _ntrk_had;
	  }
	}
	else if( _ntrk_had == 3 ){
	  if(!tau_had){
	    tau_had = get_tau_had(_tau);
		tau_had_prong = _ntrk_had;
	  }else{
	    tau_had2 = get_tau_had(_tau);
		tau_had2_prong = _ntrk_had;
	  }
	}

	else if( _ntrk_had == 5 ){
	  if(!tau_had){
	    tau_had = get_tau_had(_tau);
		tau_had_prong = _ntrk_had;
	  }else{
	    tau_had2 = get_tau_had(_tau);
		tau_had2_prong = _ntrk_had;
	  }
	}

	// record prongs in case of more than the types of taus we look for above
    else{
	  if( _tau->nChildren() > 0 ){ // Sherpa has tau without decay, skip those
	    // assumed found two taus already, thus tau3,4,5 here
	    if( tau3_prong  == -1 ) tau3_prong = _ntrk_had;
	    else if( tau4_prong == -1 ) tau4_prong = _ntrk_had;
	    else if( tau5_prong == -1 ) tau5_prong = _ntrk_had;
	    else;
	  }
	}
  }
  if( tau2.size() >= 2 ){
    if( tau2[0]->pt() > tau2[1]->pt() ){
      truth_t0_pT = tau2[0]->pt()*GeV;
      truth_t0_eta = tau2[0]->eta();
      truth_t0_phi = tau2[0]->phi();
      truth_t0_E = tau2[0]->e()*GeV;
      truth_t1_pT = tau2[1]->pt()*GeV;
      truth_t1_eta = tau2[1]->eta();
      truth_t1_phi = tau2[1]->phi();
      truth_t1_E = tau2[1]->e()*GeV;
    }else{
      truth_t1_pT = tau2[0]->pt()*GeV;
      truth_t1_eta = tau2[0]->eta();
      truth_t1_phi = tau2[0]->phi();
      truth_t1_E = tau2[0]->e()*GeV;
      truth_t0_pT = tau2[1]->pt()*GeV;
      truth_t0_eta = tau2[1]->eta();
      truth_t0_phi = tau2[1]->phi();
      truth_t0_E = tau2[1]->e()*GeV;
    }
  }
  tau1_prong = tau_had_prong;
  tau2_prong = tau_had2_prong;

  // if no etau mutau or emu, drop
  isHowManyTau = tau2.size(); // this is not always 2, especially for Sherpa
  ++ctrAll;
  if( electron&&tau_had ){
    decay_tau_x += 2;
    decay_tau_e += 1;
    if( tau_had_prong == 1 ){ isZtautau_etau_1prong=1; ++ctrZtautau_etau_1prong; decay_tau_had_1prong +=1; }
	if( tau_had_prong == 3 ){ isZtautau_etau_3prong=1; ++ctrZtautau_etau_3prong; decay_tau_had_3prong +=1; }
	if( tau_had_prong == 5 ){ isZtautau_etau_5prong=1; ++ctrZtautau_etau_5prong; decay_tau_had_5prong +=1; }
  }else if( muon&&tau_had ){
    decay_tau_x += 2;
    decay_tau_mu += 1;
    if( tau_had_prong == 1 ){ isZtautau_mutau_1prong=1; ++ctrZtautau_mutau_1prong; decay_tau_had_1prong +=1; }
	if( tau_had_prong == 3 ){ isZtautau_mutau_3prong=1; ++ctrZtautau_mutau_3prong; decay_tau_had_3prong +=1; }
	if( tau_had_prong == 5 ){ isZtautau_mutau_5prong=1; ++ctrZtautau_mutau_5prong; decay_tau_had_5prong +=1; }
  }else if( electron&&muon ){
    decay_tau_x += 2;
    decay_tau_e += 1;
	decay_tau_mu += 1;
    isZtautau_emu=1;
    ++ctrZtautau_emu;
  }else if( tau_had&&tau_had2 ){
    decay_tau_x += 2;
    if( (tau_had_prong == 1) && (tau_had2_prong == 1) ){ isZtautau_tautau_11prong=1; ++ctrZtautau_tautau_11prong; decay_tau_had_1prong += 2; } // prong 1-1
    if( (tau_had_prong == 1) && (tau_had2_prong == 3) || (tau_had_prong == 3) && (tau_had2_prong == 1) )
	  { isZtautau_tautau_13prong=1; ++ctrZtautau_tautau_13prong; decay_tau_had_1prong += 1; decay_tau_had_3prong += 1;} // prong 1-3
    if( (tau_had_prong == 3) && (tau_had2_prong == 3) ){ isZtautau_tautau_33prong=1; ++ctrZtautau_tautau_33prong; decay_tau_had_3prong += 2; } // prong 3-3
	//
    if( (tau_had_prong == 1) && (tau_had2_prong == 5) || (tau_had_prong == 5) && (tau_had2_prong == 1) )
	  { isZtautau_tautau_15prong=1; ++ctrZtautau_tautau_15prong; decay_tau_had_1prong += 1; decay_tau_had_5prong += 1;} // prong 1-5
    if( (tau_had_prong == 3) && (tau_had2_prong == 5) || (tau_had_prong == 5) && (tau_had2_prong == 3) )
	  { isZtautau_tautau_35prong=1; ++ctrZtautau_tautau_35prong; decay_tau_had_3prong += 1; decay_tau_had_5prong += 1;} // prong 3-5
    if( (tau_had_prong == 5) && (tau_had2_prong == 5) ){ isZtautau_tautau_55prong=1; ++ctrZtautau_tautau_55prong; decay_tau_had_5prong += 2; } // prong 5-5
  }else if( electron&&electron2 ){
    decay_tau_x += 2;
    isZtautau_ee = 1; ++ctrZtautau_ee; decay_tau_e += 2;
  }else if( muon&&muon2 ){
    decay_tau_x += 2;
    isZtautau_mumu = 1; ++ctrZtautau_mumu; decay_tau_mu += 2;
  }else{
    // Fill tree here, so that all events have entries
	// all channel flags are filled
	// after this, only the analysis channels ltau and emu are filled with kinematics

	// Uncategorised
	isUncategorised = 1; ++ctrUncategorised;

    // test check Uncategorised events !!!
    //ANA_MSG_INFO("Uncategorised event (only print Z and tau and their decays): eventNumber="<<eventInfo->eventNumber());
    //for( auto part : *particles ){
    //  int _id_ = abs(part->pdgId());
	//  if( (_id_ == 23) || (_id_ == 15) ) ;
	//  else continue;
    //  ANA_MSG_INFO("ID [status charge] " << _id_ << " " << part->status() << " " << part->charge() );
    //  for( unsigned iii=0; iii<part->nChildren(); ++iii )
    //    if(part->child(iii)) ANA_MSG_INFO("ID   kid [status charge] " << part->child(iii)->pdgId() << " " << part->child(iii)->status() << " " << part->charge() );
    //    else ANA_MSG_INFO("ID   kid invalid pointer " << part->child(iii) );
    //}
	//

    tree( "Zll" )->Fill();
    return StatusCode::SUCCESS;
  }

  // test
  /*
  if( isZtautau_ee ){
    ANA_MSG_INFO("This is ee ... tracing its parent's full decays");
	auto part = electron->parent(0);
    ANA_MSG_INFO("Parent of electron1: ID " << part->pdgId() );
	for( unsigned iii=0; iii<part->nChildren(); ++iii )
	  if(part->child(iii)) ANA_MSG_INFO("ID   kid " << part->child(iii)->pdgId() );
	  else ANA_MSG_INFO("ID   kid invalid pointer " << part->child(iii) );
	part = electron2->parent(0);
    ANA_MSG_INFO("Parent of electron2: ID " << part->pdgId() );
	for( unsigned iii=0; iii<part->nChildren(); ++iii )
	  if(part->child(iii)) ANA_MSG_INFO("ID   kid " << part->child(iii)->pdgId() );
	  else ANA_MSG_INFO("ID   kid invalid pointer " << part->child(iii) );
  }
  */

  // ALL-channel cutflows: first bin fill all events
  cf_Ztautau_etau_1prong("MC total", 1, weight);
  cf_Ztautau_etau_3prong("MC total", 1, weight);
  cf_Ztautau_mutau_1prong("MC total", 1, weight);
  cf_Ztautau_mutau_3prong("MC total", 1, weight);
  cf_Ztautau_emu("MC total", 1, weight);

  // met
  // https://twiki.cern.ch/twiki/bin/view/AtlasProtected/TruthDAOD#Truth_MET
  // in many cases it may be more accurate to sum NonInt and IntOut
  const xAOD::MissingET * met_NonInt = (*mets)["NonInt"];
  const xAOD::MissingET * met_IntOut = (*mets)["IntOut"];
  xAOD::MissingET * met = new xAOD::MissingET(*met_NonInt);
  (*met) += (*met_IntOut);

  // basic var
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    TLorentzVector _v_t = *tau_had;
	TLorentzVector _v_e = electron->p4();
	TLorentzVector _v_ll = _v_t + _v_e;
	e_pT  = _v_e.Pt()*GeV;
	e_eta = _v_e.Eta();
	e_phi = _v_e.Phi();
	e_E   = _v_e.E()*GeV;
	t_pT  = _v_t.Pt()*GeV;
	t_eta = _v_t.Eta();
	t_phi = _v_t.Phi();
	t_E   = _v_t.E()*GeV;
	ll_pT  = _v_ll.Pt()*GeV;
	ll_eta = _v_ll.Eta();
	ll_phi = _v_ll.Phi();
	ll_E   = _v_ll.E()*GeV;
	ll_m    = _v_ll.M()*GeV;
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    TLorentzVector _v_t = *tau_had;
	TLorentzVector _v_m = muon->p4();
	TLorentzVector _v_ll = _v_t + _v_m;
	m_pT  = _v_m.Pt()*GeV;
	m_eta = _v_m.Eta();
	m_phi = _v_m.Phi();
	m_E   = _v_m.E()*GeV;
	t_pT  = _v_t.Pt()*GeV;
	t_eta = _v_t.Eta();
	t_phi = _v_t.Phi();
	t_E   = _v_t.E()*GeV;
	ll_pT  = _v_ll.Pt()*GeV;
	ll_eta = _v_ll.Eta();
	ll_phi = _v_ll.Phi();
	ll_E   = _v_ll.E()*GeV;
	ll_m   = _v_ll.M()*GeV;
  }else if( isZtautau_emu ){
    TLorentzVector _v_e = electron->p4();
	TLorentzVector _v_m = muon->p4();
	TLorentzVector _v_ll = _v_e + _v_m;
	m_pT  = _v_m.Pt()*GeV;
	m_eta = _v_m.Eta();
	m_phi = _v_m.Phi();
	m_E   = _v_m.E()*GeV;
	e_pT  = _v_e.Pt()*GeV;
	e_eta = _v_e.Eta();
	e_phi = _v_e.Phi();
	e_E   = _v_e.E()*GeV;
	ll_pT  = _v_ll.Pt()*GeV;
	ll_eta = _v_ll.Eta();
	ll_phi = _v_ll.Phi();
	ll_E   = _v_ll.E()*GeV;
	ll_m   = _v_ll.M()*GeV;
  }
  if( zboson ){ // Powheg has truth Z, go ahead
    TLorentzVector _v_z = zboson->p4();
    z_pT  = _v_z.Pt()*GeV;
    z_eta = _v_z.Eta();
    z_phi = _v_z.Phi();
    z_E   = _v_z.E()*GeV;
	z_m   = _v_z.M()*GeV;
  }else{ // Sherpa does not have Z truth
    TLorentzVector _v_z = beforeRadiation(tau2[0])->p4()+beforeRadiation(tau2[1])->p4(); // use tautau to reco Z in Sherpa, as Sherpa does not have Z truth
    z_pT  = _v_z.Pt()*GeV;
    z_eta = _v_z.Eta();
    z_phi = _v_z.Phi();
    z_E   = _v_z.E()*GeV;
	z_m   = _v_z.M()*GeV;
  }
  // in case of wrong mZ, such as in Powheg events that hold w=1 (normally w=+/-2000)
  TLorentzVector _v_z = beforeRadiation(tau2[0])->p4()+beforeRadiation(tau2[1])->p4(); // use tautau to reco Z in Sherpa, as Sherpa does not have Z truth
  tautau_truth_pT  = _v_z.Pt()*GeV;
  tautau_truth_eta = _v_z.Eta();
  tautau_truth_phi = _v_z.Phi();
  tautau_truth_E   = _v_z.E()*GeV;
  tautau_truth_m   = _v_z.M()*GeV;
  //

  // channels
  //
  // // // // // //

  pass_all = 1;
  if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("This channel", 1, weight);
  if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("This channel", 1, weight);
  if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("This channel", 1, weight);
  if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("This channel", 1, weight);
  if(isZtautau_emu) cf_Ztautau_emu("This channel", 1, weight);

  // pass e
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_emu ){
    // pT
    pass_e_pT = ( electron->pt()*GeV > 27 );
	pass_all *= pass_e_pT;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("e pT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("e pT", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("e pT", 1, weight);
	}
	// eta
	double _eta = abs(electron->eta());
	pass_e_eta = ( _eta<1.37 ) || ( (_eta>1.52) && (_eta<2.47) );
	pass_all *= pass_e_eta;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("e eta", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("e eta", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("e eta", 1, weight);
	}
  }

  // pass mu
  if( isZtautau_mutau_1prong || isZtautau_mutau_3prong || isZtautau_emu ){
    // pT
    pass_mu_pT = ( muon->pt()*GeV > 27 );
	pass_all *= pass_mu_pT;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("mu pT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("mu pT", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("mu pT", 1, weight);
	}
	// eta
	double _eta = abs(muon->eta());
	pass_mu_eta = ( _eta<2.5 );
	pass_all *= pass_mu_eta;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("mu eta", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("mu eta", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("mu eta", 1, weight);
	}
  }
  
  // pass tau
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    // pT
    pass_tau_pT = ( tau_had->Pt()*GeV > 25 );
	pass_all *= pass_tau_pT;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("tau pT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("tau pT", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("tau pT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("tau pT", 1, weight);
	}
	// eta
	double _eta = abs(tau_had->Eta());
	pass_tau_eta = ( _eta<1.37 ) || ( (_eta>1.52) && (_eta<2.5) );
	pass_all *= pass_tau_eta;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("tau eta", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("tau eta", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("tau eta", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("tau eta", 1, weight);
	}
  }
  
  // bjet, no overlap removal is needed
  // as e mu from W/Z/H/tau are not included
  // only count for pT>25 eta<2.5 !!! already in get_nbjet()
  pass_nbjet = ( get_nbjet( jets ) == 0 );
  pass_all *= pass_nbjet;
  if( pass_all ){
    if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("nb=0 pT eta", 1, weight);
    if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("nb=0 pT eta", 1, weight);
    if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("nb=0 pT eta", 1, weight);
    if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("nb=0 pT eta", 1, weight);
    if(isZtautau_emu) cf_Ztautau_emu("nb=0 pT eta", 1, weight);
  }

  //  pass_m3star
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    std::vector<double> _v_m3star = get_m3star( *tau_had, electron->p4(), electron->charge(), met );
	m3T = _v_m3star[0]*GeV;
	sinthetastar = _v_m3star[1];
	m3star = _v_m3star[2]*GeV;
	pass_m3star = ((m3star > 60) && (m3star < 110));
	pass_all *= pass_m3star;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("m3star", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("m3star", 1, weight);
	}
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    std::vector<double> _v_m3star = get_m3star( *tau_had, muon->p4(), muon->charge(), met );
	m3T = _v_m3star[0]*GeV;
	sinthetastar = _v_m3star[1];
	m3star = _v_m3star[2]*GeV;
	pass_m3star = ((m3star > 60) && (m3star < 110));
	pass_all *= pass_m3star;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("m3star", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("m3star", 1, weight);
	}
  }else if( isZtautau_emu ){
    std::vector<double> _v_m3star = get_m3star( electron->p4(), muon->p4(), muon->charge(), met );
	m3T = _v_m3star[0]*GeV;
	sinthetastar = _v_m3star[1];
	m3star = _v_m3star[2]*GeV;
	pass_m3star = ((m3star > 60) && (m3star < 110));
	pass_all *= pass_m3star;
	if( pass_all ){
      cf_Ztautau_emu("m3star", 1, weight);
	}
  }


  // pass_sumcosdphi
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    sumcosphi = get_sumcosphi( *tau_had, electron->p4(), met );
	pass_sumcosdphi = ( sumcosphi > -0.1 );
	pass_all *= pass_sumcosdphi;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("sumcosphi", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("sumcosphi", 1, weight);
	}
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    sumcosphi = get_sumcosphi( *tau_had, muon->p4(), met );
	pass_sumcosdphi = ( sumcosphi > -0.1 );
	pass_all *= pass_sumcosdphi;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("sumcosphi", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("sumcosphi", 1, weight);
	}
  }else if( isZtautau_emu ){
    sumcosphi = get_sumcosphi( electron->p4(), muon->p4(), met );
	pass_sumcosdphi = ( sumcosphi > -0.1 );
	pass_all *= pass_sumcosdphi;
	if( pass_all ){
      cf_Ztautau_emu("sumcosphi", 1, weight);
	}
  }


  // pass_at
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    aT = get_aT( *tau_had, electron->p4() )*GeV;
	pass_at = ( aT < 60 );
	pass_all *= pass_at;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("aT", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("aT", 1, weight);
	}
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    aT = get_aT( *tau_had, muon->p4() )*GeV;
	pass_at = ( aT < 60 );
	pass_all *= pass_at;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("aT", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("aT", 1, weight);
	}
  }else if( isZtautau_emu ){
    aT = get_aT( electron->p4(), muon->p4() )*GeV;
	pass_at = ( aT < 60 );
	pass_all *= pass_at;
	if( pass_all ){
      cf_Ztautau_emu("aT", 1, weight);
	}
  }


  // pass_tau1prong_eta for mutau only
  if( isZtautau_mutau_1prong ){
    pass_tau1prong_eta = (abs(tau_had->Eta()) > 0.1);
	pass_all *= pass_tau1prong_eta;
    if( pass_all ) cf_Ztautau_mutau_1prong("eta tau_had 1prong", 1, weight);
  }

  // pass_m_ll for etau only
  if( isZtautau_etau_1prong || isZtautau_etau_3prong || isZtautau_mutau_1prong || isZtautau_mutau_3prong || isZtautau_emu ){
    TLorentzVector _ll;
	if( isZtautau_etau_1prong || isZtautau_etau_3prong ) _ll = ( *tau_had + electron->p4() );
	if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ) _ll = ( *tau_had + muon->p4() );
	if(isZtautau_emu)  _ll = ( electron->p4() + muon->p4() );
    double _m_ll = _ll.M()*GeV;
	pass_m_ll = ( _m_ll < 85 );
	pass_all *= pass_m_ll;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("m(l,l)", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("m(l,l)", 1, weight);
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("m(l,l)", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("m(l,l)", 1, weight);
      if(isZtautau_emu) cf_Ztautau_emu("m(l,l)", 1, weight);
	}
  }

  // pass reco
  if( isZtautau_etau_1prong || isZtautau_etau_3prong ){
    pass_reco = selected_reco_etau( eventnumber );
	pass_all *= pass_reco;
	if( pass_all ){
      if(isZtautau_etau_1prong) cf_Ztautau_etau_1prong("reco", 1, weight);
      if(isZtautau_etau_3prong) cf_Ztautau_etau_3prong("reco", 1, weight);
	}
  }else if( isZtautau_mutau_1prong || isZtautau_mutau_3prong ){
    pass_reco = selected_reco_mutau( eventnumber );
	pass_all *= pass_reco;
	if( pass_all ){
      if(isZtautau_mutau_1prong) cf_Ztautau_mutau_1prong("reco", 1, weight);
      if(isZtautau_mutau_3prong) cf_Ztautau_mutau_3prong("reco", 1, weight);
	}
  }else if( isZtautau_emu ){
    pass_reco = selected_reco_emu( eventnumber );
	pass_all *= pass_reco;
	if( pass_all ){
      cf_Ztautau_emu("reco", 1, weight);
	}
  }

  // check signal crossing
  pass_reco_etau = selected_reco_etau( eventnumber );
  pass_reco_mutau = selected_reco_mutau( eventnumber );
  pass_reco_emu = selected_reco_emu( eventnumber );

  tree( "Zll" )->Fill();

  delete met;
  delete tau_had;
  delete tau_had2;

  return StatusCode::SUCCESS;
}

StatusCode Zll :: finalize ()
{
  // This method is the mirror image of initialize(), meaning it gets
  // called after the last event has been processed on the worker node
  // and allows you to finish up any objects you created in
  // initialize() before they are written to disk.  This is actually
  // fairly rare, since this happens separately for each worker node.
  // Most of the time you want to do your post-processing on the
  // submission node after all your histogram outputs have been
  // merged.

  // fill cutflow to histogram
  ANA_CHECK (book (  cf_Ztautau_etau_1prong.cutflow_hist() ));
  ANA_CHECK (book (  cf_Ztautau_etau_3prong.cutflow_hist() ));
  ANA_CHECK (book (  cf_Ztautau_mutau_1prong.cutflow_hist() ));
  ANA_CHECK (book (  cf_Ztautau_mutau_3prong.cutflow_hist() ));
  ANA_CHECK (book (  cf_Ztautau_emu.cutflow_hist() ));
  //
  //ANA_CHECK (book (  cf_Ztautau_tautau_11prong.cutflow_hist() ));
  //ANA_CHECK (book (  cf_Ztautau_tautau_13prong.cutflow_hist() ));
  //ANA_CHECK (book (  cf_Ztautau_tautau_33prong.cutflow_hist() ));
  //ANA_CHECK (book (  cf_Ztautau_ee.cutflow_hist() ));
  //ANA_CHECK (book (  cf_Ztautau_mumu.cutflow_hist() ));

  ANA_CHECK (book (  cf_Ztautau_etau_1prong.cutflow_hist_weight() ));
  ANA_CHECK (book (  cf_Ztautau_etau_3prong.cutflow_hist_weight() ));
  ANA_CHECK (book (  cf_Ztautau_mutau_1prong.cutflow_hist_weight() ));
  ANA_CHECK (book (  cf_Ztautau_mutau_3prong.cutflow_hist_weight() ));
  ANA_CHECK (book (  cf_Ztautau_emu.cutflow_hist_weight() ));
  //
  //ANA_CHECK (book (  cf_Ztautau_tautau_11prong.cutflow_hist_weight() ));
  //ANA_CHECK (book (  cf_Ztautau_tautau_13prong.cutflow_hist_weight() ));
  //ANA_CHECK (book (  cf_Ztautau_tautau_33prong.cutflow_hist_weight() ));
  //ANA_CHECK (book (  cf_Ztautau_ee.cutflow_hist_weight() ));
  //ANA_CHECK (book (  cf_Ztautau_mumu.cutflow_hist_weight() ));

  ANA_MSG_INFO("Take BRs Zll=0.108 Wqq'=0.676");
  ANA_MSG_INFO("BR tau_had = 0.65 tau_lep = 0.35");
  ANA_MSG_INFO("Ztt->etau / Zll = 0.35 / 2 * 0.65 * 2 = 0.228");
  ANA_MSG_INFO("Ztt->emu / Zll = 0.35 / 2 * 0.35 / 2 * 2 = 0.061");

  ANA_MSG_INFO("# of Z->tt->etau 1prong: " << ctrZtautau_etau_1prong << " fraction (exp.) = " << 1.0*ctrZtautau_etau_1prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->etau 3prong: " << ctrZtautau_etau_3prong << " fraction (exp.) = " << 1.0*ctrZtautau_etau_3prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->etau all: " << (ctrZtautau_etau_1prong+ctrZtautau_etau_3prong) << " fraction (exp. 0.228) = " << 1.0*(ctrZtautau_etau_1prong+ctrZtautau_etau_3prong)/ctrAll );

  ANA_MSG_INFO("# of Z->tt->mutau 1prong: " << ctrZtautau_mutau_1prong << " fraction (exp.) = " << 1.0*ctrZtautau_mutau_1prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->mutau 3prong: " << ctrZtautau_mutau_3prong << " fraction (exp.) = " << 1.0*ctrZtautau_mutau_3prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->mutau all: " << (ctrZtautau_mutau_1prong+ctrZtautau_mutau_3prong) << " fraction (exp. 0.228) = " << 1.0*(ctrZtautau_mutau_1prong+ctrZtautau_mutau_3prong)/ctrAll );

  ANA_MSG_INFO("# of Z->tt->emu: " << ctrZtautau_emu << " fraction (exp. 0.061) = " << 1.0*ctrZtautau_emu/ctrAll );

  ANA_MSG_INFO("The others for additional checks");

  ANA_MSG_INFO("# of Z->tt->tautau 1prong 1prong: " << ctrZtautau_tautau_11prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_11prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau 1prong 3prong: " << ctrZtautau_tautau_13prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_13prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau 3prong 3prong: " << ctrZtautau_tautau_33prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_33prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau any prong: " << (ctrZtautau_tautau_11prong+ctrZtautau_tautau_13prong+ctrZtautau_tautau_33prong) << " fraction (exp.) = " << 1.0*(ctrZtautau_tautau_11prong+ctrZtautau_tautau_13prong+ctrZtautau_tautau_33prong/ctrAll ) );
  ANA_MSG_INFO("# of Z->tt->ee: " << ctrZtautau_ee << " fraction (exp.) = " << 1.0*ctrZtautau_ee/ctrAll );
  ANA_MSG_INFO("# of Z->tt->mumu: " << ctrZtautau_mumu << " fraction (exp.) = " << 1.0*ctrZtautau_mumu/ctrAll );

  ANA_MSG_INFO("# of Z->tt->etau 5prong: " << ctrZtautau_etau_5prong << " fraction (exp.) = " << 1.0*ctrZtautau_etau_5prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->mutau 5prong: " << ctrZtautau_mutau_5prong << " fraction (exp.) = " << 1.0*ctrZtautau_mutau_5prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau 15prong: " << ctrZtautau_tautau_15prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_15prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau 35prong: " << ctrZtautau_tautau_35prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_35prong/ctrAll );
  ANA_MSG_INFO("# of Z->tt->tautau 55prong: " << ctrZtautau_tautau_55prong << " fraction (exp.) = " << 1.0*ctrZtautau_tautau_55prong/ctrAll );

  ANA_MSG_INFO("# of all events: " << ctrAll << " ?= " << (ctrZtautau_tautau_11prong+ctrZtautau_tautau_13prong+ctrZtautau_tautau_33prong+ctrZtautau_ee+ctrZtautau_mumu+ctrZtautau_etau_1prong+ctrZtautau_etau_3prong+ctrZtautau_mutau_1prong+ctrZtautau_mutau_3prong+ctrZtautau_emu+ctrZtautau_etau_5prong+ctrZtautau_mutau_5prong+ctrZtautau_tautau_15prong+ctrZtautau_tautau_35prong+ctrZtautau_tautau_55prong) << " (sum of all channels)" );
  ANA_MSG_INFO("# of Uncategorised events: " << ctrUncategorised );

  delete __list_recosel_et;
  delete __list_recosel_mt;
  delete __list_recosel_em;

  return StatusCode::SUCCESS;
}

bool Zll :: fromZ( const xAOD::TruthParticle* p ){
  for( unsigned int ipa=0; ipa<p->nParents(); ++ipa ){
    if( abs(p->parent(ipa)->pdgId())==23 ){
      //ANA_MSG_INFO("PART " << p->pdgId() << " has Z parent");
	  return true;
	}
  }
  return false;
}

// return 0: tau lep
// 1: 1-prong
// 3: 2-prong
int Zll :: tauDecay( const xAOD::TruthParticle* tau ){
  //ANA_MSG_INFO("tau " << tau);
  int _sum_charge_trk = 0;
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
    // status
    int _status_ = tau->child(itau)->status();
	if( _status_ == 3 ) continue;
	// status == 3 will avoid double couting tau kids in PowhegPythia8
	//   ID [status] 15 2
	//   ID   kid [status] -16 1
	//   ID   kid [status] 111 2
	//   ID   kid [status] 211 1
	//   ID   kid [status] 211 1
	//   ID   kid [status] -211 1
	//   ID   kid [status] 22 1
	//   ID   kid [status] -16 3
	//   ID   kid [status] 111 3
	//   ID   kid [status] 211 3
	//   ID   kid [status] 211 3
	//   ID   kid [status] -211 3
	//
    //ANA_MSG_INFO("tau decay child pdgId = " << tau->child(itau)->pdgId() );
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==11) || (_id_==13) ){ // once found e/mu, regard as lep tau
	  //ANA_MSG_INFO("tau decay type : lep");
	  //return 0;
	}
	else{ // had tau decay
	  //ANA_MSG_INFO("tau decay type : had");
	  _sum_charge_trk += int(abs(tau->child(itau)->charge())); // consider |charge| as number of charged track
	}
  }
  //ANA_MSG_INFO("tau decay to charged tracks # = " << _sum_charge_trk);
  return _sum_charge_trk; // number of charged tracks
}

// pass me a tau that decays had
// return the sum p4 of non-neutrino decay products
TLorentzVector* Zll :: get_tau_had( const xAOD::TruthParticle* tau ){
  TLorentzVector* _v = nullptr;
  for( unsigned int itau=0; itau<tau->nChildren(); ++itau){
    int _status_ = tau->child(itau)->status();
	if( _status_ == 3 ) continue; // especially for pythia !!! it records all status=3 particles
	int _id_ = abs(tau->child(itau)->pdgId());
	if( (_id_==12) || (_id_==14) || (_id_==16) ){ // neutrinos
	  continue;
	}
	if( !_v ){
	  _v = new TLorentzVector(tau->child(itau)->p4()); // considering radiation all within calo cones, thus do not remove radiations
	}else{
	  (*_v) += tau->child(itau)->p4(); // considering radiation all within calo cones, thus do not remove radiations
	}
  }

  return _v;
}

// go down to the end of the "self-decay" (radiating gluons/photons etc.)
const xAOD::TruthParticle* Zll :: correctedParticle( const xAOD::TruthParticle * part ){
  const xAOD::TruthParticle* corrpart = part;
  for( unsigned ikid=0; ikid<part->nChildren(); ++ikid ){
    const xAOD::TruthParticle* kid = part->child( ikid );
	if( kid->pdgId() == part->pdgId() ){
	  //Info( "correctedParticle()", "[pdgId %d] pT %f, eta %f, barcode %d, parent barcode %d, status %d",
	  //      kid->pdgId(), kid->pt(), kid->eta(), kid->barcode(), kid->parent(0)->barcode(), kid->status() );
	  corrpart = correctedParticle( kid );
	  break;
	}
  }
  return corrpart;
}

// before any radiation
const xAOD::TruthParticle* Zll :: beforeRadiation( const xAOD::TruthParticle * part ){
  const xAOD::TruthParticle* origpart = part;
  for( unsigned int iparent=0; iparent<part->nParents(); ++ iparent ){
    const xAOD::TruthParticle* parent = part->parent( iparent );
	if( parent->pdgId() == part->pdgId() ){
	  origpart = beforeRadiation( part );
	  break;
	}
  }
  return origpart;
}

bool Zll :: selected_reco_etau( ULong64_t evtnum ){
  //if( __tree_selected_et->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_et->begin(), __list_recosel_et->end(), evtnum);
}
bool Zll :: selected_reco_mutau( ULong64_t evtnum ){
  //if( __tree_selected_mt->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_mt->begin(), __list_recosel_mt->end(), evtnum);
}
bool Zll :: selected_reco_emu( ULong64_t evtnum ){
  //if( __tree_selected_em->GetEntries(Form("MCEventNumber==%d",evtnum)) ) return true;
  //else return false;
  return std::binary_search(__list_recosel_em->begin(), __list_recosel_em->end(), evtnum);
}

// not responsible for mem consuption of the large vector!!!
std::vector<ULong64_t>* Zll :: getlist_recoselection( const char* fname ){

  auto _gD = gDirectory;
  auto _file = new TFile(PathResolverFindCalibFile(fname).c_str());
  auto _tree = (TTree*) _file->Get("output");
  ULong64_t _evtnb = 0;
  _tree->SetBranchAddress("MCEventNumber", &_evtnb);
  auto _n = _tree->GetEntries();

  // disappointed by ROOT ... this is long-type var and it does not work when 6M events are in the tree ...
  /*
  _tree->Draw("MCEventNumber","1","goff");
  int _nrows = _tree->GetSelectedRows();
  double* _darray = _tree->GetV1();
  std::vector<int>* _vec = new std::vector<int>( _nrows, 0 );
  for( int i=0; i<_nrows; ++i )
    _vec->push_back( int(_darray[i]) );
  */

  std::vector<ULong64_t>* _vec = new std::vector<ULong64_t>( _n, 0 );
  for( ULong64_t i=0; i<_n; ++i ){
    _tree->GetEntry(i);
    _vec->push_back( _evtnb );
  }
  std::sort( _vec->begin(), _vec->end());

  delete _tree;
  _file->Close();
  delete _file;
  _gD->cd();

  return _vec;

}

// https://gitlab.cern.ch/atlas/athena/-/blob/21.0/PhysicsAnalysis/AnalysisCommon/ParticleJetTools/ParticleJetTools/JetFlavourInfo.h#L10
// 5:             b jet
// 4:             c jet
// 15:           tau jet
// 0: light-flavour jet
int Zll :: get_nbjet( const xAOD::JetContainer* jets ){
  int _n = 0;
  for( auto j : *jets ){
    bool _kine = false;
	if( (j->pt()*GeV > 25) && ( abs(j->eta())<2.5 ) ) _kine = true; // only count for pT>25 eta<2.5 !!!
    if( _kine && (flavor(*j) == 5) ) ++_n;
  }
  return _n;
}

// the scattering angle of the leptons relative to the beam in the dilepton rest frame
// consider eta ~ rapidity
// rapidity sum rule eta(l_LAB) = eta(Z_LAB) + eta(l_Z)
// eta(l_Z) = eta(l_LAB) - eta(Z_LAB)
// pseudorapidity eta = arctanh( pL / |p| ) = arctanh( costheta )
// costheta = tanh(eta)
// SO: v1 = (l_LAB) v2 = (Z_LAB)
double Zll :: get_thetastar(const TLorentzVector& v1, const TLorentzVector& v2)const{
   const double cosThetaStar = TMath::TanH((v1.Eta() - v2.Eta()) / 2.);
   return TMath::ACos(cosThetaStar);
} 

// calculate 3-body transverse mass: sum of squares of 3 pairs of 2 body transverse masses
double Zll :: get_m3T(const TLorentzVector& v_lep, const TLorentzVector& v_tau, const TVector3& MET) const {
	const double mTLepMET = get_mT(v_lep, MET);
	const double mTTauMET = get_mT(v_tau, MET);
	const double mTLepTau = get_mT(v_tau, v_lep);
	const double M3Sq = TMath::Power(mTLepMET, 2.) + TMath::Power(mTTauMET, 2.) + TMath::Power(mTLepTau, 2.);
	return TMath::Sqrt(M3Sq);
}
// calculate transverse mass
double Zll :: get_mT(const TLorentzVector& v_lep, const TVector3& MET) const {
	const double dCos = 1. - TMath::Cos(v_lep.Phi() - MET.Phi());
	const double retVal = 2 * v_lep.Pt() * MET.Pt() * dCos;
	return TMath::Sqrt(retVal);
}
// calculate transverse mass
double Zll :: get_mT(const TLorentzVector& v_lep, const TLorentzVector& v_tau) const {
	const double dCos = 1. - TMath::Cos(v_lep.Phi() - v_tau.Phi());
	const double retVal = 2 * v_lep.Pt() * v_tau.Pt() * dCos;
	return TMath::Sqrt(retVal);
}

// thisp: tau_had
// thatp: e or mu
std::vector<double> Zll :: get_m3star( const TLorentzVector& thisv, const TLorentzVector& thatv, double thatcharge, const xAOD::MissingET * met){
  TVector3 ourmet = TVector3( met->mpx(), met->mpy(), 0 );
  TLorentzVector ourz =  thisv+thatv;

  //double _thetastar = get_thetastar( thisv, ourz );
  // test (eta-) - (eta+)
  double _thetastar = 0;
  //if( thisp->charge() < 0 )
  if( thatcharge > 0 )
    _thetastar = get_thetastar( thisv, thatv );
  else
    _thetastar = get_thetastar( thatv, thisv );
  // test (eta-) - (eta+)
  //
  double _sinthetastar = TMath::Sin(_thetastar);
  double _m3T = get_m3T( thatv, thisv, ourmet );
  std::vector<double> _res = {_m3T, _sinthetastar, _m3T/_sinthetastar};

  return _res;
}

// thisp: tau_had
// thatp: e or mu
double Zll :: get_aT( const TLorentzVector& v_lep1, const TLorentzVector& v_lep2 ) const {

	// calculate event axis = unit vector in direction of pT(lep1) relative to pT(lep2)
	TVector3 vec1 = v_lep1.Vect();
	TVector3 vec2 = v_lep2.Vect();
	
	// event axis definiton uses transverse plane -> set Z component to 0
	vec1.SetZ(0);
	vec2.SetZ(0);
	const TVector3 eventAxis = (vec1 - vec2).Unit();
	const TVector3 Q = vec1 + vec2;

	double aT = -999.;
	const double dPhi = TMath::Abs(v_lep1.DeltaPhi(v_lep2));
	if (dPhi < TMath::Pi() / 2.) {
		aT = TMath::Abs( Q.Mag() );
	}
	else {
	// aT = | Q * eventAxis |, * means vector 3 product
		TVector3 aTVec = Q.Cross(eventAxis);
		aT = TMath::Abs( aTVec.Mag() );
	}

	return aT;
}

// thisp: tau_had
// thatp: e or mu
double Zll :: get_sumcosphi( const TLorentzVector& v_lep1, const TLorentzVector& v_lep2, const xAOD::MissingET * met ) const {


	const double MET_phi = met->phi();
	const double SumCosDPhi = TMath::Cos(v_lep1.Phi() - MET_phi) + TMath::Cos(v_lep2.Phi() - MET_phi);
	
	return SumCosDPhi;
}

bool Zll :: _isSherpa( const int dsid ){

  for( auto d : sherpa_dsid ){
    if( d == dsid ) return true;
  }
  return false;

}
