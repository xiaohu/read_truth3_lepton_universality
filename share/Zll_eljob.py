#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
parser.add_option( '-g', '--grid-input', dest = 'gridinput',
                   action = 'store', type = 'string', default = 'grid input',
                   help = 'grid input' )
parser.add_option( '-l', '--local-input', dest = 'localinput',
                   action = 'store', type = 'string', default = 'local input',
                   help = 'local input' )
parser.add_option( '-e', '--generator', dest = 'generator',
                   action = 'store', type = 'string', default = 'powheg',
                   help = 'generator type' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# local runs
#inputFilePath = '/eos/user/x/xiaohu/LU/dataset/mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975/'
#if options.localinput != "local input":
#  inputFilePath = '/eos/user/x/xiaohu/LU/dataset/'+options.localinput
#ROOT.SH.ScanDir().filePattern( 'DAOD_STDM4*.pool.root.1' ).scan( sh, inputFilePath )

# grid runs
##ROOT.SH.scanRucio (sh, "mc16_13TeV.361108.PowhegPythia8EvtGen_AZNLOCTEQ6L1_Ztautau.deriv.DAOD_STDM4.e3601_s3126_r9364_p3975")
ROOT.SH.scanRucio (sh, options.gridinput)

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 100 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg_Zll = createAlgorithm ( 'Zll', 'myZll' )
alg_Zll.generator = options.generator

# later on we'll add some configuration options for our algorithm that go here

#
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add our algorithm to the job
job.algsAdd( alg_Zll )

# Run the job using the direct driver.

# local runs
#driver = ROOT.EL.DirectDriver()
#driver.submit( job, options.submission_dir )

mc=''
if 'r9364' in options.gridinput:
  mc='a'
if 'r10201' in options.gridinput:
  mc='d'
if 'r10724' in options.gridinput:
  mc='e'
# grid runs
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv19{0}.%in:name[2]%.%in:name[6]%".format(mc))
driver.submitOnly( job, options.submission_dir )

