#!/usr/bin/env python

# Read the submission directory as a command line argument. You can
# extend the list of arguments with your private ones later on.
import optparse
parser = optparse.OptionParser()
parser.add_option( '-s', '--submission-dir', dest = 'submission_dir',
                   action = 'store', type = 'string', default = 'submitDir',
                   help = 'Submission directory for EventLoop' )
( options, args ) = parser.parse_args()

# Set up (Py)ROOT.
import ROOT
ROOT.xAOD.Init().ignore()

# Set up the sample handler object. See comments from the C++ macro
# for the details about these lines.
import os
sh = ROOT.SH.SampleHandler()
sh.setMetaString( 'nc_tree', 'CollectionTree' )

# local runs
##inputFilePath = '/eos/user/x/xiaohu/LU/dataset/mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r9364_p4097/'
#inputFilePath = '/eos/user/x/xiaohu/LU/dataset/mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_s3126_r9364_p4097/'
#ROOT.SH.ScanDir().filePattern( 'DAOD_STDM4*.pool.root.1' ).scan( sh, inputFilePath )

# grid runs
# this dataset is gone #ROOT.SH.scanRucio (sh, "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r9364_p3972")
#ROOT.SH.scanRucio (sh, "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r9364_p4097")
#ROOT.SH.scanRucio (sh, "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r10201_p4097")
ROOT.SH.scanRucio (sh, "mc16_13TeV.410470.PhPy8EG_A14_ttbar_hdamp258p75_nonallhad.deriv.DAOD_STDM4.e6337_s3126_r10724_p4097")
##ROOT.SH.scanRucio (sh, "mc16_13TeV.410472.PhPy8EG_A14_ttbar_hdamp258p75_dil.deriv.DAOD_STDM4.e6348_s3126_r9364_p4097")

sh.printContent()

# Create an EventLoop job.
job = ROOT.EL.Job()
job.sampleHandler( sh )
#job.options().setDouble( ROOT.EL.Job.optMaxEvents, 500 )
job.options().setString( ROOT.EL.Job.optSubmitDirMode, 'unique-link')

# Create the algorithm's configuration.
from AnaAlgorithm.DualUseConfig import createAlgorithm
alg_Wlv = createAlgorithm ( 'Wlv', 'myWlv' )
# channel: Wetau Wmutau (tt: W->ev / W->muv W->taunv); Wemu (tt: W->ev W->muv)
#alg_Wlv.channel = 'Wetau'

# later on we'll add some configuration options for our algorithm that go here

#
job.outputAdd (ROOT.EL.OutputStream ('ANALYSIS'))

# Add our algorithm to the job
job.algsAdd( alg_Wlv )

# Run the job using the direct driver.

# local runs
#driver = ROOT.EL.DirectDriver()
#driver.submit( job, options.submission_dir )

# grid runs
driver = ROOT.EL.PrunDriver()
driver.options().setString("nc_outputSampleName", "user.xiaohu.lu.testv20e.%in:name[2]%.%in:name[6]%")
driver.submitOnly( job, options.submission_dir )

