#ifndef read_truth3_Zll_H
#define read_truth3_Zll_H

#include <algorithm>
#include <vector>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODTruth/TruthParticle.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <xAODEventInfo/EventInfo.h>
#include <PathResolver/PathResolver.h>

#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETAssociationMap.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODJet/JetContainer.h>

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>

#include <read_truth3/Tree_Zll.h>
#include <read_truth3/Cutflow.h>

class Zll : public EL::AnaAlgorithm, public Tree_Zll
{
public:
  // this is a standard algorithm constructor
  Zll (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  //
  //std::string channel;

  // constants
  const double GeV = 0.001;

  // config
  std::string generator;

  // counting raw MC events for each truth channel
  int ctrAll = 0;
  int ctrZtautau_etau_1prong = 0;
  int ctrZtautau_etau_3prong = 0;
  int ctrZtautau_mutau_1prong = 0;
  int ctrZtautau_mutau_3prong = 0;
  int ctrZtautau_emu = 0;
  int ctrUncategorised = 0;
  // additional to check
  int ctrZtautau_tautau_11prong = 0;
  int ctrZtautau_tautau_13prong = 0;
  int ctrZtautau_tautau_33prong = 0;
  int ctrZtautau_ee = 0;
  int ctrZtautau_mumu = 0;
  //
  int ctrZtautau_etau_5prong = 0;
  int ctrZtautau_mutau_5prong = 0;
  int ctrZtautau_tautau_15prong = 0;
  int ctrZtautau_tautau_35prong = 0;
  int ctrZtautau_tautau_55prong = 0;

  // cutflow for each truth channel
  Cutflow cf_Ztautau_etau_1prong;
  Cutflow cf_Ztautau_etau_3prong;
  Cutflow cf_Ztautau_mutau_1prong;
  Cutflow cf_Ztautau_mutau_3prong;
  Cutflow cf_Ztautau_emu;
  //// additional to check
  //Cutflow cf_Ztautau_tautau_11prong;
  //Cutflow cf_Ztautau_tautau_13prong;
  //Cutflow cf_Ztautau_tautau_33prong;
  //Cutflow cf_Ztautau_ee;
  //Cutflow cf_Ztautau_mumu;
  ////
  //Cutflow cf_Ztautau_etau_5prong;
  //Cutflow cf_Ztautau_mutau_5prong;
  //Cutflow cf_Ztautau_tautau_15prong;
  //Cutflow cf_Ztautau_tautau_35prong;
  //Cutflow cf_Ztautau_tautau_55prong;

  // sherpa_dsid
  std::vector<int> sherpa_dsid;

  //
  static SG::AuxElement::ConstAccessor<unsigned int> ptype;
  static SG::AuxElement::ConstAccessor<unsigned int> porigin;
  static SG::AuxElement::ConstAccessor<int> flavor;
  int get_nbjet( const xAOD::JetContainer* jets );

  // truth tool
  bool fromZ( const xAOD::TruthParticle* );
  int tauDecay( const xAOD::TruthParticle* tau ); // lep decay: 0; had 1prong: 1; had 3-pring: 3
  TLorentzVector* get_tau_had( const xAOD::TruthParticle* tau );
  const xAOD::TruthParticle* correctedParticle( const xAOD::TruthParticle * part );
  const xAOD::TruthParticle* beforeRadiation( const xAOD::TruthParticle * part );

  // kinematic tool
  double get_mT(const TLorentzVector& v_lep, const TLorentzVector& v_tau) const ;
  double get_mT(const TLorentzVector& v_lep, const TVector3& MET) const ;
  double get_m3T(const TLorentzVector& v_lep, const TLorentzVector& v_tau, const TVector3& MET) const ;
  double get_thetastar(const TLorentzVector& v1, const TLorentzVector& v2) const ;
  std::vector<double> get_m3star( const TLorentzVector&, const TLorentzVector&, double, const xAOD::MissingET *);

  double get_aT( const TLorentzVector&, const TLorentzVector& ) const ;
  double get_sumcosphi( const TLorentzVector&, const TLorentzVector&, const xAOD::MissingET * ) const ;

  // reco
  std::vector<ULong64_t>* getlist_recoselection( const char* fname );
  bool selected_reco_etau( ULong64_t evtnum );
  bool selected_reco_mutau( ULong64_t evtnum );
  bool selected_reco_emu( ULong64_t evtnum );

  // MC
  bool _isSherpa( const int disd );

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  // lists of event numbers of reco selections
  // delete in finalize()
  std::vector<ULong64_t>* __list_recosel_et; // etau
  std::vector<ULong64_t>* __list_recosel_mt; // mutau
  std::vector<ULong64_t>* __list_recosel_em; // emu

};

#endif
