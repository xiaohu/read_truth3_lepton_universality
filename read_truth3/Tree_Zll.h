#ifndef VjetTreeMaker_Tree_Zll_H
#define VjetTreeMaker_Tree_Zll_H

#include <TTree.h>

// define tree leaves, connect to local variables and manage them
// creation and filling of the tree should be handled exernally
class Tree_Zll {

public:

  Tree_Zll() {;}
  void initialize_tree( TTree* tr );
  void initialize_leaves();

  // event basic info
  ULong64_t eventnumber = 0;
  // weights
  double weight = 1;

  // reord channel by truth finding
  int isZtautau_etau_1prong = 0;
  int isZtautau_etau_3prong = 0;
  int isZtautau_mutau_1prong = 0;
  int isZtautau_mutau_3prong = 0;
  int isZtautau_emu = 0;
  int isUncategorised = 0;
  int isHowManyTau = 0;
  // additional to check
  int isZtautau_tautau_11prong = 0;
  int isZtautau_tautau_13prong = 0;
  int isZtautau_tautau_33prong = 0;
  int isZtautau_ee = 0;
  int isZtautau_mumu = 0;
  //
  int isZtautau_etau_5prong = 0;
  int isZtautau_mutau_5prong = 0;
  int isZtautau_tautau_15prong = 0;
  int isZtautau_tautau_35prong = 0;
  int isZtautau_tautau_55prong = 0;

  // pass selection
  int pass_e_pT = 0;
  int pass_e_eta = 0;
  int pass_mu_pT = 0;
  int pass_mu_eta = 0;
  int pass_tau_pT = 0;
  int pass_tau_eta = 0;
  int pass_jet_pT = 0;
  int pass_jet_eta = 0;
  int pass_nbjet = 0;
  int pass_m3star = 0;
  int pass_sumcosdphi = 0;
  int pass_at = 0;
  int pass_tau1prong_eta = 0;
  int pass_m_ll = 0;
  int pass_reco = 0;
  int pass_all = 0;

  int pass_reco_etau = 0; // check signal crossing
  int pass_reco_mutau = 0; // check signal crossing
  int pass_reco_emu = 0; // check signal crossing

  // tau decay
  int decay_tau_x = 0;
  int decay_tau_e = 0;
  int decay_tau_mu = 0;
  int decay_tau_had_1prong = 0;
  int decay_tau_had_3prong = 0;
  int decay_tau_had_5prong = 0;

  // variables
  double e_pT = 0;
  double e_eta = 0;
  double e_phi = 0;
  double e_E = 0;

  double m_pT = 0;
  double m_eta = 0;
  double m_phi = 0;
  double m_E = 0;

  double t_pT = 0;
  double t_eta = 0;
  double t_phi = 0;
  double t_E = 0;

  double truth_t0_pT = 0;
  double truth_t0_eta = 0;
  double truth_t0_phi = 0;
  double truth_t0_E = 0;
  double truth_t1_pT = 0;
  double truth_t1_eta = 0;
  double truth_t1_phi = 0;
  double truth_t1_E = 0;

  double ll_pT = 0; // l are decay products; e,mu,tau_had
  double ll_eta = 0; // l are decay products; e,mu,tau_had
  double ll_phi = 0; // l are decay products; e,mu,tau_had
  double ll_E = 0; // l are decay products; e,mu,tau_had
  double ll_m = 0; // l are decay products; e,mu,tau_had

  double z_pT = 0; // z mediator, powheg has z, sherpa does not (use tautau truth before radiation)
  double z_eta = 0; // z mediator
  double z_phi = 0; // z mediator
  double z_E = 0; // z mediator
  double z_m = 0; // z mediator

  double tautau_truth_pT = 0; // truth tautau before radiation, powheg as z, but occasionally this mZ=0, use tautau truth then
  double tautau_truth_eta = 0; // truth tautau before radiation
  double tautau_truth_phi = 0; // truth tautau before radiation
  double tautau_truth_E = 0; // truth tautau before radiation
  double tautau_truth_m = 0; // truth tautau before radiation

  double m3star = 0;
  double m3T = 0;
  double sinthetastar = 0;
  double sumcosphi = 0;
  double aT = 0;

  int tau1_prong = -1;
  int tau2_prong = -1;
  // for additional tau beyond the two we are looking for
  int tau3_prong = -1;
  int tau4_prong = -1;
  int tau5_prong = -1;

private:

  TTree* thetree;

};
#endif
