#ifndef read_truth3_Wlv_H
#define read_truth3_Wlv_H

#include <algorithm>
#include <vector>

#include <AnaAlgorithm/AnaAlgorithm.h>
#include <xAODTruth/TruthParticle.h>
#include <MCTruthClassifier/MCTruthClassifierDefs.h>
#include <xAODEventInfo/EventInfo.h>
#include <PathResolver/PathResolver.h>

#include <xAODMissingET/MissingETAuxContainer.h>
#include <xAODMissingET/MissingETAssociationMap.h>
#include <xAODMissingET/MissingETContainer.h>
#include <xAODJet/JetContainer.h>

#include <TFile.h>
#include <TTree.h>
#include <TH1.h>

#include <read_truth3/Tree_Wlv.h>
#include <read_truth3/Cutflow.h>

//#include <PMGAnalysisInterfaces/IPMGTruthWeightTool.h>

class Wlv : public EL::AnaAlgorithm, public Tree_Wlv
{
public:
  // this is a standard algorithm constructor
  Wlv (const std::string& name, ISvcLocator* pSvcLocator);

  // these are the functions inherited from Algorithm
  virtual StatusCode initialize () override;
  virtual StatusCode execute () override;
  virtual StatusCode finalize () override;

  //
  //std::string channel;

  // constants
  const double GeV = 0.001;

  // CUT
  const double CUT_jetpT = 25; // default 25 GeV

  // counting raw MC events for each truth channel
  int ctrWetauHad_1prong;
  int ctrWetauHad_3prong;
  //int ctrWetauLep;
  int ctrWmutauHad_1prong;
  int ctrWmutauHad_3prong;
  //int ctrWmutauLep;
  int ctrWemu;
  //int ctrWee;
  //int ctrWmumu;
  //int ctrWtautau;
  int ctrWej; // treat Wej as Wetau for jet faking tau
  int ctrWmuj; // treat Wmuj as Wmutau for jet faking tau
  //int ctrWtauj;
  //int ctrUnknown;
  int ctrAll;

  // cutflow for each truth channel
  Cutflow cf_WetauHad_1prong;
  Cutflow cf_WetauHad_3prong;
  //Cutflow cf_WetauLep;
  Cutflow cf_WmutauHad_1prong;
  Cutflow cf_WmutauHad_3prong;
  //Cutflow cf_WmutauLep;
  Cutflow cf_Wemu;
  Cutflow cf_Wej;
  Cutflow cf_Wmuj;

  Cutflow cf_WetauHad_1prong_fsr_muR05;
  Cutflow cf_WetauHad_3prong_fsr_muR05;
  Cutflow cf_WmutauHad_1prong_fsr_muR05;
  Cutflow cf_WmutauHad_3prong_fsr_muR05;
  Cutflow cf_Wemu_fsr_muR05;
  Cutflow cf_Wej_fsr_muR05;
  Cutflow cf_Wmuj_fsr_muR05;

  Cutflow cf_WetauHad_1prong_fsr_muR20;
  Cutflow cf_WetauHad_3prong_fsr_muR20;
  Cutflow cf_WmutauHad_1prong_fsr_muR20;
  Cutflow cf_WmutauHad_3prong_fsr_muR20;
  Cutflow cf_Wemu_fsr_muR20;
  Cutflow cf_Wej_fsr_muR20;
  Cutflow cf_Wmuj_fsr_muR20;

  //
  static SG::AuxElement::ConstAccessor<unsigned int> ptype;
  static SG::AuxElement::ConstAccessor<unsigned int> porigin;
  static SG::AuxElement::ConstAccessor<int> flavor;

  bool fromW( const xAOD::TruthParticle* );
  bool tauHad( const xAOD::TruthParticle* tau );
  const xAOD::TruthParticle* correctedParticle( const xAOD::TruthParticle * part );

  int tauDecay( const xAOD::TruthParticle* tau );
  int tauDecayLep( const xAOD::TruthParticle* tau );
  TLorentzVector* get_tau_had( const xAOD::TruthParticle* tau );
  TLorentzVector* get_tau_had_fakedbyWqq( const xAOD::JetContainer* jets );
  std::vector<int> get_jetorigin( const xAOD::JetContainer* jets );
  int get_njetx( const std::vector<int> & jetid, const int whichid );
  std::vector<int> get_idx_jetx( const std::vector<int> & jetid, const int whichid );
  //
  const xAOD::Jet* get_3rdjet( const xAOD::JetContainer* jets );
  const xAOD::Jet* get_3pXrdjet( const xAOD::JetContainer* jets, int X );
  const xAOD::Jet* get_3rdjet_inWlj( const xAOD::JetContainer* jets, TLorentzVector* jtau_fake );
  const xAOD::Jet* get_3pXrdjet_inWlj( const xAOD::JetContainer* jets, TLorentzVector* jtau_fake, int X );

  // FSR weight: simplye does not work ...
  //ToolHandle<PMGTools::IPMGTruthWeightTool> tool_weight;

  // reco
  std::vector<ULong64_t>* getlist_recoselection( const char* fname );
  bool selected_reco_etau( ULong64_t evtnum );
  bool selected_reco_mutau( ULong64_t evtnum );
  bool selected_reco_emu( ULong64_t evtnum );

private:
  // Configuration, and any other types of variables go here.
  //float m_cutValue;
  //TTree *m_myTree;
  //TH1 *m_myHist;

  // lists of event numbers of reco selections
  // delete in finalize()
  std::vector<ULong64_t>* __list_recosel_et; // etau
  std::vector<ULong64_t>* __list_recosel_mt; // mutau
  std::vector<ULong64_t>* __list_recosel_em; // emu

};

#endif
