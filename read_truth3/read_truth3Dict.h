#ifndef read_truth3_dict_h
#define read_truth3_dict_h

// This file includes all the header files that you need to create
// dictionaries for.

#include <read_truth3/Wlv.h>
#include <read_truth3/Tree_Wlv.h>
#include <read_truth3/Zll.h>
#include <read_truth3/Tree_Zll.h>
#include <read_truth3/Cutflow.h>

#endif
