#ifndef VjetTreeMaker_Tree_Wlv_H
#define VjetTreeMaker_Tree_Wlv_H

#include <TTree.h>

// define tree leaves, connect to local variables and manage them
// creation and filling of the tree should be handled exernally
class Tree_Wlv {

public:

  Tree_Wlv() {;}
  void initialize_tree( TTree* tr );
  void initialize_leaves();

  // event basic info
  ULong64_t eventnumber = 0;
  // weights
  double weight = 1;
  double weight_fsr_muR05 = 1;
  double weight_fsr_muR20 = 1;
  double weight_fsr_muR0625 = 1;
  double weight_fsr_muR0750 = 1;
  double weight_fsr_muR0875 = 1;
  double weight_fsr_muR1250 = 1;
  double weight_fsr_muR1500 = 1;
  double weight_fsr_muR1750 = 1;
  double weight_Var3cUp = 1;
  double weight_Var3cDown = 1;

  // reord channel by truth finding
  int isWetauHad_1prong = 0;
  int isWetauHad_3prong = 0;
  //int isWetauLep = 0;
  int isWmutauHad_1prong = 0;
  int isWmutauHad_3prong = 0;
  //int isWmutauLep = 0;
  int isWemu = 0;
  //int isWee = 0;
  //int isWmumu = 0;
  //int isWtautau = 0;
  int isWej = 0;
  int isWmuj = 0;
  //int isWtauj = 0;
  //int isUnknown = 0;

  // pass selection
  int pass_e_pT = 0;
  int pass_e_eta = 0;
  int pass_mu_pT = 0;
  int pass_mu_eta = 0;
  int pass_tau_pT = 0;
  int pass_tau_eta = 0;
  int pass_bjet_n = 0;
  int pass_3rdjet = 0;
  int pass_m_taujet = 0;
  int pass_reco_etau = 0;
  int pass_reco_mutau = 0;
  int pass_reco_emu = 0;
  int pass_all = 0;

  // tau decay
  int decay_tau_x = 0;
  int decay_tau_e = 0;
  int decay_tau_mu = 0;
  int decay_tau_had_1prong = 0;
  int decay_tau_had_3prong = 0;
  int decay_tau_had_5prong = 0;

  // variables
  // in principle only fill kinematics for ltau, emu events
  double e_pT = 0;
  double e_eta = 0;
  double e_phi = 0;
  double e_E = 0;

  double m_pT = 0;
  double m_eta = 0;
  double m_phi = 0;
  double m_E = 0;

  double t_pT = 0;
  double t_eta = 0;
  double t_phi = 0;
  double t_E = 0;

  double met_pT = 0;
  double met_eta = 0;
  double met_phi = 0;
  double met_E = 0;

  double b0_pT = 0;
  double b0_eta = 0;
  double b0_phi = 0;
  double b0_E = 0;

  double b1_pT = 0;
  double b1_eta = 0;
  double b1_phi = 0;
  double b1_E = 0;

  // 3rd jet, for m(j,tau) cut
  double j3_pT = 0;
  double j3_eta = 0;
  double j3_phi = 0;
  double j3_E = 0;
  double j4_pT = 0;
  double j4_eta = 0;
  double j4_phi = 0;
  double j4_E = 0;
  double j5_pT = 0;
  double j5_eta = 0;
  double j5_phi = 0;
  double j5_E = 0;
  double dR_l_j3 = 0;

  int n_bjet = 0;
  int n_ljet = 0;
  int n_jet = 0; // this is number of jets (light and b etc. all inclusive)
                 // n_jet == 2 mean only 2b; n_jet == 3, means 1 more additional jet
  double w0_pT = 0;
  double w0_eta = 0;
  double w0_phi = 0;
  double w0_E = 0;

  double w1_pT = 0;
  double w1_eta = 0;
  double w1_phi = 0;
  double w1_E = 0;

  double m_taujet = 0;

private:

  TTree* thetree;

};
#endif
